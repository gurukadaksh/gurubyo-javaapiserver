-- MySQL dump 10.13  Distrib 5.5.20, for osx10.6 (i386)
--
-- Host: localhost    Database: gurukadakshDB
-- ------------------------------------------------------
-- Server version	5.5.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applicationTokens`
--

DROP TABLE IF EXISTS `applicationTokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationTokens` (
  `applicationName` varchar(500) DEFAULT NULL,
  `applicationToken` varchar(600) NOT NULL,
  PRIMARY KEY (`applicationToken`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contactTable`
--

DROP TABLE IF EXISTS `contactTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactTable` (
  `primaryEmail` varchar(100) DEFAULT NULL,
  `alternateEmails` longtext,
  `primaryPhoneNumber` varchar(100) DEFAULT NULL,
  `alternatePhoneNumbers` longtext,
  `countryOfResidence_ISO` varchar(10) DEFAULT NULL,
  `address` longtext,
  `linkedinId` varchar(50) NOT NULL,
  PRIMARY KEY (`linkedinId`),
  CONSTRAINT `contactTable_ibfk_1` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `messageCenter`
--

DROP TABLE IF EXISTS `messageCenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messageCenter` (
  `messageId` varchar(250) NOT NULL,
  `fromId` varchar(50) NOT NULL,
  `Datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `projectId` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`messageId`),
  KEY `fromId` (`fromId`),
  KEY `projectId` (`projectId`),
  CONSTRAINT `messagecenter_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `userProjects` (`projectId`) ON DELETE SET NULL,
  CONSTRAINT `messageCenter_ibfk_2` FOREIGN KEY (`fromId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `messageRecievers`
--

DROP TABLE IF EXISTS `messageRecievers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messageRecievers` (
  `messageId` varchar(250) NOT NULL,
  `toId` varchar(50) NOT NULL,
  PRIMARY KEY (`messageId`,`toId`),
  KEY `toId` (`toId`),
  CONSTRAINT `messageRecievers_ibfk_1` FOREIGN KEY (`messageId`) REFERENCES `messageCenter` (`messageId`) ON DELETE CASCADE,
  CONSTRAINT `messageRecievers_ibfk_2` FOREIGN KEY (`toId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organizationPateAssociations`
--

DROP TABLE IF EXISTS `organizationPateAssociations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizationPateAssociations` (
  `linkedinCompanyId` bigint(20) unsigned NOT NULL,
  `pateKeyId` bigint(20) unsigned NOT NULL,
  `keyType` tinyint(1) DEFAULT NULL,
  `keyWeight` float DEFAULT NULL,
  PRIMARY KEY (`linkedinCompanyId`,`pateKeyId`),
  KEY `pateKeyId` (`pateKeyId`),
  CONSTRAINT `organizationpateassociations_ibfk_1` FOREIGN KEY (`linkedinCompanyId`) REFERENCES `organizationsDatabase` (`linkedinCompanyId`) ON DELETE CASCADE,
  CONSTRAINT `organizationpateassociations_ibfk_2` FOREIGN KEY (`pateKeyId`) REFERENCES `skillsDatabase` (`skillId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organizationSkillsTable`
--

DROP TABLE IF EXISTS `organizationSkillsTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizationSkillsTable` (
  `linkedinCompanyId` bigint(20) unsigned NOT NULL,
  `skillId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`linkedinCompanyId`,`skillId`),
  KEY `skillId` (`skillId`),
  CONSTRAINT `organizationskillstable_ibfk_1` FOREIGN KEY (`linkedinCompanyId`) REFERENCES `organizationsDatabase` (`linkedinCompanyId`) ON DELETE CASCADE,
  CONSTRAINT `organizationskillstable_ibfk_2` FOREIGN KEY (`skillId`) REFERENCES `skillsDatabase` (`skillId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organizationsDatabase`
--

DROP TABLE IF EXISTS `organizationsDatabase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizationsDatabase` (
  `linkedinCompanyId` bigint(20) unsigned NOT NULL DEFAULT '0',
  `organizationName` varchar(500) DEFAULT NULL,
  `organizationType` varchar(500) DEFAULT NULL,
  `organizationIndustry` varchar(500) DEFAULT NULL,
  `organizationDescription` longtext,
  PRIMARY KEY (`linkedinCompanyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pateKeywordsDatabase`
--

DROP TABLE IF EXISTS `pateKeywordsDatabase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pateKeywordsDatabase` (
  `pateKeyId` bigint(20) unsigned NOT NULL DEFAULT '0',
  `pateKey` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`pateKeyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projectGuru`
--

DROP TABLE IF EXISTS `projectGuru`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectGuru` (
  `projectId` bigint(20) unsigned NOT NULL,
  `linkedinId` varchar(50) NOT NULL,
  PRIMARY KEY (`projectId`,`linkedinId`),
  KEY `linkedinId` (`linkedinId`),
  CONSTRAINT `projectGuru_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `userProjects` (`projectId`) ON DELETE CASCADE,
  CONSTRAINT `projectGuru_ibfk_2` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projectInvites`
--

DROP TABLE IF EXISTS `projectInvites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectInvites` (
  `projectId` bigint(20) unsigned NOT NULL,
  `linkedinId` varchar(50) NOT NULL,
  PRIMARY KEY (`projectId`,`linkedinId`),
  KEY `linkedinId` (`linkedinId`),
  CONSTRAINT `projectInvites_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `userProjects` (`projectId`) ON DELETE CASCADE,
  CONSTRAINT `projectInvites_ibfk_2` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projectPateAssociations`
--

DROP TABLE IF EXISTS `projectPateAssociations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectPateAssociations` (
  `projectId` bigint(20) unsigned NOT NULL,
  `pateKeyId` bigint(20) unsigned NOT NULL DEFAULT '0',
  `keyType` tinyint(1) DEFAULT NULL,
  `keyWeight` float DEFAULT NULL,
  PRIMARY KEY (`projectId`,`pateKeyId`),
  KEY `pateKeyId` (`pateKeyId`),
  CONSTRAINT `projectPateAssociations_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `userProjects` (`projectId`) ON DELETE CASCADE,
  CONSTRAINT `projectPateAssociations_ibfk_2` FOREIGN KEY (`pateKeyId`) REFERENCES `skillsDatabase` (`skillId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projectSkills`
--

DROP TABLE IF EXISTS `projectSkills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectSkills` (
  `projectId` bigint(20) unsigned NOT NULL,
  `skillId` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`projectId`,`skillId`),
  KEY `skillId` (`skillId`),
  CONSTRAINT `projectSkills_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `userProjects` (`projectId`) ON DELETE CASCADE,
  CONSTRAINT `projectSkills_ibfk_2` FOREIGN KEY (`skillId`) REFERENCES `skillsDatabase` (`skillId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projectStudent`
--

DROP TABLE IF EXISTS `projectStudent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectStudent` (
  `projectId` bigint(20) unsigned NOT NULL,
  `linkedinId` varchar(50) NOT NULL,
  PRIMARY KEY (`projectId`,`linkedinId`),
  KEY `linkedinId` (`linkedinId`),
  CONSTRAINT `projectStudent_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `userProjects` (`projectId`) ON DELETE CASCADE,
  CONSTRAINT `projectStudent_ibfk_2` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skillsDatabase`
--

DROP TABLE IF EXISTS `skillsDatabase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skillsDatabase` (
  `skillName` varchar(300) DEFAULT NULL,
  `skillId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `addedByPate` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`skillId`)
) ENGINE=InnoDB AUTO_INCREMENT=621 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tokenTable`
--

DROP TABLE IF EXISTS `tokenTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokenTable` (
  `gkToken` varchar(600) NOT NULL,
  `linkedinToken` varchar(600) DEFAULT NULL,
  `linkedinSecret` varchar(600) DEFAULT NULL,
  `linkedinId` varchar(50) DEFAULT NULL,
  `timestamp` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`gkToken`),
  KEY `linkedinIDIndex` (`linkedinId`),
  CONSTRAINT `tokenTable_ibfk_1` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userDetails`
--

DROP TABLE IF EXISTS `userDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userDetails` (
  `linkedinId` varchar(50) NOT NULL DEFAULT '',
  `summary` longtext,
  `specialities` longtext,
  `lastModified` timestamp NULL DEFAULT NULL,
  `honors` longtext,
  `isoCountryCode` varchar(5) DEFAULT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `mfeedRssURL` varchar(500) DEFAULT NULL,
  `industry` varchar(500) DEFAULT NULL,
  `proposalComments` longtext,
  PRIMARY KEY (`linkedinId`),
  CONSTRAINT `userdetails_ibfk_1` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userEducationDetails`
--

DROP TABLE IF EXISTS `userEducationDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userEducationDetails` (
  `linkedinId` varchar(50) NOT NULL DEFAULT '',
  `linkedinEducationId` bigint(20) unsigned NOT NULL DEFAULT '0',
  `startYear` varchar(4) DEFAULT NULL,
  `endYear` varchar(4) DEFAULT NULL,
  `fieldOfStudy` longtext,
  `schoolName` varchar(500) DEFAULT NULL,
  `degree` varchar(500) DEFAULT NULL,
  `activities` longtext,
  `notes` longtext,
  PRIMARY KEY (`linkedinId`,`linkedinEducationId`),
  CONSTRAINT `usereducationdetails_ibfk_1` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userFileNames`
--

DROP TABLE IF EXISTS `userFileNames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userFileNames` (
  `fileId` varchar(500) NOT NULL,
  `fileName` varchar(500) DEFAULT NULL,
  `linkedinId` varchar(50) NOT NULL,
  PRIMARY KEY (`fileId`),
  KEY `linkedinIDIndex` (`linkedinId`),
  CONSTRAINT `userFileNames_ibfk_1` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE,
  CONSTRAINT `userFileNames_ibfk_2` FOREIGN KEY (`fileId`) REFERENCES `userFilesTable` (`fileId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userFilesTable`
--

DROP TABLE IF EXISTS `userFilesTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userFilesTable` (
  `fileId` varchar(500) NOT NULL,
  `file` longblob,
  `linkedinId` varchar(50) NOT NULL,
  PRIMARY KEY (`fileId`),
  KEY `linkedinIDIndex` (`linkedinId`),
  CONSTRAINT `userFilesTable_ibfk_1` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userPateAssociations`
--

DROP TABLE IF EXISTS `userPateAssociations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userPateAssociations` (
  `linkedinId` varchar(50) NOT NULL DEFAULT '',
  `pateKeyId` bigint(20) unsigned NOT NULL DEFAULT '0',
  `keyType` tinyint(1) DEFAULT NULL,
  `keyWeight` float DEFAULT NULL,
  PRIMARY KEY (`linkedinId`,`pateKeyId`),
  KEY `pateKeyId` (`pateKeyId`),
  CONSTRAINT `userpateassociations_ibfk_1` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE,
  CONSTRAINT `userpateassociations_ibfk_2` FOREIGN KEY (`pateKeyId`) REFERENCES `skillsDatabase` (`skillId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userPositionsDetails`
--

DROP TABLE IF EXISTS `userPositionsDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userPositionsDetails` (
  `linkedinId` varchar(50) NOT NULL DEFAULT '',
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `summary` longtext,
  `title` varchar(500) DEFAULT NULL,
  `linkedinCompanyId` bigint(20) unsigned DEFAULT NULL,
  `linkedinPositionId` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`linkedinId`,`linkedinPositionId`),
  KEY `linkedinCompanyId` (`linkedinCompanyId`),
  CONSTRAINT `userpositionsdetails_ibfk_1` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE,
  CONSTRAINT `userpositionsdetails_ibfk_2` FOREIGN KEY (`linkedinCompanyId`) REFERENCES `organizationsDatabase` (`linkedinCompanyId`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userProjects`
--

DROP TABLE IF EXISTS `userProjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userProjects` (
  `projectId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `projectName` varchar(400) DEFAULT NULL,
  `projectDescription` longtext,
  PRIMARY KEY (`projectId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userSkillsTable`
--

DROP TABLE IF EXISTS `userSkillsTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userSkillsTable` (
  `linkedinId` varchar(50) NOT NULL DEFAULT '',
  `skillId` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`linkedinId`,`skillId`),
  KEY `skillId` (`skillId`),
  CONSTRAINT `userSkillsTable_ibfk_1` FOREIGN KEY (`linkedinId`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE,
  CONSTRAINT `userSkillsTable_ibfk_2` FOREIGN KEY (`skillId`) REFERENCES `skillsDatabase` (`skillId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userconnectionstable`
--

DROP TABLE IF EXISTS `userconnectionstable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userconnectionstable` (
  `linkedinId1` varchar(50) NOT NULL DEFAULT '',
  `linkedinId2` varchar(50) NOT NULL DEFAULT '',
  `isLinkedIn` tinyint(1) NOT NULL,
  PRIMARY KEY (`linkedinId1`,`linkedinId2`),
  KEY `linkedinId2` (`linkedinId2`),
  KEY `linkedinId1` (`linkedinId1`),
  CONSTRAINT `userConnectionsTable_ibfk_1` FOREIGN KEY (`linkedinId1`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE,
  CONSTRAINT `userConnectionsTable_ibfk_2` FOREIGN KEY (`linkedinId2`) REFERENCES `usertable` (`linkedinId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usertable`
--

DROP TABLE IF EXISTS `usertable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usertable` (
  `linkedinId` varchar(50) NOT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `headline` longtext,
  `fileId` varchar(500) DEFAULT NULL,
  `pictureURL` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`linkedinId`),
  KEY `fileId` (`fileId`),
  CONSTRAINT `fileId` FOREIGN KEY (`fileId`) REFERENCES `userFilesTable` (`fileId`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-16 11:14:59
