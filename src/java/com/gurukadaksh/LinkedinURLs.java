/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh;

/**
 *
 * @author Deepak Subramanian
 */
public class LinkedinURLs
{
    public static final String CONNECTIONS_URL = "http://api.linkedin.com/v1/people/~/connections?format=json"; //:(id,last-name)
    public static final String SKILLS_URL = "http://api.linkedin.com/v1/people/~/skills?format=json";
    public static final String POSITIONS_URL = "http://api.linkedin.com/v1/people/~/position?format=json";
    public static final String ADVACED_PROFILE_URL = "http://api.linkedin.com/v1/people/~:(maiden-name,formatted-name,phonetic-first-name,phonetic-last-name,formatted-phonetic-name,current-share,summary,specialties,positions,email-address,last-modified-timestamp,proposal-comments,associations,honors,publications,patents,languages,certifications,educations,courses,volunteer,num-recommenders,recommendations-received,mfeed-rss-url,following,job-bookmarks,suggestions,date-of-birth,member-url-resources,related-profile-views,interests,industry,location:(country:(code)),phone-numbers,bound-account-types,im-accounts,main-address)?format=json";
    public static final String PROFILE_URL = "http://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,picture-url)?format=json";
    
    public static String getCompanyUrl(Long companyId)
    {
        final String COMPANY_URL = "http://api.linkedin.com/v1/companies/"; ///< This 
        final String COMPANY_URL_END = ":(id,name,ticker,description,specialties)?format=json"; 
        return COMPANY_URL + companyId + COMPANY_URL_END;
    }
    
    public static class LinkedInResponseParameters
    {
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String HEADLINE = "headline";
        public static final String PICTURE_URL = "pictureUrl";
        public static final String COUNTRY_OUTER = "country";
        public static final String LOCATION = "location";
        public static final String COUNTRY_CODE = "code";
        public static final String PERSON = "person";
        public static final String LINKEDIN_ID = "id";
        public static final String INDUSTRY = "industry";
        public static final String RESULT_LIST = "values";
        public static final String SKILL_DICTIONARY = "skill";
        public static final String SKILL_NAME = "name";
        public static final String SUMMARY = "summary";
        public static final String SPECIALITIES = "specialties";
        public static final String LAST_MODIFIED = "lastModifiedTimestamp";
        public static final String HONORS = "honors";
        public static final String DATE_OF_BIRTH = "dateOfBirth";
        public static final String MONTH = "month";
        public static final String DAY = "day";
        public static final String YEAR = "year";
        public static final String RSS_URL = "mfeedRssUrl";
        public static final String PROPOSAL_COMMENTS = "proposalComments";
        public static final String EDUCATION = "educations";
        public static final String ARRAY_VALUES = "values";
        public static final String EDUCATION_ID = "id";
        public static final String DATE_YEAR = "year";
        public static final String DATE_MONTH = "month";
        public static final String DATE_START = "startDate";
        public static final String DATE_END = "endDate";
        public static final String EDUCATION_FIELD_OF_STUDY = "fieldOfStudy";
        public static final String EDUCATION_SCHOOL_NAME = "schoolName";
        public static final String EDUCATION_DEGREE = "degree";
        public static final String EDUCATION_ACTIVITIES = "activities";
        public static final String EDUCATION_NOTES = "notes";
        public static final String POSITION = "positions";
        public static final String POSITION_SUMMARY = "summary";
        public static final String POSITION_ID = "id";
        public static final String POSITION_TITLE = "title";
        public static final String COMPANY = "company";
        public static final String COMPANY_ID  = "id";
        public static final String COMPANY_NAME = "name";
        public static final String COMPANY_TYPE = "type";
        public static final String COMPANY_INDUSTRY = "industry";   
        public static final String COMPANY_DESCRIPTION = "description";
        public static final String POSITION_ISCURRENT = "isCurrent";
    }
}
