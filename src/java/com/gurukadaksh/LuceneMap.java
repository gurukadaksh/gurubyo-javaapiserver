/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh;

/**
 *
 * @author deepaksubramanian
 */
public class LuceneMap
    {
        String id;
        String name;
        String type; //"Skill","Company","Person","Education"
        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public boolean equals(Object otherObject)
        {
            if (!(otherObject instanceof LuceneMap))
            {
                return false;
            }
            LuceneMap other = (LuceneMap) otherObject;
            if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId())))
            {
                return false;
            }
            if ((this.getName() == null && other.getName() != null) || (this.getName() != null && !this.getName().equals(other.getName())))
            {
                return false;
            }
            if ((this.getType() == null && other.getType() != null) || (this.getType() != null && !this.getType().equals(other.getType())))
            {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 29 * hash + (this.id != null ? this.id.hashCode() : 0);
            hash = 29 * hash + (this.name != null ? this.name.hashCode() : 0);
            hash = 29 * hash + (this.type != null ? this.type.hashCode() : 0);
            return hash;
        }

    }
