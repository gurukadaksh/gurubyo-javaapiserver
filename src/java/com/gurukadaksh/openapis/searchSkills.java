/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh.openapis;

import com.gurukadaksh.LuceneMap;
import com.gurukadaksh.LuceneOperations;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Deepak Subramanian
 */
public class searchSkills extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try
        {
            String i = request.getParameter("i");
            String searchParam = request.getParameter("q");
            String callbackFunction = request.getParameter("callback");
            if (i.equalsIgnoreCase("1"))
            {
                LuceneOperations luceneOperations = new LuceneOperations();
                luceneOperations.addCompleteSkillIndex();

                out.println("DONE");
            }
            else
            {
                response.setContentType("application/json;charset=UTF-8");
                LuceneOperations luceneOperations = new LuceneOperations();
                ArrayList<LuceneMap> lsm = null;
                switch (i)
                {
                    case "2":
                        lsm = luceneOperations.searchTerm(searchParam);
                        break;
                    case "3":
                        lsm = luceneOperations.fullTextSearch(searchParam);
                        break;
                    default:
                        lsm = null;
                        break;
                }
                JSONObject jsonObj = new JSONObject();
                JSONArray jarray = null;
                
                if (lsm != null)
                {
                    jarray = new JSONArray();
                    for (int j = 0; j < lsm.size(); j++)
                    {
                       JSONObject skill = new JSONObject();
                       skill.put("id", lsm.get(j).getId());
                       skill.put("name", lsm.get(j).getName());
                       skill.put("searchType",lsm.get(j).getType());
                       jarray.put(skill);
                    }
                }
                
                jsonObj.put("results",jarray );
                
                if (callbackFunction == null)
                {
                    out.print(jsonObj.toString());
                }
                else
                {
                    jsonObj.put("callback", callbackFunction);
                    out.print(callbackFunction+"("+jsonObj.toString()+")");
                }
            }
            
        }
        catch ( Exception exception )
        {
            out.print("Exception is"+exception);
        }
        finally
        {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
