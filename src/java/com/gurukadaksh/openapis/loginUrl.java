/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh.openapis;

import com.gurukadaksh.linkedInInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Deepak Subramanian
 */
public class loginUrl extends HttpServlet
{
    /** 
     * Processes requests for both HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processGetRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try
        {
            String oauthVerifier = request.getParameter("oauth_verifier");
            if(oauthVerifier != null)
            {
                String callbackUrl_UI = linkedInInfo.performLogin(request.getParameter("oauth_token"), oauthVerifier);
                response.sendRedirect(callbackUrl_UI);
            }
            else
            {                
                // <editor-fold defaultstate="collapsed" desc="testingCode">
                /*EntityManagerFactory enf = null;
                EntityManager em = null;
                enf = Persistence.createEntityManagerFactory("GurukadakshPU");
                em = enf.createEntityManager();
                em.getTransaction().begin();
                Usertable utable = new Usertable();
                utable.setLinkedinId("trial");
                utable.setFirstName("John");
                utable.setHeadline("Doe");
                utable.setLastName("Doe");
                utable.setPictureURL("http://something");
                em.persist(utable);
                em.getTransaction().commit();
                em.close();
                enf.close();*/// </editor-fold>
                processPostRequest(request, response);
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace(out);
        }
        finally
        {
            out.close();
        }
    }
/**
     * Processes requests for both HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processPostRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try
        {
            String redirectURL = request.getParameter("redirectUrl");
            JSONObject obj = new JSONObject();
            if(redirectURL == null) redirectURL = "http://localhost:80";
            obj.put("linkedinURL",linkedInInfo.getAuthenticationUrl(request.getScheme()+"://"+request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + request.getServletPath(), redirectURL));
            out.print(obj.toString());
        }
        catch (Exception exception)
        {
            Logger.getLogger(loginUrl.class.getName()).log(Level.SEVERE, null, exception);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            JSONObject obj = new JSONObject();
            try
            {
                obj.put("errorCause", exception.toString());
                out.print(obj.toString());
            }
            catch (JSONException ex)
            {
                Logger.getLogger(loginUrl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        finally
        {
            out.close();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processGetRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processPostRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
