/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.openapis;

import com.gurukadaksh.DBModel.Usertable;
import com.gurukadaksh.TokenManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author deepaksubramanian
 */
public class searchGurus extends HttpServlet {

    /**
     * Processes requests for  HTTP
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processPostRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        EntityManagerFactory enf = null;
        EntityManager em = null;

        try 
        {
            enf = Persistence.createEntityManagerFactory("GurukadakshPU");
            em = enf.createEntityManager();
            em.getTransaction().begin();   
            String projectId = request.getParameter("projectId");
            String gkToken = request.getParameter("gkToken");
            TokenManager tokenManager = new TokenManager(gkToken);
            Usertable userTable = tokenManager.getUserTable();
            if ( userTable != null )
            {
//                out.print("START");
                out.flush();
                //Query pateQuery = em.createNativeQuery("select usertable.linkedinId,usertable.firstname,usertable.lastname,userSkillsTable.skillid,skillsDatabase.skillname from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 left join userSkillsTable on userSkillsTable.linkedinid=usertable.linkedinid left join skillsDatabase on skillsDatabase.skillid=userSkillsTable.skillid where (userconnectionstable.linkedinId1 in (select usertable.linkedinId from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 where userconnectionstable.linkedinId1 = '5xxovp4bC1' or userconnectionstable.linkedinId2 = '5xxovp4bC1' group by usertable.linkedinid) or userconnectionstable.linkedinId2 in (select usertable.linkedinId from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 where userconnectionstable.linkedinId1 = '5xxovp4bC1' or userconnectionstable.linkedinId2 = '5xxovp4bC1' group by usertable.linkedinid)) and (usertable.linkedinid != '5xxovp4bC1') and (userSkillsTable.skillid in (1,44,73,45)) GROUP BY usertable.linkedinid");
                Query skillsQuery =  em.createNativeQuery("select DISTINCTROW usertable.linkedinId, usertable.pictureURL, usertable.firstname,usertable.lastname,userSkillsTable.skillid,skillsDatabase.skillname from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 left join userSkillsTable on userSkillsTable.linkedinid=usertable.linkedinid left join skillsDatabase on skillsDatabase.skillid=userSkillsTable.skillid where (userconnectionstable.linkedinId1 in (select DISTINCT usertable.linkedinId from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 where userconnectionstable.linkedinId1= ? or userconnectionstable.linkedinId2= ? ) or userconnectionstable.linkedinId2 in (select usertable.linkedinId from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 where userconnectionstable.linkedinId1= ? or userconnectionstable.linkedinId2 = ? )) and (usertable.linkedinid != ? ) and (userSkillsTable.skillid in (select projectSkills.skillId from projectSkills where projectSkills.projectId = ? UNION select projectPateAssociations.pateKeyId from projectPateAssociations where projectPateAssociations.projectId = ?))");//(1,44,73,45))");
                skillsQuery.setParameter(1, userTable.getLinkedinId());
                skillsQuery.setParameter(2, userTable.getLinkedinId());
                skillsQuery.setParameter(3, userTable.getLinkedinId());
                skillsQuery.setParameter(4, userTable.getLinkedinId());
                skillsQuery.setParameter(5, userTable.getLinkedinId());
                skillsQuery.setParameter(6, Long.parseLong(projectId));
                skillsQuery.setParameter(7, Long.parseLong(projectId));
                List<Object[]> skillsResultList = skillsQuery.getResultList();
                
                Query pateQuery =  em.createNativeQuery("select DISTINCTROW usertable.linkedinId, usertable.pictureURL, usertable.firstname,usertable.lastname,userPateAssociations.pateKeyId, userPateAssociations.keyType, userPateAssociations.keyWeight ,skillsDatabase.skillname from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 left join userPateAssociations on userPateAssociations.linkedinid=usertable.linkedinid left join skillsDatabase on skillsDatabase.skillid=userPateAssociations.pateKeyId where (userconnectionstable.linkedinId1 in (select DISTINCT usertable.linkedinId from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 where userconnectionstable.linkedinId1 = ? or userconnectionstable.linkedinId2 = ?) or userconnectionstable.linkedinId2 in (select usertable.linkedinId from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 where userconnectionstable.linkedinId1 = ? or userconnectionstable.linkedinId2 = ?)) and (usertable.linkedinid != ?) and (userPateAssociations.pateKeyId in (select projectSkills.skillId from projectSkills where projectSkills.projectId = ?  UNION select projectPateAssociations.pateKeyId from projectPateAssociations where projectPateAssociations.projectId = ?))");//(1,44,73,45))");
                pateQuery.setParameter(1, userTable.getLinkedinId());
                pateQuery.setParameter(2, userTable.getLinkedinId());
                pateQuery.setParameter(3, userTable.getLinkedinId());
                pateQuery.setParameter(4, userTable.getLinkedinId());
                pateQuery.setParameter(5, userTable.getLinkedinId());
                pateQuery.setParameter(6, Long.parseLong(projectId));
                pateQuery.setParameter(7, Long.parseLong(projectId));
                List<Object[]> pateResultList = pateQuery.getResultList();
                
                //Company details
                Query companySkillsQuery = em.createNativeQuery("select DISTINCTROW usertable.linkedinId, usertable.pictureURL, usertable.firstname, usertable.lastname, organizationSkillsTable.skillId, organizationsDatabase.linkedinCompanyId, organizationsDatabase.organizationName, skillsDatabase.skillName from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 left join userPositionsDetails on userPositionsDetails.linkedinId=usertable.linkedinId left join organizationsDatabase on organizationsDatabase.linkedinCompanyId=userPositionsDetails.linkedinCompanyId left join organizationSkillsTable on organizationSkillsTable.linkedinCompanyId=organizationsDatabase.linkedinCompanyId left join skillsDatabase on skillsDatabase.skillId=organizationSkillsTable.skillId where (userconnectionstable.linkedinId1 in (select DISTINCT usertable.linkedinId from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 where userconnectionstable.linkedinId1 = ? or userconnectionstable.linkedinId2 = ?) or userconnectionstable.linkedinId2 in (select usertable.linkedinId from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 where userconnectionstable.linkedinId1 = ? or userconnectionstable.linkedinId2 = ?)) and (usertable.linkedinid != ?) and (organizationSkillsTable.skillId in (select projectSkills.skillId from projectSkills where projectSkills.projectId = ?  UNION select projectPateAssociations.pateKeyId from projectPateAssociations where projectPateAssociations.projectId = ?))");
                companySkillsQuery.setParameter(1, userTable.getLinkedinId());
                companySkillsQuery.setParameter(2, userTable.getLinkedinId());
                companySkillsQuery.setParameter(3, userTable.getLinkedinId());
                companySkillsQuery.setParameter(4, userTable.getLinkedinId());
                companySkillsQuery.setParameter(5, userTable.getLinkedinId());
                companySkillsQuery.setParameter(6, Long.parseLong(projectId));
                companySkillsQuery.setParameter(7, Long.parseLong(projectId));
                List<Object[]> companySkillsResultList = companySkillsQuery.getResultList();
                
                Query companyPateQuery = em.createNativeQuery("select DISTINCTROW usertable.linkedinId, usertable.pictureURL, usertable.firstname, usertable.lastname, organizationPateAssociations.pateKeyId, organizationPateAssociations.keyType, organizationPateAssociations.keyWeight, organizationsDatabase.linkedinCompanyId, organizationsDatabase.organizationName,skillsDatabase.skillName from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 left join userPositionsDetails on userPositionsDetails.linkedinId=usertable.linkedinId left join organizationsDatabase on organizationsDatabase.linkedinCompanyId=userPositionsDetails.linkedinCompanyId left join organizationPateAssociations on organizationPateAssociations.linkedinCompanyId=organizationsDatabase.linkedinCompanyId left join skillsDatabase on skillsDatabase.skillId=organizationPateAssociations.pateKeyId where (userconnectionstable.linkedinId1 in (select DISTINCT usertable.linkedinId from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 where userconnectionstable.linkedinId1 = ? or userconnectionstable.linkedinId2 = ?) or userconnectionstable.linkedinId2 in (select usertable.linkedinId from usertable left join userconnectionstable on usertable.linkedinid=userconnectionstable.linkedinid1 or usertable.linkedinid=userconnectionstable.linkedinid2 where userconnectionstable.linkedinId1 = ? or userconnectionstable.linkedinId2 = ?)) and (usertable.linkedinid != ?) and (organizationPateAssociations.pateKeyId in (select projectSkills.skillId from projectSkills where projectSkills.projectId = ?  UNION select projectPateAssociations.pateKeyId from projectPateAssociations where projectPateAssociations.projectId = ?))");
                companyPateQuery.setParameter(1, userTable.getLinkedinId());
                companyPateQuery.setParameter(2, userTable.getLinkedinId());
                companyPateQuery.setParameter(3, userTable.getLinkedinId());
                companyPateQuery.setParameter(4, userTable.getLinkedinId());
                companyPateQuery.setParameter(5, userTable.getLinkedinId());
                companyPateQuery.setParameter(6, Long.parseLong(projectId));
                companyPateQuery.setParameter(7, Long.parseLong(projectId));
                List<Object[]> companyPateResultList = companyPateQuery.getResultList();
                
                HashMap<String, UserScore> userMap = new HashMap<String, UserScore>();
                //Adding the various skills
                for (int i = 0 ; i < skillsResultList.size() ; i++ ) 
                {
                    Object[] current = skillsResultList.get(i);      
//                    for(int j = 0; j < current.length ; j++)
//                    {
//                        out.print(current[j]+",");
//                    }
//                    
//                    out.println("<br>");
                    //HashMap<String, UserScore> userMap = new HashMap<String, UserScore>();
                    UserScore userScore = userMap.get((String) current[0]);
                    if ( userScore == null)
                    {
                        userScore = new UserScore();
                        userScore.linkedinId = (String) current[0];
                        userScore.linkedinProfilePictureUrl = (String) current[1];
                        userScore.firstName = (String) current[2];
                        userScore.lastName = (String) current[3];
                        userScore.pateSkillsFound = new ArrayList<PateSkill>();
                        userScore.skillsFound = new ArrayList<Long>();
                        userScore.companySkillsFound = new ArrayList<CompanySkill>();
                        userScore.companyPateSkillFound = new ArrayList<CompanyPateSkill>();
                        userMap.put(userScore.linkedinId, userScore);
                    }
//                    out.print("<br>Current 4 "+  current[4].toString());
                    userScore.skillsFound.add(Long.parseLong(current[4].toString())); 
                }
                
                //Adding the various pateSkills
                
                for (int i = 0 ; i < pateResultList.size() ; i++ ) 
                {
                    Object[] current = pateResultList.get(i);                   
//                    out.println("<br>");
                    
                    UserScore userScore = userMap.get((String) current[0]);
                    if ( userScore == null)
                    {
                        userScore = new UserScore();
                        userScore.linkedinId = (String) current[0];
                        userScore.linkedinProfilePictureUrl = (String) current[1];
                        userScore.firstName = (String) current[2];
                        userScore.lastName = (String) current[3];
                        userScore.pateSkillsFound = new ArrayList<PateSkill>(); 
                        userScore.skillsFound = new ArrayList<Long>();
                        userScore.companySkillsFound = new ArrayList<CompanySkill>();
                        userScore.companyPateSkillFound = new ArrayList<CompanyPateSkill>();
                        userMap.put(userScore.linkedinId, userScore);
                    }
//                    out.print(current[5]);
                    userScore.pateSkillsFound.add(new PateSkill(Long.parseLong(current[4].toString()),  Double.parseDouble( current[6].toString() ),(Boolean) current[5])); 
                }
//                out.print("Result");
                //Adding the Company Skills
                
                for (int i = 0 ; i < companySkillsResultList.size() ; i++ ) 
                {
                    Object[] current = companySkillsResultList.get(i);                   
//                    out.println("<br>");
                    
                    UserScore userScore = userMap.get((String) current[0]);
                    //0 - usertable.linkedinId, 1 - usertable.pictureURL,2- usertable.firstname, 3 - usertable.lastname, 4 - organizationSkillsTable.skillId, 5 - organizationsDatabase.linkedinCompanyId, 6 - organizationsDatabase.organizationName, 7 - skillsDatabase.skillName
                    if ( userScore == null)
                    {
                        userScore = new UserScore();
                        userScore.linkedinId = (String) current[0];
                        userScore.linkedinProfilePictureUrl = (String) current[1];
                        userScore.firstName = (String) current[2];
                        userScore.lastName = (String) current[3];
                        userScore.pateSkillsFound = new ArrayList<PateSkill>(); 
                        userScore.skillsFound = new ArrayList<Long>();
                        userScore.companySkillsFound = new ArrayList<CompanySkill>();
                        userScore.companyPateSkillFound = new ArrayList<CompanyPateSkill>();
                        userMap.put(userScore.linkedinId, userScore);
                    }
//                    out.print(current[5]);
//                    userScore.pateSkillsFound.add(new PateSkill(Long.parseLong(current[4].toString()),  Double.parseDouble( current[6].toString() ),(Boolean) current[5])); 
                    userScore.companySkillsFound.add(new CompanySkill(Long.parseLong(current[4].toString()), current[6].toString(), current[5].toString()));
                }
                
                //Adding the Company  PATE Skills
                for (int i = 0 ; i < companyPateResultList.size() ; i++ ) 
                {
                    Object[] current = companyPateResultList.get(i);                   
//                    out.println("<br>");
                    
                    UserScore userScore = userMap.get((String) current[0]);
                    // 0 - usertable.linkedinId, 1 - usertable.pictureURL, 2 - usertable.firstname, 3 - usertable.lastname, 4 - organizationPateAssociations.pateKeyId, 5 - organizationPateAssociations.keyType, 6 - organizationPateAssociations.keyWeight, 7 - organizationsDatabase.linkedinCompanyId, 8 - organizationsDatabase.organizationName, 9 - skillsDatabase.skillName
                    if ( userScore == null)
                    {
                        userScore = new UserScore();
                        userScore.linkedinId = (String) current[0];
                        userScore.linkedinProfilePictureUrl = (String) current[1];
                        userScore.firstName = (String) current[2];
                        userScore.lastName = (String) current[3];
                        userScore.pateSkillsFound = new ArrayList<PateSkill>(); 
                        userScore.skillsFound = new ArrayList<Long>();
                        userScore.companySkillsFound = new ArrayList<CompanySkill>();
                        userScore.companyPateSkillFound = new ArrayList<CompanyPateSkill>();
                        userMap.put(userScore.linkedinId, userScore);
                    }
//                    out.print(current[5]);
//                    userScore.pateSkillsFound.add(new PateSkill(Long.parseLong(current[4].toString()),  Double.parseDouble( current[6].toString() ),(Boolean) current[5])); 
//                    userScore.companySkillsFound.add(new CompanySkill(Long.parseLong(current[4].toString()), current[6].toString(), current[5].toString()));
                    userScore.companyPateSkillFound.add(new CompanyPateSkill(new PateSkill(Long.parseLong(current[4].toString()),  Double.parseDouble( current[6].toString() ),(Boolean) current[5]), current[8].toString(), current[7].toString()));
                }
                JSONArray returnArray = new JSONArray();
                Object[] keySet = userMap.keySet().toArray();
                ArrayList<UserScore> values = new ArrayList<>(userMap.values());
                Collections.sort(values, new UserScore());
                for ( UserScore current : values )
                {
                    
                    JSONObject returnObject = new JSONObject();
                   // UserScore current = userMap.get(keySet[i].toString());
                    returnObject.putOpt("FirstName",current.firstName);
                    returnObject.putOpt("LastName",current.lastName);
                    returnObject.putOpt("PictureURL",current.linkedinProfilePictureUrl);
                    returnObject.putOpt("SkillsFound", new JSONArray(current.skillsFound));
                    returnObject.putOpt("PateSkillsFound", new JSONArray(current.pateSkillsFound));
                    returnObject.putOpt("OrganizationSkillsFound", new JSONArray(current.companySkillsFound));
                    returnObject.putOpt("OrganizationPateSkillsFound", new JSONArray(current.companyPateSkillFound));
                    returnObject.putOpt("Score",current.calculatedScore);
                    returnArray.put(returnObject);
                }
                
                out.print(returnArray.toString());
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Guru Return Array is {0}",returnArray.toString());
            }
        }
        catch (Exception exception)
        {
            out.print("Exception is "+exception);
            em.getTransaction().rollback();
        }
        finally {            
            out.close();
            em.close();
            enf.close();
        }
    }
        /**
     * Processes requests for  HTTP
     * <code>GET</code> and
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processGetRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            processPostRequest(request, response);
           //response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } finally {            
            out.close();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processGetRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processPostRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}


class UserScore implements Comparator<Object>
{


     ArrayList<Long> skillsFound; ///< The various skills that were found will be mapped here.
     ArrayList<PateSkill> pateSkillsFound; ///< The various pate skills mapped to the user will be mapped here.
     ArrayList<CompanySkill> companySkillsFound; ///< The various skills associated to the company the user worked in.
     ArrayList<CompanyPateSkill> companyPateSkillFound; ///< The various pate skills associated to the company the user w
     String linkedinId; ///< The linkedinId that is mapped to the user
     String linkedinProfilePictureUrl; ///< The profile picture as required by the api call result
     String firstName; ///< The first name as required by the api call result.
     String lastName; ///< The last name as required by the api call result.
     float calculatedScore;
     public void calculateScore()
     {
         float skillsWeight = 1;
         float pateSkillsWeight = (float) 0.9;
         float companySkillWeight = 0.7f;
         float companyPateSkillWeight = 0.5f;
         float userPateValue = 0f;
         for (PateSkill pskill : pateSkillsFound)
         {
             userPateValue += pskill.weight;
         }
         float companyPateValue = 0f;
         for ( CompanyPateSkill cpskill : companyPateSkillFound)
         {
             companyPateValue += cpskill.skill.weight;
         }
         calculatedScore = (skillsWeight * skillsFound.size())+(pateSkillsWeight*userPateValue)+(companySkillWeight*companySkillsFound.size())+(companyPateSkillWeight*companyPateValue);
     }
     
    @Override
    /**
     * reverse order
     */
     public int compare(Object o1, Object o2) {
        UserScore score1 = (UserScore) o1;
        UserScore score2 = (UserScore) o2;
        score1.calculateScore();score2.calculateScore();
        return ((score1.calculatedScore == score2.calculatedScore) ? 0 : (score1.calculatedScore > score2.calculatedScore) ? -1 : 1);
    }


}

class CompanySkill
{
        Long skillId; ///< The skillId that is used in the companySkill
        String companyName; ///< The company name 
        String companyId; ///< the unique linkedin id of the company
        public CompanySkill(Long skillId, String companyName, String companyId) 
        {
            this.skillId = skillId; 
            this.companyName = companyName;
            this.companyId = companyId;
        }
    }
class CompanyPateSkill 
{
        String companyName;
        String companyId;
        PateSkill skill;

    public CompanyPateSkill( PateSkill skill, String companyName, String companyId) 
    {
        this.companyName = companyName;
        this.companyId = companyId;
        this.skill = skill;
    }
        
}
class PateSkill
{
    Long skillId; ///< The skillId
    double weight; ///< the weight
    boolean keyType; ///< 1 primary key

    public PateSkill(Long skillId, double weight, boolean keyType)
    {
        this.skillId = skillId;
        this.weight = weight;
        this.keyType = keyType;
    }

    @Override
    public boolean equals(Object o) ///< Not yet completed
    {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + (this.skillId != null ? this.skillId.hashCode() : 0);
        return hash;
    }
   
    
    
    
}