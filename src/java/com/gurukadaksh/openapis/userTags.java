/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.openapis;

import com.gurukadaksh.DBModel.OrganizationPateAssociations;
import com.gurukadaksh.DBModel.SkillsDatabase;
import com.gurukadaksh.DBModel.UserPateAssociations;
import com.gurukadaksh.DBModel.UserPositionsDetails;
import com.gurukadaksh.DBModel.Usertable;
import com.gurukadaksh.TokenManager;
import com.gurukadaksh.linkedInInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;

/**
 *
 * @author karthik
 */
public class userTags extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        EntityManagerFactory enf = null;
        EntityManager em = null;
        try {
            Logger.getLogger(createProject.class.getName()).log(Level.SEVERE, "H9 ");
            enf = Persistence.createEntityManagerFactory("GurukadakshPU");
            em = enf.createEntityManager();
            em.getTransaction().begin();   
            String gkToken = request.getParameter(linkedInInfo.GURUKADAKSH_TOKEN);
            TokenManager tokenManager = new TokenManager(gkToken);
            Usertable userTable = tokenManager.getUserTable();
            Collection<UserPateAssociations> givenUserAssoc = userTable.getUserPateAssociationsCollection();
            Collection<SkillsDatabase> givenUserSkills = userTable.getSkillsDatabaseCollection();
            Collection<UserPositionsDetails> givenUserPos = userTable.getUserPositionsDetailsCollection();
            
            JSONArray keywordsArray = new JSONArray();
            for (SkillsDatabase currentSkill : givenUserSkills)
            {
                keywordsArray.put(currentSkill.getSkillName());
            }
            Logger.getLogger(createProject.class.getName()).log(Level.SEVERE, "H91 ");
            for (UserPateAssociations currentPate : givenUserAssoc)
            {
                keywordsArray.put(currentPate.getSkillsDatabase().getSkillName());
            }
            Logger.getLogger(createProject.class.getName()).log(Level.SEVERE, "H92 ");
            for (UserPositionsDetails currentPos : givenUserPos)
            {
                if (currentPos.getLinkedinCompanyId()!=null)
                {
                Collection<SkillsDatabase> givenOrgSkills = currentPos.getLinkedinCompanyId().getSkillsDatabaseCollection();
                Collection<OrganizationPateAssociations> givenOrgAssoc = currentPos.getLinkedinCompanyId().getOrganizationPateAssociationsCollection();
                
                for (SkillsDatabase currentSkill : givenOrgSkills)
                {
                    keywordsArray.put(currentSkill.getSkillName());
                }
                Logger.getLogger(createProject.class.getName()).log(Level.SEVERE, "H921 " );
                for (OrganizationPateAssociations currentPate : givenOrgAssoc)
                {
                    SkillsDatabase currentPateSkill = em.find(SkillsDatabase.class, currentPate.getOrganizationPateAssociationsPK().getPateKeyId());
                    keywordsArray.put(currentPateSkill.getSkillName());
                }
                Logger.getLogger(createProject.class.getName()).log(Level.SEVERE, "H922 ");
                }
            }
            Logger.getLogger(createProject.class.getName()).log(Level.SEVERE, "H10 ");
            out.print(keywordsArray.toString());
        } 
        catch (Exception ex)
        {
            Logger.getLogger(createProject.class.getName()).log(Level.SEVERE, "Hari Exception is " + ex);
        }
        finally {            
            out.close();
            em.close();
            enf.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
