/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh.openapis;

import com.gurukadaksh.DBModel.CustomCreated.*;
import com.gurukadaksh.DBModel.UserProjects;
import com.gurukadaksh.DBModel.Usertable;
import com.gurukadaksh.TokenManager;
import com.gurukadaksh.linkedInInfo;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Deepak Subramanian
 */
public class createProject extends HttpServlet {
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        EntityManagerFactory enf = null;
        EntityManager em = null;
        try 
        {
            enf = Persistence.createEntityManagerFactory("GurukadakshPU");
            em = enf.createEntityManager();
            em.getTransaction().begin();   
            String gkToken = request.getParameter(linkedInInfo.GURUKADAKSH_TOKEN);
            TokenManager tokenManager = new TokenManager(gkToken);
            Usertable userTable = tokenManager.getUserTable();
            String description = request.getParameter("description");
            String title = request.getParameter("title");
            String keywordsJSON = request.getParameter("keywords");
            JSONArray keywordsArray = null;
            
            //out.print("1\n");
            UserProjects userProjects = new UserProjects();            
            userProjects.setProjectDescription(description);
            userProjects.setProjectName(title);
            em.persist(userProjects);           
            em.flush();    
            //out.print("2\n");
            JSONObject resultObject = new JSONObject();
            resultObject.put("projectId", userProjects.getProjectId());
            out.print(resultObject.toString());//"The generated project id is : "+userProjects.getProjectId());
            ProjectStudent pstudent = new ProjectStudent();
            pstudent.setId(new ProjectStudentPK(userTable.getLinkedinId(), userProjects.getProjectId()));
            em.persist(pstudent);
            em.flush();
            //Sample KeywordsJSON - [{"id":1,searchType="Skill"},{"id":2,"searchType":"Skill"}]
            if (keywordsJSON != null && !keywordsJSON.trim().isEmpty())
            {
                try
                {
                    keywordsArray = new JSONArray(keywordsJSON);
                    for ( int i = 0 ; i < keywordsArray.length() ; i++ )
                    {
                        Logger.getLogger(createProject.class.getName()).log(Level.SEVERE, "JSON is " + keywordsJSON);
                        JSONObject idDict = keywordsArray.optJSONObject(i);
                        if ( idDict != null )
                        {
                            
                            String id = idDict.optString("id");
                            String searchType = idDict.optString("searchType");
                            
                            if ( id != null && searchType != null )
                            {
                                if (searchType.equalsIgnoreCase("Skill"))
                                {
                                    ProjectSkills projectSkill = new ProjectSkills();
                                    projectSkill.setId(new ProjectSkillsPK(Long.parseLong(id), userProjects.getProjectId()));
                                    em.persist(projectSkill);
                                    em.flush();
                                }
//                                else if (searchType.equalsIgnoreCase("Pate"))
//                                {
//                                    ProjectPateAssociations projectPateAssociations = new ProjectPateAssociations();
//                                    projectPateAssociations.setId(new ProjectPateAssociationsPK(Long.parseLong(id), userProjects.getProjectId()));
//                                    em.persist(projectPateAssociations);
//                                    em.flush();
//                                }
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                   //out.print("Failure because: "+exception.getMessage()); 
                   Logger.getLogger(createProject.class.getName()).log(Level.SEVERE, "Exception is " + exception);
                }
                finally
                {
                    
                }
            }
            em.getTransaction().commit(); 
            //CALL PATE FOR PROJECTS
            callPate(userProjects.getProjectId().toString());
            
        } 
        catch (Exception exception)
        {
            em.getTransaction().rollback();
            //out.print("Failure because: "+exception.getMessage());
            Logger.getLogger(createProject.class.getName()).log(Level.SEVERE, "Exception is {0}", exception);
        }
        finally { 
            out.close();
            em.close();
            enf.close();
        }
    } 

     /**
     * Calls the python implementation of PATE.
     * @param utable 
     */
    private void callPate(String id) 
    {
        try
        {
            String url = "http://localhost:7283";
            URL connectTo = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) connectTo.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(true);
            String parameterString = "id="+id+"&analysisType=projectPate";
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length", "" + Integer.toString(parameterString.getBytes().length));
            connection.setRequestMethod("POST");
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
            wr.writeBytes(parameterString);
            wr.flush();
            wr.close();
            DataInputStream inStream = new DataInputStream(connection.getInputStream());
            String buffer;
            String connectionOutput = "";
            while((buffer = inStream.readLine()) != null)
            {
                connectionOutput += buffer + "\n";
            }
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "callPate answer {0}",connectionOutput);

            connection.disconnect();
        }
        catch ( Exception exception )
        {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "callPate Exception {0}",exception.getMessage());
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
