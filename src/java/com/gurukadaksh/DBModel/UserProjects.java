/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "userProjects", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserProjects.findAll", query = "SELECT u FROM UserProjects u"),
    @NamedQuery(name = "UserProjects.findByProjectId", query = "SELECT u FROM UserProjects u WHERE u.projectId = :projectId"),
    @NamedQuery(name = "UserProjects.findByProjectName", query = "SELECT u FROM UserProjects u WHERE u.projectName = :projectName")})
public class UserProjects implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@Basic(optional = false)
    //@NotNull
    @Column(name = "projectId")
    private Long projectId;
    @Size(max = 400)
    @Column(name = "projectName")
    private String projectName;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "projectDescription")
    private String projectDescription;
    @JoinTable(name = "projectSkills", joinColumns = {
        @JoinColumn(name = "projectId", referencedColumnName = "projectId")}, inverseJoinColumns = {
        @JoinColumn(name = "skillId", referencedColumnName = "skillId")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<SkillsDatabase> skillsDatabaseCollection;
    @JoinTable(name = "projectPateAssociations", joinColumns = {
        @JoinColumn(name = "projectId", referencedColumnName = "projectId")}, inverseJoinColumns = {
        @JoinColumn(name = "pateKeyId", referencedColumnName = "pateKeyId")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<PateKeywordsDatabase> pateKeywordsDatabaseCollection;
    @JoinTable(name = "projectGuru", joinColumns = {
        @JoinColumn(name = "projectId", referencedColumnName = "projectId")}, inverseJoinColumns = {
        @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<Usertable> usertableCollection;
    @JoinTable(name = "projectInvites", joinColumns = {
        @JoinColumn(name = "projectId", referencedColumnName = "projectId")}, inverseJoinColumns = {
        @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<Usertable> usertableCollection1;
    @JoinTable(name = "projectStudent", joinColumns = {
        @JoinColumn(name = "projectId", referencedColumnName = "projectId")}, inverseJoinColumns = {
        @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<Usertable> usertableCollection2;
    @OneToMany(mappedBy = "projectId", fetch = FetchType.LAZY)
    private Collection<MessageCenter> messageCenterCollection;

    public UserProjects() {
    }

    public UserProjects(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    @XmlTransient
    public Collection<SkillsDatabase> getSkillsDatabaseCollection() {
        return skillsDatabaseCollection;
    }

    public void setSkillsDatabaseCollection(Collection<SkillsDatabase> skillsDatabaseCollection) {
        this.skillsDatabaseCollection = skillsDatabaseCollection;
    }

    @XmlTransient
    public Collection<PateKeywordsDatabase> getPateKeywordsDatabaseCollection() {
        return pateKeywordsDatabaseCollection;
    }

    public void setPateKeywordsDatabaseCollection(Collection<PateKeywordsDatabase> pateKeywordsDatabaseCollection) {
        this.pateKeywordsDatabaseCollection = pateKeywordsDatabaseCollection;
    }

    @XmlTransient
    public Collection<Usertable> getUsertableCollection() {
        return usertableCollection;
    }

    public void setUsertableCollection(Collection<Usertable> usertableCollection) {
        this.usertableCollection = usertableCollection;
    }

    @XmlTransient
    public Collection<Usertable> getUsertableCollection1() {
        return usertableCollection1;
    }

    public void setUsertableCollection1(Collection<Usertable> usertableCollection1) {
        this.usertableCollection1 = usertableCollection1;
    }

    @XmlTransient
    public Collection<Usertable> getUsertableCollection2() {
        return usertableCollection2;
    }

    public void setUsertableCollection2(Collection<Usertable> usertableCollection2) {
        this.usertableCollection2 = usertableCollection2;
    }

    @XmlTransient
    public Collection<MessageCenter> getMessageCenterCollection() {
        return messageCenterCollection;
    }

    public void setMessageCenterCollection(Collection<MessageCenter> messageCenterCollection) {
        this.messageCenterCollection = messageCenterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (projectId != null ? projectId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserProjects)) {
            return false;
        }
        UserProjects other = (UserProjects) object;
        if ((this.projectId == null && other.projectId != null) || (this.projectId != null && !this.projectId.equals(other.projectId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserProjects[ projectId=" + projectId + " ]";
    }
    
}
