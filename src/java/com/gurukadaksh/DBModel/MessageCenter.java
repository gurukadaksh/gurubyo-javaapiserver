/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "messageCenter", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MessageCenter.findAll", query = "SELECT m FROM MessageCenter m"),
    @NamedQuery(name = "MessageCenter.findByMessageId", query = "SELECT m FROM MessageCenter m WHERE m.messageId = :messageId"),
    @NamedQuery(name = "MessageCenter.findByDatetime", query = "SELECT m FROM MessageCenter m WHERE m.datetime = :datetime")})
public class MessageCenter implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "Datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datetime;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "messageId")
    private String messageId;
    @JoinTable(name = "messageRecievers", joinColumns = {
        @JoinColumn(name = "messageId", referencedColumnName = "messageId")}, inverseJoinColumns = {
        @JoinColumn(name = "toId", referencedColumnName = "linkedinId")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<Usertable> usertableCollection;
    @JoinColumn(name = "fromId", referencedColumnName = "linkedinId")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usertable fromId;
    @JoinColumn(name = "projectId", referencedColumnName = "projectId")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserProjects projectId;

    public MessageCenter() {
    }

    public MessageCenter(String messageId) {
        this.messageId = messageId;
    }

    public MessageCenter(String messageId, Date datetime) {
        this.messageId = messageId;
        this.datetime = datetime;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    @XmlTransient
    public Collection<Usertable> getUsertableCollection() {
        return usertableCollection;
    }

    public void setUsertableCollection(Collection<Usertable> usertableCollection) {
        this.usertableCollection = usertableCollection;
    }

    public Usertable getFromId() {
        return fromId;
    }

    public void setFromId(Usertable fromId) {
        this.fromId = fromId;
    }

    public UserProjects getProjectId() {
        return projectId;
    }

    public void setProjectId(UserProjects projectId) {
        this.projectId = projectId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (messageId != null ? messageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MessageCenter)) {
            return false;
        }
        MessageCenter other = (MessageCenter) object;
        if ((this.messageId == null && other.messageId != null) || (this.messageId != null && !this.messageId.equals(other.messageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.MessageCenter[ messageId=" + messageId + " ]";
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }
    
}
