/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel.CustomCreated;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "projectGuru", catalog = "gurukadakshDB", schema = "")
@NamedQueries({
    @NamedQuery(name = "ProjectGuru.findAll", query = "SELECT p FROM ProjectGuru p")
})
public class ProjectGuru implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    //@GeneratedValue(strategy = GenerationType.AUTO)
    private ProjectGuruPK id;

    public ProjectGuruPK getId() {
        return id;
    }

    public void setId(ProjectGuruPK id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectGuru)) {
            return false;
        }
        ProjectGuru other = (ProjectGuru) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.CustomCreated.ProjectGuru[ id=" + id + " ]";
    }
    
}
