/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh.DBModel.CustomCreated;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Deepak Subramanian
 */
@Entity
@Table(name = "userSkillsTable", catalog = "gurukadakshDB", schema = "")
@NamedQueries({
    @NamedQuery(name = "UserSkillsTable.findAll", query = "SELECT t FROM TokenTable t")
})
public class UserSkillsTable implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "linkedinId")
    private String linkedinId; ///< LinkedInId is the 
    
    @Basic(optional = false)
    @Column(name = "skillId")
    private Long skillId;

    public UserSkillsTable() {
    }

    public UserSkillsTable(String linkedinId, Long skillId)
    {
        this.linkedinId = linkedinId;
        this.skillId = skillId;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public Long getSkillId() {
        return skillId;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }

    
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (linkedinId != null ? linkedinId.hashCode() : 0);
        hash += (skillId != null ? skillId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserSkillsTable)) {
            return false;
        }
        UserSkillsTable other = (UserSkillsTable) object;
        if ((this.linkedinId == null && other.linkedinId != null) || (this.linkedinId != null && !this.linkedinId.equals(other.linkedinId)))
        {
            return false;
        }
        if ((this.skillId == null && other.skillId != null) || (this.skillId != null && !this.skillId.equals(other.skillId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.gurukadaksh.DBModel.Custom.UserSkillsTable[linkedinId=" + this.linkedinId + ", skillId=" + this.skillId + "]";
    }

}
