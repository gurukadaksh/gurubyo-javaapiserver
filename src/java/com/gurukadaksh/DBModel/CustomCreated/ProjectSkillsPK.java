/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel.CustomCreated;


import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author deepaksubramanian
 */
@Embeddable
public class ProjectSkillsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "skillId")
    private long skillId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "projectId")
    private long projectId;

    public ProjectSkillsPK() {
    }

    public ProjectSkillsPK(long skillId, long projectId) {
        this.skillId = skillId;
        this.projectId = projectId;
    }

    public long getSkillId() {
        return skillId;
    }

    public void setSkillId(long skillId) {
        this.skillId = skillId;
    }



    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) skillId;
        hash += (int) projectId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof com.gurukadaksh.DBModel.CustomCreated.ProjectSkillsPK)) {
            return false;
        }
        com.gurukadaksh.DBModel.CustomCreated.ProjectSkillsPK other = (com.gurukadaksh.DBModel.CustomCreated.ProjectSkillsPK) object;
        if (this.skillId != other.skillId) {
            return false;
        }
        if (this.projectId != other.projectId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.ProjectSkillsPK[ skillId=" + skillId + ", projectId=" + projectId + " ]";
    }
    
}
