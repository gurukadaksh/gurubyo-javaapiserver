/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel.CustomCreated;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "organizationSkillsTable", catalog = "gurukadakshDB", schema = "")
@NamedQueries({
    @NamedQuery(name = "organizationSkills.findAll", query = "SELECT o FROM OrganizationSkills o")
})
public class OrganizationSkills implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    //@GeneratedValue(strategy = GenerationType.AUTO)
    private OrganizationSkillsPK id;

    public OrganizationSkillsPK getId() {
        return id;
    }

    public void setId(OrganizationSkillsPK id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganizationSkills)) {
            return false;
        }
        OrganizationSkills other = (OrganizationSkills) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.CustomCreated.ProjectSkills[ id=" + id + " ]";
    }
    
}
