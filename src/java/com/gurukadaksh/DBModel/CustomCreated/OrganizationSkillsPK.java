/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel.CustomCreated;


import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author deepaksubramanian
 */
@Embeddable
public class OrganizationSkillsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "skillId")
    private long skillId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "linkedinCompanyId")
    private long linkedinCompanyId;

    public OrganizationSkillsPK() {
    }

    public OrganizationSkillsPK(long skillId, long linkedinCompanyId) {
        this.skillId = skillId;
        this.linkedinCompanyId = linkedinCompanyId;
    }

    public long getSkillId() {
        return skillId;
    }

    public void setSkillId(long skillId) {
        this.skillId = skillId;
    }



    public long getLinkedinCompanyId() {
        return linkedinCompanyId;
    }

    public void setLinkedinCompanyId(long linkedinCompanyId) {
        this.linkedinCompanyId = linkedinCompanyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) skillId;
        hash += (int) linkedinCompanyId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof com.gurukadaksh.DBModel.CustomCreated.OrganizationSkillsPK)) {
            return false;
        }
        com.gurukadaksh.DBModel.CustomCreated.OrganizationSkillsPK other = (com.gurukadaksh.DBModel.CustomCreated.OrganizationSkillsPK) object;
        if (this.skillId != other.skillId) {
            return false;
        }
        if (this.linkedinCompanyId != other.linkedinCompanyId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.ProjectSkillsPK[ skillId=" + skillId + ", linkedinCompanyId=" + linkedinCompanyId + " ]";
    }
    
}
