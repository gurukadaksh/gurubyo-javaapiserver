/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel.CustomCreated;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "projectSkills", catalog = "gurukadakshDB", schema = "")
@NamedQueries({
    @NamedQuery(name = "ProjectSkills.findAll", query = "SELECT p FROM ProjectSkills p")
})
public class ProjectSkills implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    //@GeneratedValue(strategy = GenerationType.AUTO)
    private ProjectSkillsPK id;

    public ProjectSkillsPK getId() {
        return id;
    }

    public void setId(ProjectSkillsPK id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectSkills)) {
            return false;
        }
        ProjectSkills other = (ProjectSkills) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.CustomCreated.ProjectSkills[ id=" + id + " ]";
    }
    
}
