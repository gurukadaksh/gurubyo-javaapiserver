/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel.CustomCreated;


import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author deepaksubramanian
 */
@Embeddable
public class ProjectPateAssociationsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "pateKeyId")
    private long pateKeyId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "projectId")
    private long projectId;

    public long getPateKeyId() {
        return pateKeyId;
    }

    public void setPateKeyId(long pateKeyId) {
        this.pateKeyId = pateKeyId;
    }

    public ProjectPateAssociationsPK() {
    }

    public ProjectPateAssociationsPK(long pateKeyId, long projectId) {
        this.pateKeyId = pateKeyId;
        this.projectId = projectId;
    }




    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) pateKeyId;
        hash += (int) projectId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof com.gurukadaksh.DBModel.CustomCreated.ProjectPateAssociationsPK)) {
            return false;
        }
        com.gurukadaksh.DBModel.CustomCreated.ProjectPateAssociationsPK other = (com.gurukadaksh.DBModel.CustomCreated.ProjectPateAssociationsPK) object;
        if (this.pateKeyId != other.pateKeyId) {
            return false;
        }
        if (this.projectId != other.projectId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.ProjectPateAssociationsPK[ pateKeyId=" + pateKeyId + ", projectId=" + projectId + " ]";
    }
    
}
