/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "userFileNames", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserFileNames.findAll", query = "SELECT u FROM UserFileNames u"),
    @NamedQuery(name = "UserFileNames.findByFileId", query = "SELECT u FROM UserFileNames u WHERE u.fileId = :fileId"),
    @NamedQuery(name = "UserFileNames.findByFileName", query = "SELECT u FROM UserFileNames u WHERE u.fileName = :fileName")})
public class UserFileNames implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "fileId")
    private String fileId;
    @Size(max = 500)
    @Column(name = "fileName")
    private String fileName;
    @JoinColumn(name = "fileId", referencedColumnName = "fileId", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private UserFilesTable userFilesTable;
    @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usertable linkedinId;

    public UserFileNames() {
    }

    public UserFileNames(String fileId) {
        this.fileId = fileId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public UserFilesTable getUserFilesTable() {
        return userFilesTable;
    }

    public void setUserFilesTable(UserFilesTable userFilesTable) {
        this.userFilesTable = userFilesTable;
    }

    public Usertable getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(Usertable linkedinId) {
        this.linkedinId = linkedinId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileId != null ? fileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserFileNames)) {
            return false;
        }
        UserFileNames other = (UserFileNames) object;
        if ((this.fileId == null && other.fileId != null) || (this.fileId != null && !this.fileId.equals(other.fileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserFileNames[ fileId=" + fileId + " ]";
    }
    
}
