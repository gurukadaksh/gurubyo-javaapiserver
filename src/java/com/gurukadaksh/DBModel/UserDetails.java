/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "userDetails", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserDetails.findAll", query = "SELECT u FROM UserDetails u"),
    @NamedQuery(name = "UserDetails.findByLinkedinId", query = "SELECT u FROM UserDetails u WHERE u.linkedinId = :linkedinId"),
    @NamedQuery(name = "UserDetails.findByLastModified", query = "SELECT u FROM UserDetails u WHERE u.lastModified = :lastModified"),
    @NamedQuery(name = "UserDetails.findByIsoCountryCode", query = "SELECT u FROM UserDetails u WHERE u.isoCountryCode = :isoCountryCode"),
    @NamedQuery(name = "UserDetails.findByDateOfBirth", query = "SELECT u FROM UserDetails u WHERE u.dateOfBirth = :dateOfBirth"),
    @NamedQuery(name = "UserDetails.findByMfeedRssURL", query = "SELECT u FROM UserDetails u WHERE u.mfeedRssURL = :mfeedRssURL"),
    @NamedQuery(name = "UserDetails.findByIndustry", query = "SELECT u FROM UserDetails u WHERE u.industry = :industry")})
public class UserDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "linkedinId")
    private String linkedinId;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "summary")
    private String summary;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "specialities")
    private String specialities;
    @Column(name = "lastModified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "honors")
    private String honors;
    @Size(max = 5)
    @Column(name = "isoCountryCode")
    private String isoCountryCode;
    @Column(name = "dateOfBirth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @Size(max = 500)
    @Column(name = "mfeedRssURL")
    private String mfeedRssURL;
    @Size(max = 500)
    @Column(name = "industry")
    private String industry;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "proposalComments")
    private String proposalComments;
    @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Usertable usertable;

    public UserDetails() {
    }

    public UserDetails(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSpecialities() {
        return specialities;
    }

    public void setSpecialities(String specialities) {
        this.specialities = specialities;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getHonors() {
        return honors;
    }

    public void setHonors(String honors) {
        this.honors = honors;
    }

    public String getIsoCountryCode() {
        return isoCountryCode;
    }

    public void setIsoCountryCode(String isoCountryCode) {
        this.isoCountryCode = isoCountryCode;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMfeedRssURL() {
        return mfeedRssURL;
    }

    public void setMfeedRssURL(String mfeedRssURL) {
        this.mfeedRssURL = mfeedRssURL;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getProposalComments() {
        return proposalComments;
    }

    public void setProposalComments(String proposalComments) {
        this.proposalComments = proposalComments;
    }

    public Usertable getUsertable() {
        return usertable;
    }

    public void setUsertable(Usertable usertable) {
        this.usertable = usertable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (linkedinId != null ? linkedinId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserDetails)) {
            return false;
        }
        UserDetails other = (UserDetails) object;
        if ((this.linkedinId == null && other.linkedinId != null) || (this.linkedinId != null && !this.linkedinId.equals(other.linkedinId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserDetails[ linkedinId=" + linkedinId + " ]";
    }
    
}
