/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author deepaksubramanian
 */
@Embeddable
public class UserEducationDetailsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "linkedinId")
    private String linkedinId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "linkedinEducationId")
    private long linkedinEducationId;

    public UserEducationDetailsPK() {
    }

    public UserEducationDetailsPK(String linkedinId, long linkedinEducationId) {
        this.linkedinId = linkedinId;
        this.linkedinEducationId = linkedinEducationId;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public long getLinkedinEducationId() {
        return linkedinEducationId;
    }

    public void setLinkedinEducationId(long linkedinEducationId) {
        this.linkedinEducationId = linkedinEducationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (linkedinId != null ? linkedinId.hashCode() : 0);
        hash += (int) linkedinEducationId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserEducationDetailsPK)) {
            return false;
        }
        UserEducationDetailsPK other = (UserEducationDetailsPK) object;
        if ((this.linkedinId == null && other.linkedinId != null) || (this.linkedinId != null && !this.linkedinId.equals(other.linkedinId))) {
            return false;
        }
        if (this.linkedinEducationId != other.linkedinEducationId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserEducationDetailsPK[ linkedinId=" + linkedinId + ", linkedinEducationId=" + linkedinEducationId + " ]";
    }
    
}
