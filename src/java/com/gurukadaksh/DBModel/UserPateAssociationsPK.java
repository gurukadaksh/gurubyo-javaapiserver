/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author deepaksubramanian
 */
@Embeddable
public class UserPateAssociationsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "linkedinId")
    private String linkedinId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pateKeyId")
    private long pateKeyId;

    public UserPateAssociationsPK() {
    }

    public UserPateAssociationsPK(String linkedinId, long pateKeyId) {
        this.linkedinId = linkedinId;
        this.pateKeyId = pateKeyId;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public long getPateKeyId() {
        return pateKeyId;
    }

    public void setPateKeyId(long pateKeyId) {
        this.pateKeyId = pateKeyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (linkedinId != null ? linkedinId.hashCode() : 0);
        hash += (int) pateKeyId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserPateAssociationsPK)) {
            return false;
        }
        UserPateAssociationsPK other = (UserPateAssociationsPK) object;
        if ((this.linkedinId == null && other.linkedinId != null) || (this.linkedinId != null && !this.linkedinId.equals(other.linkedinId))) {
            return false;
        }
        if (this.pateKeyId != other.pateKeyId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserPateAssociationsPK[ linkedinId=" + linkedinId + ", pateKeyId=" + pateKeyId + " ]";
    }
    
}
