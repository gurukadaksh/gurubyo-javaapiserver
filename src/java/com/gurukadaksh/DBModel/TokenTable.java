/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "tokenTable", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TokenTable.findAll", query = "SELECT t FROM TokenTable t"),
    @NamedQuery(name = "TokenTable.findByGkToken", query = "SELECT t FROM TokenTable t WHERE t.gkToken = :gkToken"),
    @NamedQuery(name = "TokenTable.findByLinkedinToken", query = "SELECT t FROM TokenTable t WHERE t.linkedinToken = :linkedinToken"),
    @NamedQuery(name = "TokenTable.findByLinkedinSecret", query = "SELECT t FROM TokenTable t WHERE t.linkedinSecret = :linkedinSecret"),
    @NamedQuery(name = "TokenTable.findByTimestamp", query = "SELECT t FROM TokenTable t WHERE t.timestamp = :timestamp")})
public class TokenTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 600)
    @Column(name = "gkToken")
    private String gkToken;
    @Size(max = 600)
    @Column(name = "linkedinToken")
    private String linkedinToken;
    @Size(max = 600)
    @Column(name = "linkedinSecret")
    private String linkedinSecret;
    @Basic(optional = false)
    @NotNull
    @Column(name = "timestamp")
    private long timestamp;
    @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId")
    @ManyToOne(fetch = FetchType.LAZY)
    private Usertable linkedinId;

    public TokenTable() {
    }

    public TokenTable(String gkToken) {
        this.gkToken = gkToken;
    }

    public TokenTable(String gkToken, long timestamp) {
        this.gkToken = gkToken;
        this.timestamp = timestamp;
    }

    public String getGkToken() {
        return gkToken;
    }

    public void setGkToken(String gkToken) {
        this.gkToken = gkToken;
    }

    public String getLinkedinToken() {
        return linkedinToken;
    }

    public void setLinkedinToken(String linkedinToken) {
        this.linkedinToken = linkedinToken;
    }

    public String getLinkedinSecret() {
        return linkedinSecret;
    }

    public void setLinkedinSecret(String linkedinSecret) {
        this.linkedinSecret = linkedinSecret;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Usertable getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(Usertable linkedinId) {
        this.linkedinId = linkedinId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gkToken != null ? gkToken.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TokenTable)) {
            return false;
        }
        TokenTable other = (TokenTable) object;
        if ((this.gkToken == null && other.gkToken != null) || (this.gkToken != null && !this.gkToken.equals(other.gkToken))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.TokenTable[ gkToken=" + gkToken + " ]";
    }
    
}
