/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "organizationsDatabase", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrganizationsDatabase.findAll", query = "SELECT o FROM OrganizationsDatabase o"),
    @NamedQuery(name = "OrganizationsDatabase.findByLinkedinCompanyId", query = "SELECT o FROM OrganizationsDatabase o WHERE o.linkedinCompanyId = :linkedinCompanyId"),
    @NamedQuery(name = "OrganizationsDatabase.findByOrganizationName", query = "SELECT o FROM OrganizationsDatabase o WHERE o.organizationName = :organizationName"),
    @NamedQuery(name = "OrganizationsDatabase.findByOrganizationType", query = "SELECT o FROM OrganizationsDatabase o WHERE o.organizationType = :organizationType"),
    @NamedQuery(name = "OrganizationsDatabase.findByOrganizationIndustry", query = "SELECT o FROM OrganizationsDatabase o WHERE o.organizationIndustry = :organizationIndustry")})
public class OrganizationsDatabase implements Serializable {
    @JoinTable(name = "organizationSkillsTable", joinColumns = {
        @JoinColumn(name = "linkedinCompanyId", referencedColumnName = "linkedinCompanyId")}, inverseJoinColumns = {
        @JoinColumn(name = "skillId", referencedColumnName = "skillId")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<SkillsDatabase> skillsDatabaseCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "linkedinCompanyId")
    private Long linkedinCompanyId;
    @Size(max = 500)
    @Column(name = "organizationName")
    private String organizationName;
    @Size(max = 500)
    @Column(name = "organizationType")
    private String organizationType;
    @Size(max = 500)
    @Column(name = "organizationIndustry")
    private String organizationIndustry;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "organizationDescription")
    private String organizationDescription;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organizationsDatabase", fetch = FetchType.LAZY)
    private Collection<OrganizationPateAssociations> organizationPateAssociationsCollection;

    public OrganizationsDatabase() {
    }

    public OrganizationsDatabase(Long linkedinCompanyId) {
        this.linkedinCompanyId = linkedinCompanyId;
    }

    public Long getLinkedinCompanyId() {
        return linkedinCompanyId;
    }

    public void setLinkedinCompanyId(Long linkedinCompanyId) {
        this.linkedinCompanyId = linkedinCompanyId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType;
    }

    public String getOrganizationIndustry() {
        return organizationIndustry;
    }

    public void setOrganizationIndustry(String organizationIndustry) {
        this.organizationIndustry = organizationIndustry;
    }

    public String getOrganizationDescription() {
        return organizationDescription;
    }

    public void setOrganizationDescription(String organizationDescription) {
        this.organizationDescription = organizationDescription;
    }

    @XmlTransient
    public Collection<OrganizationPateAssociations> getOrganizationPateAssociationsCollection() {
        return organizationPateAssociationsCollection;
    }

    public void setOrganizationPateAssociationsCollection(Collection<OrganizationPateAssociations> organizationPateAssociationsCollection) {
        this.organizationPateAssociationsCollection = organizationPateAssociationsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (linkedinCompanyId != null ? linkedinCompanyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganizationsDatabase)) {
            return false;
        }
        OrganizationsDatabase other = (OrganizationsDatabase) object;
        if ((this.linkedinCompanyId == null && other.linkedinCompanyId != null) || (this.linkedinCompanyId != null && !this.linkedinCompanyId.equals(other.linkedinCompanyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.OrganizationsDatabase[ linkedinCompanyId=" + linkedinCompanyId + " ]";
    }

    @XmlTransient
    public Collection<SkillsDatabase> getSkillsDatabaseCollection() {
        return skillsDatabaseCollection;
    }

    public void setSkillsDatabaseCollection(Collection<SkillsDatabase> skillsDatabaseCollection) {
        this.skillsDatabaseCollection = skillsDatabaseCollection;
    }
    
}
