/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "userconnectionstable", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Userconnectionstable.findAll", query = "SELECT u FROM Userconnectionstable u"),
    @NamedQuery(name = "Userconnectionstable.findByLinkedinId1", query = "SELECT u FROM Userconnectionstable u WHERE u.userconnectionstablePK.linkedinId1 = :linkedinId1"),
    @NamedQuery(name = "Userconnectionstable.findByLinkedinId2", query = "SELECT u FROM Userconnectionstable u WHERE u.userconnectionstablePK.linkedinId2 = :linkedinId2"),
    @NamedQuery(name = "Userconnectionstable.findByIsLinkedIn", query = "SELECT u FROM Userconnectionstable u WHERE u.isLinkedIn = :isLinkedIn")})
public class Userconnectionstable implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserconnectionstablePK userconnectionstablePK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isLinkedIn")
    private boolean isLinkedIn;
    @JoinColumn(name = "linkedinId2", referencedColumnName = "linkedinId", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usertable usertable;
    @JoinColumn(name = "linkedinId1", referencedColumnName = "linkedinId", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usertable usertable1;

    public Userconnectionstable() {
    }

    public Userconnectionstable(UserconnectionstablePK userconnectionstablePK) {
        this.userconnectionstablePK = userconnectionstablePK;
    }

    public Userconnectionstable(UserconnectionstablePK userconnectionstablePK, boolean isLinkedIn) {
        this.userconnectionstablePK = userconnectionstablePK;
        this.isLinkedIn = isLinkedIn;
    }

    public Userconnectionstable(String linkedinId1, String linkedinId2) {
        this.userconnectionstablePK = new UserconnectionstablePK(linkedinId1, linkedinId2);
    }

    public UserconnectionstablePK getUserconnectionstablePK() {
        return userconnectionstablePK;
    }

    public void setUserconnectionstablePK(UserconnectionstablePK userconnectionstablePK) {
        this.userconnectionstablePK = userconnectionstablePK;
    }

    public boolean getIsLinkedIn() {
        return isLinkedIn;
    }

    public void setIsLinkedIn(boolean isLinkedIn) {
        this.isLinkedIn = isLinkedIn;
    }

    public Usertable getUsertable() {
        return usertable;
    }

    public void setUsertable(Usertable usertable) {
        this.usertable = usertable;
    }

    public Usertable getUsertable1() {
        return usertable1;
    }

    public void setUsertable1(Usertable usertable1) {
        this.usertable1 = usertable1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userconnectionstablePK != null ? userconnectionstablePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userconnectionstable)) {
            return false;
        }
        Userconnectionstable other = (Userconnectionstable) object;
        if ((this.userconnectionstablePK == null && other.userconnectionstablePK != null) || (this.userconnectionstablePK != null && !this.userconnectionstablePK.equals(other.userconnectionstablePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.Userconnectionstable[ userconnectionstablePK=" + userconnectionstablePK + " ]";
    }
    
}
