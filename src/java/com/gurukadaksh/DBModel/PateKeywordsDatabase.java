/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "pateKeywordsDatabase", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PateKeywordsDatabase.findAll", query = "SELECT p FROM PateKeywordsDatabase p"),
    @NamedQuery(name = "PateKeywordsDatabase.findByPateKeyId", query = "SELECT p FROM PateKeywordsDatabase p WHERE p.pateKeyId = :pateKeyId"),
    @NamedQuery(name = "PateKeywordsDatabase.findByPateKey", query = "SELECT p FROM PateKeywordsDatabase p WHERE p.pateKey = :pateKey")})
public class PateKeywordsDatabase implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "pateKeyId")
    private Long pateKeyId;
    @Size(max = 500)
    @Column(name = "pateKey")
    private String pateKey;
    @ManyToMany(mappedBy = "pateKeywordsDatabaseCollection", fetch = FetchType.LAZY)
    private Collection<UserProjects> userProjectsCollection;

    public PateKeywordsDatabase() {
    }

    public PateKeywordsDatabase(Long pateKeyId) {
        this.pateKeyId = pateKeyId;
    }

    public Long getPateKeyId() {
        return pateKeyId;
    }

    public void setPateKeyId(Long pateKeyId) {
        this.pateKeyId = pateKeyId;
    }

    public String getPateKey() {
        return pateKey;
    }

    public void setPateKey(String pateKey) {
        this.pateKey = pateKey;
    }

    @XmlTransient
    public Collection<UserProjects> getUserProjectsCollection() {
        return userProjectsCollection;
    }

    public void setUserProjectsCollection(Collection<UserProjects> userProjectsCollection) {
        this.userProjectsCollection = userProjectsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pateKeyId != null ? pateKeyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PateKeywordsDatabase)) {
            return false;
        }
        PateKeywordsDatabase other = (PateKeywordsDatabase) object;
        if ((this.pateKeyId == null && other.pateKeyId != null) || (this.pateKeyId != null && !this.pateKeyId.equals(other.pateKeyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.PateKeywordsDatabase[ pateKeyId=" + pateKeyId + " ]";
    }
    
}
