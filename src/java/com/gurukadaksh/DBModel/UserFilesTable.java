/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "userFilesTable", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserFilesTable.findAll", query = "SELECT u FROM UserFilesTable u"),
    @NamedQuery(name = "UserFilesTable.findByFileId", query = "SELECT u FROM UserFilesTable u WHERE u.fileId = :fileId")})
public class UserFilesTable implements Serializable {
    @Lob
    @Column(name = "file")
    private byte[] file;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "fileId")
    private String fileId;
    @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usertable linkedinId;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userFilesTable", fetch = FetchType.LAZY)
    private UserFileNames userFileNames;
    @OneToMany(mappedBy = "fileId", fetch = FetchType.LAZY)
    private Collection<Usertable> usertableCollection;

    public UserFilesTable() {
    }

    public UserFilesTable(String fileId) {
        this.fileId = fileId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public Usertable getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(Usertable linkedinId) {
        this.linkedinId = linkedinId;
    }

    public UserFileNames getUserFileNames() {
        return userFileNames;
    }

    public void setUserFileNames(UserFileNames userFileNames) {
        this.userFileNames = userFileNames;
    }

    @XmlTransient
    public Collection<Usertable> getUsertableCollection() {
        return usertableCollection;
    }

    public void setUsertableCollection(Collection<Usertable> usertableCollection) {
        this.usertableCollection = usertableCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileId != null ? fileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserFilesTable)) {
            return false;
        }
        UserFilesTable other = (UserFilesTable) object;
        if ((this.fileId == null && other.fileId != null) || (this.fileId != null && !this.fileId.equals(other.fileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserFilesTable[ fileId=" + fileId + " ]";
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }
    
}
