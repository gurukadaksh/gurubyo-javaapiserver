/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "userPateAssociations", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserPateAssociations.findAll", query = "SELECT u FROM UserPateAssociations u"),
    @NamedQuery(name = "UserPateAssociations.findByLinkedinId", query = "SELECT u FROM UserPateAssociations u WHERE u.userPateAssociationsPK.linkedinId = :linkedinId"),
    @NamedQuery(name = "UserPateAssociations.findByPateKeyId", query = "SELECT u FROM UserPateAssociations u WHERE u.userPateAssociationsPK.pateKeyId = :pateKeyId"),
    @NamedQuery(name = "UserPateAssociations.findByKeyType", query = "SELECT u FROM UserPateAssociations u WHERE u.keyType = :keyType"),
    @NamedQuery(name = "UserPateAssociations.findByKeyWeight", query = "SELECT u FROM UserPateAssociations u WHERE u.keyWeight = :keyWeight")})
public class UserPateAssociations implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserPateAssociationsPK userPateAssociationsPK;
    @Column(name = "keyType")
    private Boolean keyType;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "keyWeight")
    private Float keyWeight;
    @JoinColumn(name = "pateKeyId", referencedColumnName = "skillId", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SkillsDatabase skillsDatabase;
    @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usertable usertable;

    public UserPateAssociations() {
    }

    public UserPateAssociations(UserPateAssociationsPK userPateAssociationsPK) {
        this.userPateAssociationsPK = userPateAssociationsPK;
    }

    public UserPateAssociations(String linkedinId, long pateKeyId) {
        this.userPateAssociationsPK = new UserPateAssociationsPK(linkedinId, pateKeyId);
    }

    public UserPateAssociationsPK getUserPateAssociationsPK() {
        return userPateAssociationsPK;
    }

    public void setUserPateAssociationsPK(UserPateAssociationsPK userPateAssociationsPK) {
        this.userPateAssociationsPK = userPateAssociationsPK;
    }

    public Boolean getKeyType() {
        return keyType;
    }

    public void setKeyType(Boolean keyType) {
        this.keyType = keyType;
    }

    public Float getKeyWeight() {
        return keyWeight;
    }

    public void setKeyWeight(Float keyWeight) {
        this.keyWeight = keyWeight;
    }

    public SkillsDatabase getSkillsDatabase() {
        return skillsDatabase;
    }

    public void setSkillsDatabase(SkillsDatabase skillsDatabase) {
        this.skillsDatabase = skillsDatabase;
    }

    public Usertable getUsertable() {
        return usertable;
    }

    public void setUsertable(Usertable usertable) {
        this.usertable = usertable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userPateAssociationsPK != null ? userPateAssociationsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserPateAssociations)) {
            return false;
        }
        UserPateAssociations other = (UserPateAssociations) object;
        if ((this.userPateAssociationsPK == null && other.userPateAssociationsPK != null) || (this.userPateAssociationsPK != null && !this.userPateAssociationsPK.equals(other.userPateAssociationsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserPateAssociations[ userPateAssociationsPK=" + userPateAssociationsPK + " ]";
    }
    
}
