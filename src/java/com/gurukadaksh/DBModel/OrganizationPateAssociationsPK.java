/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author deepaksubramanian
 */
@Embeddable
public class OrganizationPateAssociationsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "linkedinCompanyId")
    private long linkedinCompanyId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pateKeyId")
    private long pateKeyId;

    public OrganizationPateAssociationsPK() {
    }

    public OrganizationPateAssociationsPK(long linkedinCompanyId, long pateKeyId) {
        this.linkedinCompanyId = linkedinCompanyId;
        this.pateKeyId = pateKeyId;
    }

    public long getLinkedinCompanyId() {
        return linkedinCompanyId;
    }

    public void setLinkedinCompanyId(long linkedinCompanyId) {
        this.linkedinCompanyId = linkedinCompanyId;
    }

    public long getPateKeyId() {
        return pateKeyId;
    }

    public void setPateKeyId(long pateKeyId) {
        this.pateKeyId = pateKeyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) linkedinCompanyId;
        hash += (int) pateKeyId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganizationPateAssociationsPK)) {
            return false;
        }
        OrganizationPateAssociationsPK other = (OrganizationPateAssociationsPK) object;
        if (this.linkedinCompanyId != other.linkedinCompanyId) {
            return false;
        }
        if (this.pateKeyId != other.pateKeyId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.OrganizationPateAssociationsPK[ linkedinCompanyId=" + linkedinCompanyId + ", pateKeyId=" + pateKeyId + " ]";
    }
    
}
