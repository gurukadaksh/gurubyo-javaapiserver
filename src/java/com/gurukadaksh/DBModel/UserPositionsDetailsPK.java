/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author deepaksubramanian
 */
@Embeddable
public class UserPositionsDetailsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "linkedinId")
    private String linkedinId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "linkedinPositionId")
    private long linkedinPositionId;

    public UserPositionsDetailsPK() {
    }

    public UserPositionsDetailsPK(String linkedinId, long linkedinPositionId) {
        this.linkedinId = linkedinId;
        this.linkedinPositionId = linkedinPositionId;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public long getLinkedinPositionId() {
        return linkedinPositionId;
    }

    public void setLinkedinPositionId(long linkedinPositionId) {
        this.linkedinPositionId = linkedinPositionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (linkedinId != null ? linkedinId.hashCode() : 0);
        hash += (int) linkedinPositionId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserPositionsDetailsPK)) {
            return false;
        }
        UserPositionsDetailsPK other = (UserPositionsDetailsPK) object;
        if ((this.linkedinId == null && other.linkedinId != null) || (this.linkedinId != null && !this.linkedinId.equals(other.linkedinId))) {
            return false;
        }
        if (this.linkedinPositionId != other.linkedinPositionId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserPositionsDetailsPK[ linkedinId=" + linkedinId + ", linkedinPositionId=" + linkedinPositionId + " ]";
    }
    
}
