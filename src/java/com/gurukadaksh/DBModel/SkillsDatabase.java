/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "skillsDatabase", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SkillsDatabase.findAll", query = "SELECT s FROM SkillsDatabase s"),
    @NamedQuery(name = "SkillsDatabase.findBySkillName", query = "SELECT s FROM SkillsDatabase s WHERE s.skillName = :skillName"),
    @NamedQuery(name = "SkillsDatabase.findBySkillId", query = "SELECT s FROM SkillsDatabase s WHERE s.skillId = :skillId"),
    @NamedQuery(name = "SkillsDatabase.findByAddedByPate", query = "SELECT s FROM SkillsDatabase s WHERE s.addedByPate = :addedByPate")})
public class SkillsDatabase implements Serializable {
    @ManyToMany(mappedBy = "skillsDatabaseCollection", fetch = FetchType.LAZY)
    private Collection<OrganizationsDatabase> organizationsDatabaseCollection;
    private static final long serialVersionUID = 1L;
    @Size(max = 300)
    @Column(name = "skillName")
    private String skillName;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@Basic(optional = false)
    //@NotNull
    @Column(name = "skillId")
    private Long skillId;
    @Column(name = "addedByPate")
    private Boolean addedByPate;
    @ManyToMany(mappedBy = "skillsDatabaseCollection", fetch = FetchType.LAZY)
    private Collection<Usertable> usertableCollection;
    @ManyToMany(mappedBy = "skillsDatabaseCollection", fetch = FetchType.LAZY)  
    private Collection<UserProjects> userProjectsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "skillsDatabase", fetch = FetchType.LAZY)
    private Collection<UserPateAssociations> userPateAssociationsCollection;

    public SkillsDatabase() {
    }

    public SkillsDatabase(Long skillId) {
        this.skillId = skillId;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public Long getSkillId() {
        return skillId;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }

    public Boolean getAddedByPate() {
        return addedByPate;
    }

    public void setAddedByPate(Boolean addedByPate) {
        this.addedByPate = addedByPate;
    }

    @XmlTransient
    public Collection<Usertable> getUsertableCollection() {
        return usertableCollection;
    }

    public void setUsertableCollection(Collection<Usertable> usertableCollection) {
        this.usertableCollection = usertableCollection;
    }

    @XmlTransient
    public Collection<UserProjects> getUserProjectsCollection() {
        return userProjectsCollection;
    }

    public void setUserProjectsCollection(Collection<UserProjects> userProjectsCollection) {
        this.userProjectsCollection = userProjectsCollection;
    }

    @XmlTransient
    public Collection<UserPateAssociations> getUserPateAssociationsCollection() {
        return userPateAssociationsCollection;
    }

    public void setUserPateAssociationsCollection(Collection<UserPateAssociations> userPateAssociationsCollection) {
        this.userPateAssociationsCollection = userPateAssociationsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (skillId != null ? skillId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SkillsDatabase)) {
            return false;
        }
        SkillsDatabase other = (SkillsDatabase) object;
        if ((this.skillId == null && other.skillId != null) || (this.skillId != null && !this.skillId.equals(other.skillId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.SkillsDatabase[ skillId=" + skillId + " ]";
    }

    @XmlTransient
    public Collection<OrganizationsDatabase> getOrganizationsDatabaseCollection() {
        return organizationsDatabaseCollection;
    }

    public void setOrganizationsDatabaseCollection(Collection<OrganizationsDatabase> organizationsDatabaseCollection) {
        this.organizationsDatabaseCollection = organizationsDatabaseCollection;
    }
    
}
