/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author deepaksubramanian
 */
@Embeddable
public class UserconnectionstablePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "linkedinId1")
    private String linkedinId1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "linkedinId2")
    private String linkedinId2;

    public UserconnectionstablePK() {
    }

    public UserconnectionstablePK(String linkedinId1, String linkedinId2) {
        this.linkedinId1 = linkedinId1;
        this.linkedinId2 = linkedinId2;
    }

    public String getLinkedinId1() {
        return linkedinId1;
    }

    public void setLinkedinId1(String linkedinId1) {
        this.linkedinId1 = linkedinId1;
    }

    public String getLinkedinId2() {
        return linkedinId2;
    }

    public void setLinkedinId2(String linkedinId2) {
        this.linkedinId2 = linkedinId2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (linkedinId1 != null ? linkedinId1.hashCode() : 0);
        hash += (linkedinId2 != null ? linkedinId2.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserconnectionstablePK)) {
            return false;
        }
        UserconnectionstablePK other = (UserconnectionstablePK) object;
        if ((this.linkedinId1 == null && other.linkedinId1 != null) || (this.linkedinId1 != null && !this.linkedinId1.equals(other.linkedinId1))) {
            return false;
        }
        if ((this.linkedinId2 == null && other.linkedinId2 != null) || (this.linkedinId2 != null && !this.linkedinId2.equals(other.linkedinId2))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserconnectionstablePK[ linkedinId1=" + linkedinId1 + ", linkedinId2=" + linkedinId2 + " ]";
    }
    
}
