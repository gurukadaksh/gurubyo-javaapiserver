/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "applicationTokens", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ApplicationTokens.findAll", query = "SELECT a FROM ApplicationTokens a"),
    @NamedQuery(name = "ApplicationTokens.findByApplicationName", query = "SELECT a FROM ApplicationTokens a WHERE a.applicationName = :applicationName"),
    @NamedQuery(name = "ApplicationTokens.findByApplicationToken", query = "SELECT a FROM ApplicationTokens a WHERE a.applicationToken = :applicationToken")})
public class ApplicationTokens implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 500)
    @Column(name = "applicationName")
    private String applicationName;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 600)
    @Column(name = "applicationToken")
    private String applicationToken;

    public ApplicationTokens() {
    }

    public ApplicationTokens(String applicationToken) {
        this.applicationToken = applicationToken;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationToken() {
        return applicationToken;
    }

    public void setApplicationToken(String applicationToken) {
        this.applicationToken = applicationToken;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (applicationToken != null ? applicationToken.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApplicationTokens)) {
            return false;
        }
        ApplicationTokens other = (ApplicationTokens) object;
        if ((this.applicationToken == null && other.applicationToken != null) || (this.applicationToken != null && !this.applicationToken.equals(other.applicationToken))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.ApplicationTokens[ applicationToken=" + applicationToken + " ]";
    }
    
}
