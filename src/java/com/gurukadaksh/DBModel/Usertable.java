/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "usertable", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usertable.findAll", query = "SELECT u FROM Usertable u"),
    @NamedQuery(name = "Usertable.findByLinkedinId", query = "SELECT u FROM Usertable u WHERE u.linkedinId = :linkedinId"),
    @NamedQuery(name = "Usertable.findByFirstName", query = "SELECT u FROM Usertable u WHERE u.firstName = :firstName"),
    @NamedQuery(name = "Usertable.findByLastName", query = "SELECT u FROM Usertable u WHERE u.lastName = :lastName"),
    @NamedQuery(name = "Usertable.findByPictureURL", query = "SELECT u FROM Usertable u WHERE u.pictureURL = :pictureURL")})
public class Usertable implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usertable", fetch = FetchType.LAZY)
    private Collection<Userconnectionstable> userconnectionstableCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usertable1", fetch = FetchType.LAZY)
    private Collection<Userconnectionstable> userconnectionstableCollection1;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "linkedinId")
    private String linkedinId;
    @Size(max = 100)
    @Column(name = "firstName")
    private String firstName;
    @Size(max = 100)
    @Column(name = "lastName")
    private String lastName;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "headline")
    private String headline;
    @Size(max = 1024)
    @Column(name = "pictureURL")
    private String pictureURL;
    @JoinTable(name = "userSkillsTable", joinColumns = {
        @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId")}, inverseJoinColumns = {
        @JoinColumn(name = "skillId", referencedColumnName = "skillId")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<SkillsDatabase> skillsDatabaseCollection;
    @ManyToMany(mappedBy = "usertableCollection", fetch = FetchType.LAZY)
    private Collection<MessageCenter> messageCenterCollection;
    @ManyToMany(mappedBy = "usertableCollection", fetch = FetchType.LAZY)
    private Collection<UserProjects> userProjectsCollection;
    @ManyToMany(mappedBy = "usertableCollection1", fetch = FetchType.LAZY)
    private Collection<UserProjects> userProjectsCollection1;
    @ManyToMany(mappedBy = "usertableCollection2", fetch = FetchType.LAZY)
    private Collection<UserProjects> userProjectsCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usertable", fetch = FetchType.LAZY)
    private Collection<UserEducationDetails> userEducationDetailsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "linkedinId", fetch = FetchType.LAZY)
    private Collection<UserFilesTable> userFilesTableCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "linkedinId", fetch = FetchType.LAZY)
    private Collection<UserFileNames> userFileNamesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fromId", fetch = FetchType.LAZY)
    private Collection<MessageCenter> messageCenterCollection1;
    @JoinColumn(name = "fileId", referencedColumnName = "fileId")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserFilesTable fileId;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "usertable", fetch = FetchType.LAZY)
    private UserDetails userDetails;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usertable", fetch = FetchType.LAZY)
    private Collection<Userconnectionstable> userConnectionsTableCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usertable1", fetch = FetchType.LAZY)
    private Collection<Userconnectionstable> userConnectionsTableCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usertable", fetch = FetchType.LAZY)
    private Collection<UserPositionsDetails> userPositionsDetailsCollection;
    @OneToMany(mappedBy = "linkedinId", fetch = FetchType.LAZY)
    private Collection<TokenTable> tokenTableCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "usertable", fetch = FetchType.LAZY)
    private ContactTable contactTable;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usertable", fetch = FetchType.LAZY)
    private Collection<UserPateAssociations> userPateAssociationsCollection;

    public Usertable() {
    }

    public Usertable(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    @XmlTransient
    public Collection<SkillsDatabase> getSkillsDatabaseCollection() {
        return skillsDatabaseCollection;
    }

    public void setSkillsDatabaseCollection(Collection<SkillsDatabase> skillsDatabaseCollection) {
        this.skillsDatabaseCollection = skillsDatabaseCollection;
    }

    @XmlTransient
    public Collection<MessageCenter> getMessageCenterCollection() {
        return messageCenterCollection;
    }

    public void setMessageCenterCollection(Collection<MessageCenter> messageCenterCollection) {
        this.messageCenterCollection = messageCenterCollection;
    }

    @XmlTransient
    public Collection<UserProjects> getUserProjectsCollection() {
        return userProjectsCollection;
    }

    public void setUserProjectsCollection(Collection<UserProjects> userProjectsCollection) {
        this.userProjectsCollection = userProjectsCollection;
    }

    @XmlTransient
    public Collection<UserProjects> getUserProjectsCollection1() {
        return userProjectsCollection1;
    }

    public void setUserProjectsCollection1(Collection<UserProjects> userProjectsCollection1) {
        this.userProjectsCollection1 = userProjectsCollection1;
    }

    @XmlTransient
    public Collection<UserProjects> getUserProjectsCollection2() {
        return userProjectsCollection2;
    }

    public void setUserProjectsCollection2(Collection<UserProjects> userProjectsCollection2) {
        this.userProjectsCollection2 = userProjectsCollection2;
    }

    @XmlTransient
    public Collection<UserEducationDetails> getUserEducationDetailsCollection() {
        return userEducationDetailsCollection;
    }

    public void setUserEducationDetailsCollection(Collection<UserEducationDetails> userEducationDetailsCollection) {
        this.userEducationDetailsCollection = userEducationDetailsCollection;
    }

    @XmlTransient
    public Collection<UserFilesTable> getUserFilesTableCollection() {
        return userFilesTableCollection;
    }

    public void setUserFilesTableCollection(Collection<UserFilesTable> userFilesTableCollection) {
        this.userFilesTableCollection = userFilesTableCollection;
    }

    @XmlTransient
    public Collection<UserFileNames> getUserFileNamesCollection() {
        return userFileNamesCollection;
    }

    public void setUserFileNamesCollection(Collection<UserFileNames> userFileNamesCollection) {
        this.userFileNamesCollection = userFileNamesCollection;
    }

    @XmlTransient
    public Collection<MessageCenter> getMessageCenterCollection1() {
        return messageCenterCollection1;
    }

    public void setMessageCenterCollection1(Collection<MessageCenter> messageCenterCollection1) {
        this.messageCenterCollection1 = messageCenterCollection1;
    }

    public UserFilesTable getFileId() {
        return fileId;
    }

    public void setFileId(UserFilesTable fileId) {
        this.fileId = fileId;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    @XmlTransient
    public Collection<Userconnectionstable> getUserConnectionsTableCollection() {
        return userConnectionsTableCollection;
    }

    public void setUserConnectionsTableCollection(Collection<Userconnectionstable> userConnectionsTableCollection) {
        this.userConnectionsTableCollection = userConnectionsTableCollection;
    }

    @XmlTransient
    public Collection<Userconnectionstable> getUserConnectionsTableCollection1() {
        return userConnectionsTableCollection1;
    }

    public void setUserConnectionsTableCollection1(Collection<Userconnectionstable> userConnectionsTableCollection1) {
        this.userConnectionsTableCollection1 = userConnectionsTableCollection1;
    }

    @XmlTransient
    public Collection<UserPositionsDetails> getUserPositionsDetailsCollection() {
        return userPositionsDetailsCollection;
    }

    public void setUserPositionsDetailsCollection(Collection<UserPositionsDetails> userPositionsDetailsCollection) {
        this.userPositionsDetailsCollection = userPositionsDetailsCollection;
    }

    @XmlTransient
    public Collection<TokenTable> getTokenTableCollection() {
        return tokenTableCollection;
    }

    public void setTokenTableCollection(Collection<TokenTable> tokenTableCollection) {
        this.tokenTableCollection = tokenTableCollection;
    }

    public ContactTable getContactTable() {
        return contactTable;
    }

    public void setContactTable(ContactTable contactTable) {
        this.contactTable = contactTable;
    }

    @XmlTransient
    public Collection<UserPateAssociations> getUserPateAssociationsCollection() {
        return userPateAssociationsCollection;
    }

    public void setUserPateAssociationsCollection(Collection<UserPateAssociations> userPateAssociationsCollection) {
        this.userPateAssociationsCollection = userPateAssociationsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (linkedinId != null ? linkedinId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usertable)) {
            return false;
        }
        Usertable other = (Usertable) object;
        if ((this.linkedinId == null && other.linkedinId != null) || (this.linkedinId != null && !this.linkedinId.equals(other.linkedinId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.Usertable[ linkedinId=" + linkedinId + " ]";
    }

    @XmlTransient
    public Collection<Userconnectionstable> getUserconnectionstableCollection() {
        return userconnectionstableCollection;
    }

    public void setUserconnectionstableCollection(Collection<Userconnectionstable> userconnectionstableCollection) {
        this.userconnectionstableCollection = userconnectionstableCollection;
    }

    @XmlTransient
    public Collection<Userconnectionstable> getUserconnectionstableCollection1() {
        return userconnectionstableCollection1;
    }

    public void setUserconnectionstableCollection1(Collection<Userconnectionstable> userconnectionstableCollection1) {
        this.userconnectionstableCollection1 = userconnectionstableCollection1;
    }
    
}
