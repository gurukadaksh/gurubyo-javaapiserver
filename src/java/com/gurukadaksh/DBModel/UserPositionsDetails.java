/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "userPositionsDetails", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserPositionsDetails.findAll", query = "SELECT u FROM UserPositionsDetails u"),
    @NamedQuery(name = "UserPositionsDetails.findByLinkedinId", query = "SELECT u FROM UserPositionsDetails u WHERE u.userPositionsDetailsPK.linkedinId = :linkedinId"),
    @NamedQuery(name = "UserPositionsDetails.findByStartDate", query = "SELECT u FROM UserPositionsDetails u WHERE u.startDate = :startDate"),
    @NamedQuery(name = "UserPositionsDetails.findByEndDate", query = "SELECT u FROM UserPositionsDetails u WHERE u.endDate = :endDate"),
    @NamedQuery(name = "UserPositionsDetails.findByTitle", query = "SELECT u FROM UserPositionsDetails u WHERE u.title = :title"),
    @NamedQuery(name = "UserPositionsDetails.findByLinkedinPositionId", query = "SELECT u FROM UserPositionsDetails u WHERE u.userPositionsDetailsPK.linkedinPositionId = :linkedinPositionId")})
public class UserPositionsDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserPositionsDetailsPK userPositionsDetailsPK;
    @Column(name = "startDate")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "endDate")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "summary")
    private String summary;
    @Size(max = 500)
    @Column(name = "title")
    private String title;
    @JoinColumn(name = "linkedinCompanyId", referencedColumnName = "linkedinCompanyId")
    @ManyToOne(fetch = FetchType.LAZY)
    private OrganizationsDatabase linkedinCompanyId;
    @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usertable usertable;

    public UserPositionsDetails() {
    }

    public UserPositionsDetails(UserPositionsDetailsPK userPositionsDetailsPK) {
        this.userPositionsDetailsPK = userPositionsDetailsPK;
    }

    public UserPositionsDetails(String linkedinId, long linkedinPositionId) {
        this.userPositionsDetailsPK = new UserPositionsDetailsPK(linkedinId, linkedinPositionId);
    }

    public UserPositionsDetailsPK getUserPositionsDetailsPK() {
        return userPositionsDetailsPK;
    }

    public void setUserPositionsDetailsPK(UserPositionsDetailsPK userPositionsDetailsPK) {
        this.userPositionsDetailsPK = userPositionsDetailsPK;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public OrganizationsDatabase getLinkedinCompanyId() {
        return linkedinCompanyId;
    }

    public void setLinkedinCompanyId(OrganizationsDatabase linkedinCompanyId) {
        this.linkedinCompanyId = linkedinCompanyId;
    }

    public Usertable getUsertable() {
        return usertable;
    }

    public void setUsertable(Usertable usertable) {
        this.usertable = usertable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userPositionsDetailsPK != null ? userPositionsDetailsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserPositionsDetails)) {
            return false;
        }
        UserPositionsDetails other = (UserPositionsDetails) object;
        if ((this.userPositionsDetailsPK == null && other.userPositionsDetailsPK != null) || (this.userPositionsDetailsPK != null && !this.userPositionsDetailsPK.equals(other.userPositionsDetailsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserPositionsDetails[ userPositionsDetailsPK=" + userPositionsDetailsPK + " ]";
    }
    
}
