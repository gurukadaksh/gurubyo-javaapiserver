/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "contactTable", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContactTable.findAll", query = "SELECT c FROM ContactTable c"),
    @NamedQuery(name = "ContactTable.findByPrimaryEmail", query = "SELECT c FROM ContactTable c WHERE c.primaryEmail = :primaryEmail"),
    @NamedQuery(name = "ContactTable.findByPrimaryPhoneNumber", query = "SELECT c FROM ContactTable c WHERE c.primaryPhoneNumber = :primaryPhoneNumber"),
    @NamedQuery(name = "ContactTable.findByCountryOfResidenceISO", query = "SELECT c FROM ContactTable c WHERE c.countryOfResidenceISO = :countryOfResidenceISO"),
    @NamedQuery(name = "ContactTable.findByLinkedinId", query = "SELECT c FROM ContactTable c WHERE c.linkedinId = :linkedinId")})
public class ContactTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 100)
    @Column(name = "primaryEmail")
    private String primaryEmail;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "alternateEmails")
    private String alternateEmails;
    @Size(max = 100)
    @Column(name = "primaryPhoneNumber")
    private String primaryPhoneNumber;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "alternatePhoneNumbers")
    private String alternatePhoneNumbers;
    @Size(max = 10)
    @Column(name = "countryOfResidence_ISO")
    private String countryOfResidenceISO;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "address")
    private String address;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "linkedinId")
    private String linkedinId;
    @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Usertable usertable;

    public ContactTable() {
    }

    public ContactTable(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public String getPrimaryEmail() {
        return primaryEmail;
    }

    public void setPrimaryEmail(String primaryEmail) {
        this.primaryEmail = primaryEmail;
    }

    public String getAlternateEmails() {
        return alternateEmails;
    }

    public void setAlternateEmails(String alternateEmails) {
        this.alternateEmails = alternateEmails;
    }

    public String getPrimaryPhoneNumber() {
        return primaryPhoneNumber;
    }

    public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
        this.primaryPhoneNumber = primaryPhoneNumber;
    }

    public String getAlternatePhoneNumbers() {
        return alternatePhoneNumbers;
    }

    public void setAlternatePhoneNumbers(String alternatePhoneNumbers) {
        this.alternatePhoneNumbers = alternatePhoneNumbers;
    }

    public String getCountryOfResidenceISO() {
        return countryOfResidenceISO;
    }

    public void setCountryOfResidenceISO(String countryOfResidenceISO) {
        this.countryOfResidenceISO = countryOfResidenceISO;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public Usertable getUsertable() {
        return usertable;
    }

    public void setUsertable(Usertable usertable) {
        this.usertable = usertable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (linkedinId != null ? linkedinId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContactTable)) {
            return false;
        }
        ContactTable other = (ContactTable) object;
        if ((this.linkedinId == null && other.linkedinId != null) || (this.linkedinId != null && !this.linkedinId.equals(other.linkedinId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.ContactTable[ linkedinId=" + linkedinId + " ]";
    }
    
}
