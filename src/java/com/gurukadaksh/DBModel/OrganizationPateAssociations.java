/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "organizationPateAssociations", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrganizationPateAssociations.findAll", query = "SELECT o FROM OrganizationPateAssociations o"),
    @NamedQuery(name = "OrganizationPateAssociations.findByLinkedinCompanyId", query = "SELECT o FROM OrganizationPateAssociations o WHERE o.organizationPateAssociationsPK.linkedinCompanyId = :linkedinCompanyId"),
    @NamedQuery(name = "OrganizationPateAssociations.findByPateKeyId", query = "SELECT o FROM OrganizationPateAssociations o WHERE o.organizationPateAssociationsPK.pateKeyId = :pateKeyId"),
    @NamedQuery(name = "OrganizationPateAssociations.findByKeyType", query = "SELECT o FROM OrganizationPateAssociations o WHERE o.keyType = :keyType"),
    @NamedQuery(name = "OrganizationPateAssociations.findByKeyWeight", query = "SELECT o FROM OrganizationPateAssociations o WHERE o.keyWeight = :keyWeight")})
public class OrganizationPateAssociations implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrganizationPateAssociationsPK organizationPateAssociationsPK;
    @Column(name = "keyType")
    private Boolean keyType;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "keyWeight")
    private Float keyWeight;
    @JoinColumn(name = "linkedinCompanyId", referencedColumnName = "linkedinCompanyId", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private OrganizationsDatabase organizationsDatabase;

    public OrganizationPateAssociations() {
    }

    public OrganizationPateAssociations(OrganizationPateAssociationsPK organizationPateAssociationsPK) {
        this.organizationPateAssociationsPK = organizationPateAssociationsPK;
    }

    public OrganizationPateAssociations(long linkedinCompanyId, long pateKeyId) {
        this.organizationPateAssociationsPK = new OrganizationPateAssociationsPK(linkedinCompanyId, pateKeyId);
    }

    public OrganizationPateAssociationsPK getOrganizationPateAssociationsPK() {
        return organizationPateAssociationsPK;
    }

    public void setOrganizationPateAssociationsPK(OrganizationPateAssociationsPK organizationPateAssociationsPK) {
        this.organizationPateAssociationsPK = organizationPateAssociationsPK;
    }

    public Boolean getKeyType() {
        return keyType;
    }

    public void setKeyType(Boolean keyType) {
        this.keyType = keyType;
    }

    public Float getKeyWeight() {
        return keyWeight;
    }

    public void setKeyWeight(Float keyWeight) {
        this.keyWeight = keyWeight;
    }

    public OrganizationsDatabase getOrganizationsDatabase() {
        return organizationsDatabase;
    }

    public void setOrganizationsDatabase(OrganizationsDatabase organizationsDatabase) {
        this.organizationsDatabase = organizationsDatabase;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (organizationPateAssociationsPK != null ? organizationPateAssociationsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganizationPateAssociations)) {
            return false;
        }
        OrganizationPateAssociations other = (OrganizationPateAssociations) object;
        if ((this.organizationPateAssociationsPK == null && other.organizationPateAssociationsPK != null) || (this.organizationPateAssociationsPK != null && !this.organizationPateAssociationsPK.equals(other.organizationPateAssociationsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.OrganizationPateAssociations[ organizationPateAssociationsPK=" + organizationPateAssociationsPK + " ]";
    }
    
}
