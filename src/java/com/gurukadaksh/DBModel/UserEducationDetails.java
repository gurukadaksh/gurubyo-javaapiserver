/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.DBModel;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deepaksubramanian
 */
@Entity
@Table(name = "userEducationDetails", catalog = "gurukadakshDB", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserEducationDetails.findAll", query = "SELECT u FROM UserEducationDetails u"),
    @NamedQuery(name = "UserEducationDetails.findByLinkedinId", query = "SELECT u FROM UserEducationDetails u WHERE u.userEducationDetailsPK.linkedinId = :linkedinId"),
    @NamedQuery(name = "UserEducationDetails.findByLinkedinEducationId", query = "SELECT u FROM UserEducationDetails u WHERE u.userEducationDetailsPK.linkedinEducationId = :linkedinEducationId"),
    @NamedQuery(name = "UserEducationDetails.findByStartYear", query = "SELECT u FROM UserEducationDetails u WHERE u.startYear = :startYear"),
    @NamedQuery(name = "UserEducationDetails.findByEndYear", query = "SELECT u FROM UserEducationDetails u WHERE u.endYear = :endYear"),
    @NamedQuery(name = "UserEducationDetails.findBySchoolName", query = "SELECT u FROM UserEducationDetails u WHERE u.schoolName = :schoolName"),
    @NamedQuery(name = "UserEducationDetails.findByDegree", query = "SELECT u FROM UserEducationDetails u WHERE u.degree = :degree")})
public class UserEducationDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserEducationDetailsPK userEducationDetailsPK;
    @Size(max = 4)
    @Column(name = "startYear")
    private String startYear;
    @Size(max = 4)
    @Column(name = "endYear")
    private String endYear;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "fieldOfStudy")
    private String fieldOfStudy;
    @Size(max = 500)
    @Column(name = "schoolName")
    private String schoolName;
    @Size(max = 500)
    @Column(name = "degree")
    private String degree;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "activities")
    private String activities;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "notes")
    private String notes;
    @JoinColumn(name = "linkedinId", referencedColumnName = "linkedinId", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usertable usertable;

    public UserEducationDetails() {
    }

    public UserEducationDetails(UserEducationDetailsPK userEducationDetailsPK) {
        this.userEducationDetailsPK = userEducationDetailsPK;
    }

    public UserEducationDetails(String linkedinId, long linkedinEducationId) {
        this.userEducationDetailsPK = new UserEducationDetailsPK(linkedinId, linkedinEducationId);
    }

    public UserEducationDetailsPK getUserEducationDetailsPK() {
        return userEducationDetailsPK;
    }

    public void setUserEducationDetailsPK(UserEducationDetailsPK userEducationDetailsPK) {
        this.userEducationDetailsPK = userEducationDetailsPK;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getActivities() {
        return activities;
    }

    public void setActivities(String activities) {
        this.activities = activities;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Usertable getUsertable() {
        return usertable;
    }

    public void setUsertable(Usertable usertable) {
        this.usertable = usertable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userEducationDetailsPK != null ? userEducationDetailsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserEducationDetails)) {
            return false;
        }
        UserEducationDetails other = (UserEducationDetails) object;
        if ((this.userEducationDetailsPK == null && other.userEducationDetailsPK != null) || (this.userEducationDetailsPK != null && !this.userEducationDetailsPK.equals(other.userEducationDetailsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gurukadaksh.DBModel.UserEducationDetails[ userEducationDetailsPK=" + userEducationDetailsPK + " ]";
    }
    
}
