/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;


/**
 *
 * @author Deepak Subramanian
 */
public class linkedInInfo
{
    public static final boolean debugMode = true;
    private static class LoginHelper
    {
        private String callbackUrl_UI;
        private Token requestToken;

        public LoginHelper(String callbackUrl_UI, Token requestToken)
        {
            this.callbackUrl_UI = callbackUrl_UI;
            this.requestToken = requestToken;
        }

        public String getCallbackUrl_UI()
        {
            return callbackUrl_UI;
        }

        public void setCallbackUrl_UI(String callbackUrl_UI)
        {
            this.callbackUrl_UI = callbackUrl_UI;
        }

        public Token getRequestToken()
        {
            return requestToken;
        }

        public void setRequestToken(Token requestToken)
        {
            this.requestToken = requestToken;
        }

    };
    static HashMap<String,LoginHelper> authenticationRTokens = new HashMap<String, LoginHelper>();
    private static final String LINKEDIN_API_KEY = "xl6ofnjarw4b";
    private static final String LINKEDIN_API_SECRET = "HnFiHzovZ7n5ZCgC";    
    public static final String GURUKADAKSH_TOKEN = "gkToken";
    /**
     * This function is used to get the authentication url for the request.
     */
    public static String getAuthenticationUrl( String callbackUrl_thisServer, String callbackUrl_UI )
    {
        OAuthService service;
        Token requestToken;
        
        service = new ServiceBuilder()
                                    .provider(LinkedInApi.class)
                                    .apiKey(LINKEDIN_API_KEY)
                                    .apiSecret(LINKEDIN_API_SECRET)
                                    .callback(callbackUrl_thisServer)
                                    .build();
        // Obtain the Request Token
        requestToken = service.getRequestToken();
        //Get Authorization URL
        String authorizationURL = service.getAuthorizationUrl(requestToken);
        //authorizationURL = authorizationURL.replace("api", "www").replace("authorize", "authenticate");
        LoginHelper loginHelper = new LoginHelper(callbackUrl_UI, requestToken);
        authenticationRTokens.put(requestToken.getToken(), loginHelper);
        return authorizationURL;
    }
/**
 * This method is used to get the access token and login to the system.
 * @param oauth_request_token - The login token (request token that was sent)
 * @param verifierSent - The verifier that needs to be used for obtaining the final token.
 * @return
 */
    public static String performLogin( String oauth_request_token, String verifierSent )
    {
        Token accessToken;
        String callbackUrl_UI = "";
        try
        {
            OAuthService service;
            
            Token requestToken;
            Verifier verifier = new Verifier(verifierSent);
            LoginHelper loginHelper = authenticationRTokens.get(oauth_request_token);
            //Reconstruct Service
            service = new ServiceBuilder()
                                        .provider(LinkedInApi.class)
                                        .apiKey(LINKEDIN_API_KEY)
                                        .apiSecret(LINKEDIN_API_SECRET).build();
            callbackUrl_UI = loginHelper.getCallbackUrl_UI();

            accessToken = service.getAccessToken(loginHelper.getRequestToken(), verifier);
        }
        catch(Exception e)
        {
            accessToken = Token.empty();
        }
        if (accessToken != null && !accessToken.isEmpty())
        {
            authenticationRTokens.remove(oauth_request_token);
            TokenManager tokenManager = new TokenManager(accessToken);
            callbackUrl_UI = appendParametersToUrl(callbackUrl_UI,"login=1&"+GURUKADAKSH_TOKEN+"="+tokenManager.getGkToken());
        }
        else
        {
            callbackUrl_UI += appendParametersToUrl(callbackUrl_UI,"login=0");
        }
        return callbackUrl_UI;
    }

    public static String appendParametersToUrl(String urlString, String parameterString)
    {
        if (urlString.contains("?"))
        {
            return urlString.substring(0, urlString.indexOf("?")) + parameterString+ ((urlString.endsWith("?"))? "&"+urlString.substring(urlString.indexOf("?")+1):"");
        }
        else
        {
            return urlString + "?" + parameterString;
        }
    }

    /**
     * This function returns the linkedin result when the profile is extracted.
     * @param token
     * @return
     */
    public static String extractUserProfile(Token accessToken)
    {
        Logger.getLogger(linkedInInfo.class.getName()).log(Level.INFO, "***** Started extracting user profile");
        OAuthRequest request = new OAuthRequest(Verb.GET, LinkedinURLs.PROFILE_URL);
         Logger.getLogger(linkedInInfo.class.getName()).log(Level.INFO, "***** Started extracting user profile - 1");
        OAuthService service = new ServiceBuilder()
                                        .provider(LinkedInApi.class)
                                        .apiKey(LINKEDIN_API_KEY)
                                        .apiSecret(LINKEDIN_API_SECRET).build();
        Logger.getLogger(linkedInInfo.class.getName()).log(Level.INFO, "***** Started extracting user profile - 2");
        service.signRequest(accessToken, request);
        Logger.getLogger(linkedInInfo.class.getName()).log(Level.INFO, "***** Started extracting user profile - 3");
        Response response = request.send();
        Logger.getLogger(linkedInInfo.class.getName()).log(Level.INFO, "***** Started extracting user profile - 4");
        return response.getBody();
    }
    public static String extractAdvancedUserProfile(Token accessToken)
    {
        OAuthRequest request = new OAuthRequest(Verb.GET, LinkedinURLs.ADVACED_PROFILE_URL);
        OAuthService service = new ServiceBuilder()
                                        .provider(LinkedInApi.class)
                                        .apiKey(LINKEDIN_API_KEY)
                                        .apiSecret(LINKEDIN_API_SECRET).build();
        service.signRequest(accessToken, request);
        Response response = request.send();
        return response.getBody();
    }
    public static String extractUserConnections( Token accessToken )
    {
        OAuthRequest request = new OAuthRequest(Verb.GET, LinkedinURLs.CONNECTIONS_URL);
        OAuthService service = new ServiceBuilder()
                                        .provider(LinkedInApi.class)
                                        .apiKey(LINKEDIN_API_KEY)
                                        .apiSecret(LINKEDIN_API_SECRET).build();
        service.signRequest(accessToken, request);
        Response response = request.send();
        return response.getBody();
    }
    public static String extractUserSkills( Token accessToken )
    {
       // id, skills
        OAuthRequest request = new OAuthRequest(Verb.GET, LinkedinURLs.SKILLS_URL);
        OAuthService service = new ServiceBuilder()
                                        .provider(LinkedInApi.class)
                                        .apiKey(LINKEDIN_API_KEY)
                                        .apiSecret(LINKEDIN_API_SECRET).build();
        service.signRequest(accessToken, request);
        Response response = request.send();
        return response.getBody();
    }
    /**
     * This is used 
     * @param accessToken
     * @param companyId
     * @return 
     */
    public static String extractCompanyInformation( Token accessToken, Long companyId )
    {
        OAuthRequest request = new OAuthRequest(Verb.GET, LinkedinURLs.getCompanyUrl(companyId));
        OAuthService service = new ServiceBuilder()
                                        .provider(LinkedInApi.class)
                                        .apiKey(LINKEDIN_API_KEY)
                                        .apiSecret(LINKEDIN_API_SECRET).build();
        service.signRequest(accessToken, request);
        Response response = request.send();
        return response.getBody();
    }
    public static void sign_oauthTokens()
    {
        Token accessToken;
    }

    
}
