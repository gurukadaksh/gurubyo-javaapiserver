/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh;

import com.gurukadaksh.DBModel.*;
import com.gurukadaksh.DBModel.CustomCreated.OrganizationSkills;
import com.gurukadaksh.DBModel.CustomCreated.OrganizationSkillsPK;
import com.gurukadaksh.DBModel.CustomCreated.UserSkillsTable;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.model.Token;


/**
 *
 * @author Deepak Subramanian
 */
public class BackendOperator
{
//
//    /**
//     * Add a skill to the database and to the Lucene index.
//     * @param speciality
//     * @return 
//     */
//    static Long skillAdd(String speciality) 
//    {
//        Long skillId = 0L;
//        EntityManagerFactory enf = null;
//        EntityManager em = null;
//        try
//        {
//
//            enf = Persistence.createEntityManagerFactory("GurukadakshPU");
//            em = enf.createEntityManager();
//            em.getTransaction().begin();
//            
//            
//        }
//        catch ( Exception exception )
//        {
//            skillId = 0L;
//        }
//        finally
//        {
//            em.close();
//            enf.close();
//            return skillId;
//        }
//    }
    private Token accessToken = null;
    private PrintWriter out = null;

    public void setOut(PrintWriter out)
    {
        this.out = out;
    }

    public BackendOperator(Token accessToken)
    {
        this.accessToken = accessToken;
    }
    /**
     * This function is used to get the user's connections, profile and skills and save them.
     * @param utable
     */
    public Usertable createOrUpdateUserTable( ) throws JSONException
    {
        

        JSONObject job = new JSONObject(linkedInInfo.extractUserProfile(accessToken));


        if ( job != null )
        {
            
            
            //Element person_1 = (Element)person.item(0);
            String firstName = job.optString(LinkedinURLs.LinkedInResponseParameters.FIRST_NAME).trim();
            String lastName = job.optString(LinkedinURLs.LinkedInResponseParameters.LAST_NAME).trim();
            String headline = job.optString(LinkedinURLs.LinkedInResponseParameters.HEADLINE).trim();
            String pictureUrl = job.optString(LinkedinURLs.LinkedInResponseParameters.PICTURE_URL).trim();
            String linkedinId = job.getString(LinkedinURLs.LinkedInResponseParameters.LINKEDIN_ID).trim();
            Usertable utable = createOrUpdateUser(linkedinId, firstName, headline, lastName, pictureUrl);
            if(utable != null)
            {
                BackgroundSilent_ExtractSkillsAndConnections.extractNow(accessToken, utable);
            }
            return utable;
        }
        else
        {
            
            return null;
        }
      }  //NodeList firstname = doc.getElementsByTagName("first-name");
    protected static Usertable createOrUpdateUser( String linkedinId, String firstName, String headline, String lastName, String pictureUrl)
    {
        if ( linkedinId.equals("private")) return null;
        
            EntityManagerFactory enf = null;
            EntityManager em = null;
            enf = Persistence.createEntityManagerFactory("GurukadakshPU");
            em = enf.createEntityManager();
            em.getTransaction().begin();

            TypedQuery<Usertable> existingUser = em.createNamedQuery("Usertable.findByLinkedinId", Usertable.class);
            try
            {
                List<Usertable> userList = existingUser.setParameter("linkedinId", linkedinId).getResultList();
                if ( !userList.isEmpty() )
                {
                    Usertable utable = userList.get(0);
                    utable.setFirstName(firstName);
                    utable.setHeadline(headline);
                    utable.setLastName(lastName);
                    utable.setPictureURL(pictureUrl);
                    em.getTransaction().commit();
                    em.close();
                    enf.close();
                    
                    return utable;
                }
                else
                    throw new Exception("Reached else");
            }
            catch (Exception exception)
            {
                Usertable utable = new Usertable();
                utable.setLinkedinId(linkedinId);
                utable.setFirstName(firstName);
                utable.setHeadline(headline);
                utable.setLastName(lastName);
                utable.setPictureURL(pictureUrl);
                em.persist(utable);
                em.getTransaction().commit();
                em.close();
                enf.close();

                return utable;
            }
    }
    
}
/**
 * This class implements a background thread to speed up the result output for the normal operations. The background threads run silently and extract the user's skills and connections right now.
 * @author Deepak Subramanian
 */
class BackgroundSilent_ExtractSkillsAndConnections implements Runnable
{
    Usertable utable = null; ///< The user table that needs to be set for the functionalities to be completed
    Token accessToken = null; ///< The accessToken for the linkedin user.
    /**
     * This static function provides a simple way to start the thread.
     * @param accessToken
     * @param utable
     */
    public static void extractNow(Token accessToken, Usertable utable)
    {
        BackgroundSilent_ExtractSkillsAndConnections skillsAndConnections = new BackgroundSilent_ExtractSkillsAndConnections();
        skillsAndConnections.setAccessToken(accessToken);
        skillsAndConnections.setUtable(utable);
        Thread extractNow = new Thread(skillsAndConnections);
        extractNow.start();
    }

    public Token getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(Token accessToken) {
        this.accessToken = accessToken;
    }

    public Usertable getUtable() {
        return utable;
    }

    public void setUtable(Usertable utable)
    {
        this.utable = utable;
    }

    @Override
    public void run()
    {

        try
        {
            getAndSaveConnections(utable);
            getAndSaveSkills(utable);
            getAndSaveAdvancedProfile(utable);
            callPate(utable);
        } 
        catch (Exception ex)
        {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "run{0}", ex.getMessage());
        }
    }
    /**
     * This function is used to get the linkedin connections.
     * @param utable
     */
    private void getAndSaveConnections(Usertable utable)
    {
        try
        {
            if ( utable == null ) return;
            String userConnections = linkedInInfo.extractUserConnections(accessToken);
            //out.print("Connections started . . . . . ");

            JSONObject job = new JSONObject(userConnections);

            if ( job != null )
            {
                JSONArray connections = job.getJSONArray(LinkedinURLs.LinkedInResponseParameters.RESULT_LIST);
                for ( int i = 0 ; i < connections.length() ; i++ )
                {
                    JSONObject connection = connections.getJSONObject(i);
                    String firstName = connection.getString(LinkedinURLs.LinkedInResponseParameters.FIRST_NAME).trim();
                    String lastName = connection.optString(LinkedinURLs.LinkedInResponseParameters.LAST_NAME).trim();
                    String headline = connection.optString(LinkedinURLs.LinkedInResponseParameters.HEADLINE).trim();
                    String pictureUrl = connection.optString(LinkedinURLs.LinkedInResponseParameters.PICTURE_URL).trim();
                    String linkedinId = connection.getString(LinkedinURLs.LinkedInResponseParameters.LINKEDIN_ID).trim();
                    ///< The various details have been obtained from the JSON file.. Now create the  connections
                    Usertable connectionTable =  BackendOperator.createOrUpdateUser(linkedinId, firstName, headline, lastName, pictureUrl);

                    if (connectionTable == null) continue;

                    EntityManagerFactory enf = null;
                    EntityManager em = null;
                    try
                    {
                        //out.println("Out1");
                        enf = Persistence.createEntityManagerFactory("GurukadakshPU");
                        em = enf.createEntityManager();
                        em.getTransaction().begin();
                        Userconnectionstable uct = new Userconnectionstable();
                        //out.println("Out2");
                        int compResult = connectionTable.getLinkedinId().compareTo(utable.getLinkedinId());
                        //out.println("compResult:    "+ compResult);
                        if ( compResult != 0 )
                        {
                            //out.println("Out3");
                            boolean shouldPersist = true;
                            if ( compResult > 0 )
                            {
                                uct.setUsertable(utable);
                                uct.setUserconnectionstablePK(new UserconnectionstablePK(utable.getLinkedinId(), connectionTable.getLinkedinId()));
                                uct.setUsertable1(connectionTable);
                                //out.println("Out4");
                                if(utable.getUserConnectionsTableCollection1().contains(uct)) shouldPersist = false;

                            }
                            else
                            {
                                //out.println("Out5");
                                uct.setUsertable1(utable);
                                uct.setUsertable(connectionTable);
                                uct.setUserconnectionstablePK(new UserconnectionstablePK(connectionTable.getLinkedinId(),utable.getLinkedinId() ));
                                if(utable.getUserConnectionsTableCollection().contains(uct)) shouldPersist = false;

                            }
                            //out.println("Out6");
                            uct.setIsLinkedIn(true);
                            //out.println("Out7");
                            if( shouldPersist ) em.persist(uct);
                            em.getTransaction().commit();

                        }

                        em.close();
                        enf.close();
                    }
                    catch(Exception e)
                    {
                        //out.println("===============================================\n"
                        //        + "Exception is "+ e+
                        //        "===============================================\n");
                        em.getTransaction().rollback();
                        em.close();
                        enf.close();
                    }
                    finally
                    {

                    }
                }
            }
        }
        catch (Exception exception)
        {
            //out.print("Exception Occurred"+exception);
        }
        finally
        {

        }
    }
/**
 * This function is the main function that is used to call the skills api at linkedin and save all the user skill information for future retrieval.
 * @param utable
 */
    private void getAndSaveSkills(Usertable utable)
    {
        try
        {
            if ( utable == null ) return;
            String userSkills = linkedInInfo.extractUserSkills(accessToken);
            //out.print("Connections started . . . . . ");
            
            if (userSkills.startsWith("{") && userSkills.endsWith("}"))
            {
            JSONObject job = new JSONObject(userSkills);

            if ( job != null )
            {
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Skills insertion JSON is " + job.toString());

                JSONArray skillsArray = job.getJSONArray(LinkedinURLs.LinkedInResponseParameters.RESULT_LIST);
                for ( int i = 0 ; i < skillsArray.length() ; i++)
                {
                    JSONObject skillArrayElement = skillsArray.getJSONObject(i);
                    JSONObject skillElement = skillArrayElement.getJSONObject(LinkedinURLs.LinkedInResponseParameters.SKILL_DICTIONARY);
                    String skillName = skillElement.getString(LinkedinURLs.LinkedInResponseParameters.SKILL_NAME);
                    SkillsDatabase skillDBElement = checkSkillExists(skillName);
                    if ( skillDBElement == null )
                    {
                        skillDBElement = createSkillElement(skillName);
                    }
                    if ( skillDBElement != null )
                    {
                        updateUserSkill(utable,skillDBElement);
                    }
                }
            }
        }
            else
            {
                if(userSkills != null)
                {
                    SkillsDatabase skillDBElement = checkSkillExists(userSkills);
                    if ( skillDBElement == null )
                    {
                        skillDBElement = createSkillElement(userSkills);
                    }
                    if ( skillDBElement != null )
                    {
                        updateUserSkill(utable,skillDBElement);
                    }
                }
            }
        }
        catch (Exception exception)
        {
              Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Skills insertion exception is " + exception);
        }
    }
    /**
     * If the skill exists in the database, then return the object SkillsDatabase that contains the skillname, skillid else return null.
     * @param skillName - The string that represents the skill name
     * @return
     */
    public static SkillsDatabase checkSkillExists(String skillName)
    {
        SkillsDatabase returnValue = null; ///< Setting the initial parameter
        EntityManagerFactory enf = null; ///< Starting the database components
        EntityManager em = null;
        try
        {

            enf = Persistence.createEntityManagerFactory("GurukadakshPU"); ///< Actual database link
            em = enf.createEntityManager();
            em.getTransaction().begin(); ///< Begin the transaction
            TypedQuery<SkillsDatabase> existingUser = em.createNamedQuery("SkillsDatabase.findBySkillName", SkillsDatabase.class);
            List<SkillsDatabase> userList = existingUser.setParameter("skillName", skillName).getResultList(); ///< getResultList is prefered here since it does not throw an exception if the result is not found unlike the getFirstResult and getSingleResult
            if ( !userList.isEmpty() ) ///<  If the user list is not empty it implies that the skill is present in the database.
            {
                returnValue = userList.get(0); ///< There is only one element possible
                if ( returnValue.getAddedByPate() )
                {
                    returnValue.setAddedByPate(Boolean.FALSE);
                    em.flush();
                    em.getTransaction().commit();
                }
            }
            else
            {
                returnValue = null; ///< If the user list is empty, return null
            }
        }
        catch (Exception exception)
        {
            returnValue = null; ///< If something goes wrong, return null. [Like database connections at max, lock due to something else etc.,]
        }
        finally
        {
            em.close();
            enf.close();
            return returnValue;
        }
    }

    /**
     * This function creates a skill element and returns null if creation fails. NOTE: THIS FUNCTION DOES NOT CHECK IF THE SKILL EXISTS. TO CHECK IF SKILL EXISTS USE THE FUNCTION checkSkillExists(String skillName)
     * @param skillName
     * @return
     */
    private SkillsDatabase createSkillElement(String skillName)
    {
        SkillsDatabase skillsDatabase = null;
        EntityManagerFactory enf = null;
        EntityManager em = null;
        try
        {
            enf = Persistence.createEntityManagerFactory("GurukadakshPU");
            em = enf.createEntityManager();
            em.getTransaction().begin();

            skillsDatabase = new SkillsDatabase();
//            TypedQuery<SkillsDatabase> maxSkillQuery = em.createNamedQuery("SkillsDatabase.findByIdOrderDesc", SkillsDatabase.class);
//            //Query maxSkillQuery=em.createQuery("SELECT s FROM SkillsDatabase s WHERE s.skillId = (SELECT MAX(s.skillId) FROM s)");
//             List<SkillsDatabase> maxSkill = maxSkillQuery.setMaxResults(1).getResultList();
//             if(maxSkill != null)
//             {
//                 if(maxSkill.isEmpty()) skillsDatabase.setSkillId(new Long(1));
//                 else skillsDatabase.setSkillId(maxSkill.get(0).getSkillId() + 1);
                 skillsDatabase.setSkillName(skillName);
                 skillsDatabase.setAddedByPate(Boolean.FALSE);
                 Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "INSERTING INTO SKILLS DB", skillsDatabase);
                 em.persist(skillsDatabase);
                 em.getTransaction().commit();
                 
                 LuceneOperations luceneOperations = new LuceneOperations();
                 ArrayList<LuceneMap> listOfElements = new ArrayList<LuceneMap>();
                 LuceneMap currentLuceneElement = new LuceneMap();
                 currentLuceneElement.setId(skillsDatabase.getSkillId().toString());
                 currentLuceneElement.setName(skillName);
                 currentLuceneElement.setType("Skill");
                 listOfElements.add(currentLuceneElement);
                 luceneOperations.addSpecificValueToIndex(listOfElements);
//             }
        }
        catch ( Exception exception )
        {
              Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "createSkillElement {0}", exception.getMessage());
              skillsDatabase = null;
//            try
//            {
//
//            }
//            catch( Exception ex )
//            {
                em.getTransaction().rollback();
//            }
        }
        finally
        {
            em.close();
            enf.close();
            return skillsDatabase;
        }
    }
/**
 * This funcion is used to update the user skill for the given user. The skills are added if they are not already present.
 * @param utable
 * @param skillDBElement
 */
    private void updateUserSkill(Usertable utable, SkillsDatabase skillDBElement)
    {
        EntityManagerFactory enf = null;
        EntityManager em = null;
        try
        {
            if(! utable.getSkillsDatabaseCollection().contains(skillDBElement)) ///< Check if the element is already present in the database
            {
                enf = Persistence.createEntityManagerFactory("GurukadakshPU");
                em = enf.createEntityManager();
                em.getTransaction().begin();
                UserSkillsTable userSkillsTable = new UserSkillsTable(utable.getLinkedinId(), skillDBElement.getSkillId());
                try
                {
                    em.persist(userSkillsTable);
                }
                catch(Exception e)
                {

                }
                em.getTransaction().commit();
            }

        }
        catch ( Exception exception )
        {
            em.getTransaction().rollback();
        }
        finally
        {
            em.close();
            enf.close();
        }
    }
/**
 * This function will get the complete profile info and save it in the various user details sections.
 * @param utable 
 */
    private void getAndSaveAdvancedProfile(Usertable utable) 
    {
       
        try
        {
            
             if ( utable == null ) return;
            String advancedUserProfile = linkedInInfo.extractAdvancedUserProfile(accessToken);
            //out.print("Connections started . . . . . ");
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, advancedUserProfile);
            JSONObject job = new JSONObject(advancedUserProfile);
            AdvancedProfile advancedProfile = new AdvancedProfile();
            advancedProfile.setSummary( job.optString(LinkedinURLs.LinkedInResponseParameters.SUMMARY).trim() );
            advancedProfile.setSpecialities(job.optString(LinkedinURLs.LinkedInResponseParameters.SPECIALITIES).trim());
            advancedProfile.setHonors(job.optString(LinkedinURLs.LinkedInResponseParameters.HONORS).trim());
            advancedProfile.setIndustry(job.optString(LinkedinURLs.LinkedInResponseParameters.INDUSTRY).trim());
            advancedProfile.setLocation(job.optJSONObject(LinkedinURLs.LinkedInResponseParameters.LOCATION).optJSONObject(LinkedinURLs.LinkedInResponseParameters.COUNTRY_OUTER).optString(LinkedinURLs.LinkedInResponseParameters.COUNTRY_CODE).trim());
            advancedProfile.setLastModifiedTime(job.optString(LinkedinURLs.LinkedInResponseParameters.LAST_MODIFIED));
            advancedProfile.setmRssFeed(job.optString(LinkedinURLs.LinkedInResponseParameters.RSS_URL));
            advancedProfile.setProposalComments(job.optString(LinkedinURLs.LinkedInResponseParameters.PROPOSAL_COMMENTS));
            advancedProfile.setLinkedinId(utable.getLinkedinId());
            
Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Advanced Profile done " );
             //Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "1");
            //Education Stuff
            JSONArray educationArray = job.getJSONObject(LinkedinURLs.LinkedInResponseParameters.EDUCATION).getJSONArray(LinkedinURLs.LinkedInResponseParameters.ARRAY_VALUES);
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 1" );

            for ( int i = 0 ; i < educationArray.length() ; i++ )
            {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 2" );
                JSONObject education = educationArray.optJSONObject(i);
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 3" );
                if ( education != null )
                {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 4" );
                    AdvancedProfile.Education currentEducation = advancedProfile.createEducationObject();
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 5" );
                    currentEducation.setActivities(education.optString(LinkedinURLs.LinkedInResponseParameters.EDUCATION_ACTIVITIES));
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 6" );
                    currentEducation.setDegree(education.optString(LinkedinURLs.LinkedInResponseParameters.EDUCATION_DEGREE));
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 7" );
                    currentEducation.setEducationId(education.optLong(LinkedinURLs.LinkedInResponseParameters.EDUCATION_ID));
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 8" );
                    JSONObject startDate = education.optJSONObject(LinkedinURLs.LinkedInResponseParameters.DATE_START);
                    if (startDate != null)
                    {
                        currentEducation.setStartYear(startDate.optString(LinkedinURLs.LinkedInResponseParameters.DATE_YEAR));
                    }
                    JSONObject endDate = education.optJSONObject(LinkedinURLs.LinkedInResponseParameters.DATE_END);
                    if (endDate != null)
                    {
                        currentEducation.setEndYear(endDate.optString(LinkedinURLs.LinkedInResponseParameters.DATE_YEAR));
                    }
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 9" );
//                    currentEducation.setEndYear(education.optJSONObject(LinkedinURLs.LinkedInResponseParameters.DATE_END).optString(LinkedinURLs.LinkedInResponseParameters.DATE_YEAR));
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 10" );
                    currentEducation.setFieldOfStudy(education.optString(LinkedinURLs.LinkedInResponseParameters.EDUCATION_FIELD_OF_STUDY));
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 11" );
                    currentEducation.setNotes(education.optString(LinkedinURLs.LinkedInResponseParameters.EDUCATION_NOTES));
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 12" );
                    currentEducation.setSchoolName(education.optString(LinkedinURLs.LinkedInResponseParameters.EDUCATION_SCHOOL_NAME));
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "E 13" );
                    advancedProfile.educationList.add(currentEducation);
                }
            }
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Education Profile done " );
           // Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "2");
            //Company Stuff
            JSONArray positionArray = job.getJSONObject(LinkedinURLs.LinkedInResponseParameters.POSITION).getJSONArray(LinkedinURLs.LinkedInResponseParameters.ARRAY_VALUES);
            //Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "2a");
            for ( int i = 0 ; i < positionArray.length() ; i++ )
            {
                //Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "2b - {0}",i);
                JSONObject position = positionArray.optJSONObject(i);
                //Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "2b - {0} - 1");
                if ( position != null )
                {
                    
                    AdvancedProfile.Position currentPosition = advancedProfile.createPositionObject();
                    JSONObject company = position.optJSONObject(LinkedinURLs.LinkedInResponseParameters.COMPANY);
                    currentPosition.setId(position.getLong(LinkedinURLs.LinkedInResponseParameters.POSITION_ID));
                    currentPosition.setSummary(position.optString(LinkedinURLs.LinkedInResponseParameters.POSITION_SUMMARY));
                    currentPosition.setTitle(position.optString(LinkedinURLs.LinkedInResponseParameters.POSITION_TITLE));
                    JSONObject dateStart = position.optJSONObject(LinkedinURLs.LinkedInResponseParameters.DATE_START);
                    if (dateStart != null)
                    {
                        currentPosition.setStartMonth(dateStart.optInt(LinkedinURLs.LinkedInResponseParameters.DATE_MONTH));
                        currentPosition.setStartYear(dateStart.optInt(LinkedinURLs.LinkedInResponseParameters.DATE_YEAR));
                    }
                     JSONObject dateEnd = position.optJSONObject(LinkedinURLs.LinkedInResponseParameters.DATE_END);
                    if ( dateEnd != null)                  
                    {
                        currentPosition.setEndMonth(dateEnd.optInt(LinkedinURLs.LinkedInResponseParameters.DATE_MONTH));                       
                        currentPosition.setEndYear(dateEnd.optInt(LinkedinURLs.LinkedInResponseParameters.DATE_YEAR));
                    }
                    currentPosition.setIsCurrent(position.optBoolean(LinkedinURLs.LinkedInResponseParameters.POSITION_ISCURRENT));
                    currentPosition.setCompany_industry(company.optString(LinkedinURLs.LinkedInResponseParameters.COMPANY_INDUSTRY));
                    currentPosition.setCompany_name(company.optString(LinkedinURLs.LinkedInResponseParameters.COMPANY_NAME));
                    currentPosition.setCompany_type(company.optString(LinkedinURLs.LinkedInResponseParameters.COMPANY_TYPE));
                    
                    currentPosition.setCompany_id(company.optLong(LinkedinURLs.LinkedInResponseParameters.COMPANY_ID));      
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "CURRENT POSITION :"+ currentPosition.toString());
                    advancedProfile.positionList.add(currentPosition);
                }
            }
            //Usertable utable = createOrUpdateUser(linkedinId, firstName, headline, lastName, pictureUrl);
            UserDetails userDetailsElement = checkUserDetailsExists(advancedProfile);
            if ( userDetailsElement == null )
            {
                userDetailsElement = createUserDetailsElement(advancedProfile);
            }
            if ( userDetailsElement != null )
            {
                updateUserDetails(userDetailsElement,advancedProfile);
            }
        }
        catch (Exception exception)
        {
             Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "getAndSaveAdvancedProfile: " + exception);
        }
        finally
        {
            
        }
    }
/**
 * This function checks if the user exists in the database.
 * @param advancedProfile
 * @return 
 */
    private UserDetails checkUserDetailsExists(AdvancedProfile advancedProfile) 
    {
        UserDetails returnValue = null; ///< Setting the initial parameter
        EntityManagerFactory enf = null; ///< Starting the database components
        EntityManager em = null;
        try
        {

            enf = Persistence.createEntityManagerFactory("GurukadakshPU"); ///< Actual database link
            em = enf.createEntityManager();
            em.getTransaction().begin(); ///< Begin the transaction
            TypedQuery<UserDetails> existingUser = em.createNamedQuery("UserDetails.findByLinkedinId", UserDetails.class);
            List<UserDetails> userList = existingUser.setParameter("linkedinId", advancedProfile.getLinkedinId()).getResultList(); ///< getResultList is prefered here since it does not throw an exception if the result is not found unlike the getFirstResult and getSingleResult
            if ( !userList.isEmpty() ) ///<  If the user list is not empty it implies that the skill is present in the database.
            {
                returnValue = userList.get(0); ///< There is only one element possible
            }
            else
            {
                returnValue = null; ///< If the user list is empty, return null
            }
        }
        catch (Exception exception)
        {
            returnValue = null; ///< If something goes wrong, return null. [Like database connections at max, lock due to something else etc.,]
        }
        finally
        {
            em.close();
            enf.close();
            return returnValue;
        }
    }

    /**
     * This method is used to get the user details to create a new element in the database. This will return the database element that is needed to do the necessary actions.
     * @param advancedProfile
     * @return 
     */
    private UserDetails createUserDetailsElement(AdvancedProfile advancedProfile) 
    {
        UserDetails userDetails = null;
        EntityManagerFactory enf = null;
        EntityManager em = null;
        try
        {
            enf = Persistence.createEntityManagerFactory("GurukadakshPU");
            em = enf.createEntityManager();
            em.getTransaction().begin();

            userDetails = new UserDetails(advancedProfile.getLinkedinId());                
//            userDetails.setHonors(advancedProfile.getHonors());
//            userDetails.setIndustry(advancedProfile.getIndustry());
//            userDetails.setIsoCountryCode(advancedProfile.getLocation());
//            userDetails.setMfeedRssURL(advancedProfile.getmRssFeed());
//            userDetails.setProposalComments(advancedProfile.getProposalComments());
//            userDetails.setSpecialities(advancedProfile.getSpecialities());
//            userDetails.setSummary(advancedProfile.getSummary());            
            em.persist(userDetails);
            em.getTransaction().commit();
        }
        catch ( Exception exception )
        {
              Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "createUserDetailsElement {0}", exception.getMessage());
              userDetails = null;
//            try
//            {
//
//            }
//            catch( Exception ex )
//            {
                em.getTransaction().rollback();
//            }
        }
        finally
        {
            em.close();
            enf.close();
            return userDetails;
        }
    }

    /**
     * This function is used to update the user details given that the user is already present
     * @param advancedProfile 
     */
    private void updateUserDetails(UserDetails userDetailsElement, AdvancedProfile advancedProfile) 
    {
        EntityManagerFactory enf = null;
        EntityManager em = null;
        try
        {
           
                enf = Persistence.createEntityManagerFactory("GurukadakshPU");
                em = enf.createEntityManager();
                em.getTransaction().begin();                
                userDetailsElement = em.find(UserDetails.class, userDetailsElement.getLinkedinId());
                try
                {               
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, em.contains(userDetailsElement) + "1: {0}",advancedProfile.toString());
                    
                    userDetailsElement.setDateOfBirth(advancedProfile.getDateOfBirth());
                    userDetailsElement.setHonors(advancedProfile.getHonors());
                    userDetailsElement.setIndustry(advancedProfile.getIndustry());
                    userDetailsElement.setIsoCountryCode(advancedProfile.getLocation());
                    userDetailsElement.setLastModified(advancedProfile.getLastModifiedDate());
                    userDetailsElement.setMfeedRssURL(advancedProfile.getmRssFeed());
                    userDetailsElement.setProposalComments(advancedProfile.getProposalComments());
                    userDetailsElement.setSpecialities(advancedProfile.getSpecialities());
                    userDetailsElement.setSummary(advancedProfile.getSummary());
                    userDetailsElement.setSpecialities(advancedProfile.getSpecialities());   
                    em.flush();
//                   em.getTransaction().commit();

//                    em.close();
//                    em = enf.createEntityManager();
//                    em.getTransaction().begin();
                    //Education stuff
                    for ( int i = 0 ; i < advancedProfile.educationList.size() ; i++ )
                    {
                        //em.getTransaction().begin();
                        AdvancedProfile.Education currentEducation = advancedProfile.educationList.get(i);
                        //Check if the education id exists and if not create an education with that id for the user.
                        UserEducationDetails educationDetails = em.find(UserEducationDetails.class, new UserEducationDetailsPK(advancedProfile.getLinkedinId(), currentEducation.getEducationId()));//null;
                        if ( educationDetails == null ) ///<  If the user list is not empty it implies that the skill is present in the database.                    
                        {
                            educationDetails = new UserEducationDetails(advancedProfile.getLinkedinId(), currentEducation.getEducationId());
                            em.persist(educationDetails);
                            em.flush();
                        }
                        //Update the various details
                        educationDetails.setActivities(currentEducation.getActivities());
                        educationDetails.setDegree(currentEducation.getDegree());
                        educationDetails.setEndYear(currentEducation.getEndYear());
                        educationDetails.setFieldOfStudy(currentEducation.getFieldOfStudy());
                        educationDetails.setNotes(currentEducation.getNotes());
                        educationDetails.setSchoolName(currentEducation.getSchoolName());
                        educationDetails.setStartYear(currentEducation.getStartYear());
                        em.flush();
                    }
                    //Position Stuff
                    for ( int i = 0 ; i < advancedProfile.positionList.size() ; i++ )
                    {
                        //em.getTransaction().begin();
                        AdvancedProfile.Position currentPosition = advancedProfile.positionList.get(i);
                        OrganizationsDatabase orgDB = null;
                        if ( currentPosition.getCompany_id() != 0 )
                        {
                            orgDB = em.find(OrganizationsDatabase.class, currentPosition.getCompany_id());
                            if ( orgDB == null )
                            {
                                orgDB = new OrganizationsDatabase(currentPosition.getCompany_id());
                                em.persist(orgDB);
                                em.flush(); 
                            }
                            orgDB.setOrganizationIndustry(currentPosition.getCompany_industry());
                            orgDB.setOrganizationName(currentPosition.getCompany_name());
                            orgDB.setOrganizationType(currentPosition.getCompany_type());
                            em.flush(); 
                            //SET ORGANIZATION DESCRIPTION
                            String orgDetails = linkedInInfo.extractCompanyInformation(accessToken, currentPosition.getCompany_id());
//                            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Organization Description - " + orgDetails);
                            if (orgDetails != null && !orgDetails.trim().isEmpty())
                            {
//                                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Organization Description - " + orgDetails);
                                JSONObject descriptionObject = new JSONObject(orgDetails);
                                String description = descriptionObject.optString(LinkedinURLs.LinkedInResponseParameters.COMPANY_DESCRIPTION);
                                orgDB.setOrganizationDescription(description);
                                em.flush();
                                //Add Specialities
                                JSONObject specialitiesObject = descriptionObject.optJSONObject(LinkedinURLs.LinkedInResponseParameters.SPECIALITIES);
//                                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Organization Specialities - " + specialitiesObject);
                                if( specialitiesObject != null )
                                {
                                    JSONArray specialitiesArray = specialitiesObject.optJSONArray(LinkedinURLs.LinkedInResponseParameters.RESULT_LIST);
//                                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Organization List - " + specialitiesArray);
                                
                                    if ( specialitiesArray != null )
                                    {
                                        for ( int k = 0 ; k < specialitiesArray.length() ; k++ )
                                        {
                                            String skillName = specialitiesArray.getString(k);
                                            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "SkillName String - " + skillName);
                                            SkillsDatabase skillDBElement = checkSkillExists(skillName);
                                            if ( skillDBElement == null )
                                            {
                                                skillDBElement = createSkillElement(skillName);
                                            }
                                            
                                            OrganizationSkills orgSkill = em.find(OrganizationSkills.class, new OrganizationSkillsPK(skillDBElement.getSkillId(),currentPosition.getCompany_id()));
                                            if ( orgSkill == null )
                                            {
                                                orgSkill = new OrganizationSkills();
                                                orgSkill.setId(new OrganizationSkillsPK(skillDBElement.getSkillId(),currentPosition.getCompany_id()));
                                                em.persist(orgSkill);
                                                em.flush();
                                            }
                                            
                                            
                                        }
                                    }
                                }
                            }
                            
                        }
                        //Check if the education id exists and if not create an education with that id for the user.
                        UserPositionsDetails positionDetails = em.find(UserPositionsDetails.class, new UserPositionsDetailsPK(advancedProfile.getLinkedinId(), currentPosition.getId()));//null;
                        if ( positionDetails == null ) ///<  If the user list is not empty it implies that the skill is present in the database.                    
                        {
                            positionDetails = new UserPositionsDetails(advancedProfile.getLinkedinId(), currentPosition.getId());
                            em.persist(positionDetails);
                            em.flush(); 
                        }
                        //Update the various details
                        positionDetails.setLinkedinCompanyId(orgDB);
                        positionDetails.setSummary(currentPosition.getSummary());
                        if ( !currentPosition.isIsCurrent() )
                        {
                            positionDetails.setEndDate( new java.sql.Date(currentPosition.getEndYear(), currentPosition.getEndMonth(), 1));
                        }
                        else
                        {
                            positionDetails.setEndDate(null);
                        }
                        positionDetails.setTitle(currentPosition.getTitle()); 
                        positionDetails.setStartDate(new java.sql.Date(currentPosition.getStartYear(), currentPosition.getStartMonth(), 1));
                        em.flush();                  
                    }
                      em.getTransaction().commit();
                    //em.persist(userDetailsElement);
                }
                catch(Exception e)
                {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "updateUserDetails {0}", e);
                    em.getTransaction().rollback();
                }
                //em.getTransaction().commit();
        }
        catch ( Exception exception )
        {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "updateUserDetails outer {0}",exception.getMessage());
            em.getTransaction().rollback();
        }
        finally
        {
            em.close();
            enf.close();
        }
    }
    /**
     * Calls the python implementation of PATE.
     * @param utable 
     */
    private void callPate(Usertable utable) 
    {
        try
        {
            String url = "http://localhost:7283";
            URL connectTo = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) connectTo.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(true);
            String parameterString = "id="+utable.getLinkedinId()+"&analysisType=profilePate";
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length", "" + Integer.toString(parameterString.getBytes().length));
            connection.setRequestMethod("POST");
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
            wr.writeBytes(parameterString);
            wr.flush();
            wr.close();
            DataInputStream inStream = new DataInputStream(connection.getInputStream());
            String buffer;
            String connectionOutput = "";
            while((buffer = inStream.readLine()) != null)
            {
                connectionOutput += buffer + "\n";
            }
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "callPate answer {0}",connectionOutput);

            connection.disconnect();
            
             url = "http://localhost:7283";
             connectTo = new URL(url);
             connection = (HttpURLConnection) connectTo.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(true);
             parameterString = "id="+utable.getLinkedinId()+"&analysisType=companyPate";
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length", "" + Integer.toString(parameterString.getBytes().length));
            connection.setRequestMethod("POST");
             wr = new DataOutputStream(connection.getOutputStream ());
            wr.writeBytes(parameterString);
            wr.flush();
            wr.close();
             inStream = new DataInputStream(connection.getInputStream());

             connectionOutput = "";
            while((buffer = inStream.readLine()) != null)
            {
                connectionOutput += buffer + "\n";
            }
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "callPate answer {0}",connectionOutput);

            connection.disconnect();
        }
        catch ( Exception exception )
        {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "callPate Exception {0}",exception.getMessage());
        }
    }

    /**
     * This is a class that contains the various parameters that are contained in the linkedin complete profile.
     */
    private class AdvancedProfile 
    {

        private Education createEducationObject() {
           Education returnObj = new Education();
           return returnObj;
           
        }
        private Position createPositionObject() {
            Position returnObj = new Position();
            return returnObj;
        }
        public class Position
        {
            private String summary, title, company_name, company_type, company_industry;
            private long id,company_id;
            private int startMonth, startYear, endMonth, endYear;
            private boolean isCurrent;

            public boolean isIsCurrent() {
                return isCurrent;
            }

            public void setIsCurrent(boolean isCurrent) {
                this.isCurrent = isCurrent;
            }
            public Position()
            {
                summary = title = company_name = company_type = company_industry = null;
                id = 1;
                company_id = 1;
                startMonth = startYear = endMonth = endYear = 0;
            }

            public long getCompany_id() {
                return company_id;
            }

            public void setCompany_id(long company_id) {
                this.company_id = company_id;
            }

            public String getCompany_industry() {
                return company_industry;
            }

            public void setCompany_industry(String company_industry) {
                this.company_industry = company_industry;
            }

            public String getCompany_name() {
                return company_name;
            }

            public void setCompany_name(String company_name) {
                this.company_name = company_name;
            }

            public String getCompany_type() {
                return company_type;
            }

            public void setCompany_type(String company_type) {
                this.company_type = company_type;
            }

            public int getEndMonth() {
                return endMonth;
            }

            public void setEndMonth(int endMonth) {
                this.endMonth = endMonth;
            }

            public int getEndYear() {
                return endYear;
            }

            public void setEndYear(int endYear) {
                this.endYear = endYear;
            }

            public long getId() {
                return id;
            }

            public void setId(long id) {
                this.id = id;
            }

            public int getStartMonth() {
                return startMonth;
            }

            public void setStartMonth(int startMonth) {
                this.startMonth = startMonth;
            }

            public int getStartYear() {
                return startYear;
            }

            public void setStartYear(int startYear) {
                this.startYear = startYear;
            }

            public String getSummary() {
                return summary;
            }

            public void setSummary(String summary) {
                this.summary = summary;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            } 
        }
        public class Education
        {
            String startYear, endYear, fieldOfStudy, schoolName, degree, activities, notes;
            long educationId;
            public Education() {
                 startYear = endYear = fieldOfStudy = schoolName = degree = activities = notes = null;
                 educationId = 1;
            }

            public String getActivities() {
                return activities;
            }

            public void setActivities(String activities) {
                this.activities = activities;
            }

            public String getDegree() {
                return degree;
            }

            public void setDegree(String degree) {
                this.degree = degree;
            }

            public long getEducationId() {
                return educationId;
            }

            public void setEducationId(long educationId) {
                this.educationId = educationId;
            }

            public String getEndYear() {
                return endYear;
            }

            public void setEndYear(String endYear) {
                this.endYear = endYear;
            }

            public String getFieldOfStudy() {
                return fieldOfStudy;
            }

            public void setFieldOfStudy(String fieldOfStudy) {
                this.fieldOfStudy = fieldOfStudy;
            }

            public String getNotes() {
                return notes;
            }

            public void setNotes(String notes) {
                this.notes = notes;
            }

            public String getSchoolName() {
                return schoolName;
            }

            public void setSchoolName(String schoolName) {
                this.schoolName = schoolName;
            }

            public String getStartYear() {
                return startYear;
            }

            public void setStartYear(String startYear) {
                this.startYear = startYear;
            }
            
        }
        private String summary, specialities, honors, industry, location, lastModifiedTime, mRssFeed, proposalComments, linkedinId;
        private ArrayList<Education> educationList = new ArrayList<Education>();
        private ArrayList<Position> positionList = new ArrayList<Position>();
        
        public AdvancedProfile() {
            summary = specialities = honors = industry = location = lastModifiedTime = mRssFeed = proposalComments = linkedinId = null;
        }

        @Override
        public String toString() {
            return "AdvancedProfile{" + "summary=" + summary + ", specialities=" + specialities + ", honors=" + honors + ", industry=" + industry + ", location=" + location + ", lastModifiedTime=" + lastModifiedTime + ", mRssFeed=" + mRssFeed + ", proposalComments=" + proposalComments + ", linkedinId=" + linkedinId + ", educationList=" + educationList + ", positionList=" + positionList + '}';
        }

        public String getLinkedinId() {
            return linkedinId;
        }

        public void setLinkedinId(String linkedinId) {
            this.linkedinId = linkedinId;
        }

        public String getHonors() {
            return honors;
        }

        public void setHonors(String honors) {
            this.honors = honors;
        }

        public String getIndustry() {
            return industry;
        }

        public void setIndustry(String industry) {
            this.industry = industry;
        }

        public String getLastModifiedTime() {
            return lastModifiedTime;
        }

        public void setLastModifiedTime(String lastModifiedTime) {
            this.lastModifiedTime = lastModifiedTime;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getmRssFeed() {
            return mRssFeed;
        }

        public void setmRssFeed(String mRssFeed) {
            this.mRssFeed = mRssFeed;
        }

        public String getProposalComments() {
            return proposalComments;
        }

        public void setProposalComments(String proposalComments) {
            this.proposalComments = proposalComments;
        }

        public String getSpecialities() {
            return specialities;
        }

        public void setSpecialities(String specialities) {
            this.specialities = specialities;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        private Date getDateOfBirth() {
            return null;
        }

        private Date getLastModifiedDate() {
            return null;
        }
        
    }
}










