/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh;

import org.scribe.model.Token;
import org.apache.commons.lang3.RandomStringUtils;
import com.gurukadaksh.DBModel.TokenTable;
import com.gurukadaksh.DBModel.Usertable;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
/**
 * Manages the Gurukadaksh token
 * @author Deepak Subramanian
 */
public class TokenManager
{
    private String gkToken; ///< The gurukadaksh token that will be released to the client
    private Token accessToken; ///< The access token that will be stored in the database and retrieved as we see fit
    private String userId; ///< The user id to which the token has been issued
    private Usertable userTable;

    public Usertable getUserTable()
    {
        return userTable;
    }

    public Token getAccessToken()
    {
        return accessToken;
    }

    public String getGkToken()
    {
        return gkToken;
    }

    public String getUserId()
    {
        return userId;
    }

    /**
     * To get linkedin info from valid token
     * @param gkToken
     */
    public TokenManager(String gkToken)
    {
        EntityManagerFactory enf = null;
        EntityManager em = null;
        if(gkToken != null)
        {
            this.gkToken = gkToken;
            try
            {
                enf = Persistence.createEntityManagerFactory("GurukadakshPU");
                em = enf.createEntityManager();
                em.getTransaction().begin();

                TypedQuery<TokenTable> existingToken = em.createNamedQuery("TokenTable.findByGkToken", TokenTable.class);

                try
                {
                    List<TokenTable> tokenList = existingToken.setParameter("gkToken", this.gkToken).getResultList();
                    if ( !tokenList.isEmpty() )
                    {
                        TokenTable tokenItem = tokenList.get(0);
                        accessToken = new Token(tokenItem.getLinkedinToken(), tokenItem.getLinkedinSecret());
                        userId = tokenItem.getLinkedinId().getLinkedinId();
                        userTable = tokenItem.getLinkedinId();
                    }
                    else
                        accessToken = Token.empty();
                }
                catch ( Exception exception )
                {
                    accessToken = Token.empty();
                }
            }
            catch (Exception exception)
            {

            }
            finally
            {
                em.close();
                enf.close();
            }
        }
    }
    /**
     * To get gkToken from linkedIn information
     * @param accessToken
     * @param userId
     */
    public TokenManager(Token accessToken)
    {
        EntityManagerFactory enf = null;
        EntityManager em = null;
        try
        {                      
            enf = Persistence.createEntityManagerFactory("GurukadakshPU");
            em = enf.createEntityManager();
            em.getTransaction().begin();

            TokenTable tokenTable = new TokenTable();
            TypedQuery<TokenTable> existingToken = em.createNamedQuery("TokenTable.findByGkToken", TokenTable.class);
            BackendOperator backendOperator = new BackendOperator(accessToken);

            Usertable usertable = backendOperator.createOrUpdateUserTable();
            while (true)
            {
                this.gkToken = RandomStringUtils.randomAlphanumeric(128);
                try
                {
                    List<TokenTable> tokenItem = existingToken.setParameter("gkToken", this.gkToken).getResultList();
                    if ( !tokenItem.isEmpty() )
                        continue;
                    else
                        break;
                }
                catch ( Exception exception )
                {
                    break;
                }
            }
            this.accessToken = accessToken;
            this.userId = usertable.getLinkedinId();
            tokenTable.setGkToken(this.gkToken);
            tokenTable.setLinkedinSecret(this.accessToken.getSecret());
            tokenTable.setLinkedinToken(this.accessToken.getToken());
            tokenTable.setTimestamp(System.currentTimeMillis());
            tokenTable.setLinkedinId(usertable);
            em.persist(tokenTable);
            em.getTransaction().commit();
        }
        catch (Exception exception)
        {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Exception is{0}", exception.getLocalizedMessage());
            em.getTransaction().rollback();
        }
        finally
        {
            if (em != null)
            em.close();
            if (enf != null)
            enf.close();
        }       
        
    }

}
