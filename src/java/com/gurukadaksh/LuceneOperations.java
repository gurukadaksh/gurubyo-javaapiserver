/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh;

import com.gurukadaksh.DBModel.SkillsDatabase;
import com.gurukadaksh.linkedInInfo;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.FilteredQuery;
import org.apache.lucene.index.memory.MemoryIndex;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PositiveScoresOnlyCollector;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopDocsCollector;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

/**
 *
 * @author Deepak Subramanian
 */
public class LuceneOperations
{
    final File INDEX_DIR = new File("index");
    
    public void addCompleteSkillIndex()
    {
        try
        {
           if(linkedInInfo.debugMode) Logger.getLogger(this.getClass().getName()).log(Level.FINEST, "Add Complete Skill Index");

           StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);
           IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36, analyzer);
           Directory d = FSDirectory.open(INDEX_DIR);
           
           IndexWriter writer = new IndexWriter(d,config);//new IndexWriter(INDEX_DIR, analyzer, true);
           indexAllDocs(writer);
           if(linkedInInfo.debugMode) Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,"Documents have been indexed");
           //writer.optimize();
           writer.commit();
           writer.close();
           
           d.close();
        }
        catch (Exception e)
        {
           e.printStackTrace();
        }
    }
    /**
     * Add specific skills/values to the lucene indess.
     * @param luceneMapList - this is a arraylist of all skills in the LuceneMap format
     */
    public void addSpecificValueToIndex(ArrayList<LuceneMap> luceneMapList)
    {
        try
        {
            
            StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);
            IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36, analyzer);
            Directory d = FSDirectory.open(INDEX_DIR);

            IndexWriter writer = new IndexWriter(d,config);//new IndexWriter(INDEX_DIR, analyzer, true);
            for ( int i = 0 ; i < luceneMapList.size() ; i++ )
            {
                LuceneMap luceneMap = luceneMapList.get(i);
                Document doc = new Document();
                doc.add(new Field("id", luceneMap.id, Field.Store.YES, Field.Index.NO));
                doc.add(new Field("name", luceneMap.name , Field.Store.YES, Field.Index.ANALYZED));
                doc.add(new Field("type", luceneMap.type,Field.Store.YES, Field.Index.NO));
                if(linkedInInfo.debugMode) Logger.getLogger(this.getClass().getName()).log(Level.INFO, "\nid\t{0}\nname\t"+doc.get("name"), doc.get("id"));
                writer.addDocument(doc);
            }
            if(linkedInInfo.debugMode) Logger.getLogger(this.getClass().getName()).log(Level.FINEST,"Current Document has been indexed");
            //writer.optimize();
            writer.commit();
            writer.close();
           
            d.close();
        }
        catch (Exception exception)
        {
            
        }
        finally
        {
            
        }
    }
    /**
     * This function gets the various elements from the database and indexes the whole skills database all over.
     * @param writer
     * @throws Exception
     */
    void indexAllDocs(IndexWriter writer) throws Exception
    {
        EntityManagerFactory enf = null;
        EntityManager em = null;

        try
        {
            writer.deleteAll();
            enf = Persistence.createEntityManagerFactory("GurukadakshPU");
            em = enf.createEntityManager();
            em.getTransaction().begin();

            //
            TypedQuery<SkillsDatabase> allSkillsQuery = em.createNamedQuery("SkillsDatabase.findAll", SkillsDatabase.class);
            List<SkillsDatabase> skillsDatabaseList = allSkillsQuery.getResultList();
            for ( int i = 0 ; i < skillsDatabaseList.size() ; i++ )
            {
                 Document d = new Document();
                 d.add(new Field("id", skillsDatabaseList.get(i).getSkillId().toString(), Field.Store.YES, Field.Index.NO));
                 d.add(new Field("name", skillsDatabaseList.get(i).getSkillName() , Field.Store.YES, Field.Index.ANALYZED));
                 d.add(new Field("type","Skill",Field.Store.YES,Field.Index.NO));
                 if(linkedInInfo.debugMode) Logger.getLogger(this.getClass().getName()).log(Level.INFO, "\nid\t{0}\nname\t"+d.get("name"), d.get("id"));
                 writer.addDocument(d);
                 
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        finally
        {
            em.close();
            enf.close();
        }     
    }


/**
 * This function is used to search the entire luceneIndex for the given term
 * @param givenTerm
 * @return 
 */

    public ArrayList<LuceneMap> searchTerm(String givenTerm)
    {
        ArrayList<LuceneMap> returnStruct = null;
        try
        {
            Directory d = FSDirectory.open(INDEX_DIR);
            IndexSearcher indexSearcher = new IndexSearcher(IndexReader.open(d));//.open("index"));
            StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);
            String lowercaseTerm = givenTerm.toLowerCase();
            Term termWildCard1 = new Term("name",lowercaseTerm+"*");    
            Term termWildCard2 = new Term("name","*"+lowercaseTerm+"*"); 
            Term termFuzzy = new Term("name",lowercaseTerm);
            Query queryWildCard = new WildcardQuery(termWildCard1);//new QueryParser(Version.LUCENE_36,"color",analyzer).parse("white");
            Query queryWildCard_more = new WildcardQuery(termWildCard2);
            Query queryFuzzy = new FuzzyQuery(termFuzzy, (float) 0.3);
            TopDocs tdWildCard = indexSearcher.search(queryWildCard, 10, Sort.RELEVANCE);
            TopDocs tdWildCard_more = indexSearcher.search(queryWildCard_more, 10, Sort.RELEVANCE);
            TopDocs tdFuzzy = indexSearcher.search(queryFuzzy, 10, Sort.RELEVANCE);
            //
            ScoreDoc[] scoreDosArray = tdWildCard.scoreDocs;
            returnStruct = new ArrayList<LuceneMap>();
            for(ScoreDoc scoredoc: scoreDosArray)
            {
                //Retrieve the matched document and show relevant details
                Document doc = indexSearcher.doc(scoredoc.doc);
//                System.out.println("\nSender: "+doc.getField("sender").stringValue());
//                System.out.println("Subject: "+doc.getField("subject").stringValue());
//                System.out.println("Email file location: "
//                                                +doc.getField("emailDoc").stringValue());
                LuceneMap lsm = new LuceneMap();
                lsm.setId(doc.get("id"));
                lsm.setName(doc.get("name"));
                lsm.setType(doc.get("type"));
                if(linkedInInfo.debugMode) {
                    Logger.getLogger(this.getClass().getName()).log(Level.INFO,"name is {0}",lsm.name);
                }
                if(!returnStruct.contains(lsm))
                {
                    returnStruct.add(lsm);
                }

            }
            scoreDosArray = tdFuzzy.scoreDocs;
            for(ScoreDoc scoredoc: scoreDosArray)
            {

                //Retrieve the matched document and show relevant details
                Document doc = indexSearcher.doc(scoredoc.doc);
//                System.out.println("\nSender: "+doc.getField("sender").stringValue());
//                System.out.println("Subject: "+doc.getField("subject").stringValue());
//                System.out.println("Email file location: "
//                                                +doc.getField("emailDoc").stringValue());
                LuceneMap lsm = new LuceneMap();
                lsm.setId(doc.get("id"));
                lsm.setName(doc.get("name"));
                lsm.setType(doc.get("type"));
                if(!returnStruct.contains(lsm))
                {
                    returnStruct.add(lsm);
                }
            }
            scoreDosArray = tdWildCard_more.scoreDocs;
            for(ScoreDoc scoredoc: scoreDosArray)
            {

                //Retrieve the matched document and show relevant details
                Document doc = indexSearcher.doc(scoredoc.doc);
//                System.out.println("\nSender: "+doc.getField("sender").stringValue());
//                System.out.println("Subject: "+doc.getField("subject").stringValue());
//                System.out.println("Email file location: "
//                                                +doc.getField("emailDoc").stringValue());
                LuceneMap lsm = new LuceneMap();
                lsm.setId(doc.get("id"));
                lsm.setName(doc.get("name"));
                lsm.setType(doc.get("type"));
                if(!returnStruct.contains(lsm))
                {
                    returnStruct.add(lsm);
                }
            }
        }
        catch (Exception exception)
        {
           
            return  null;
        }
        finally
        {

            return returnStruct;
        }

    }

    /**
     * This function is used to get the various results for a given full text.[Generally in the description and in the title]
     * @param searchParam
     * @return 
     */
    public ArrayList<LuceneMap> fullTextSearch(String givenText) 
    {
        ArrayList<LuceneMap> returnStruct = null;
        EntityManagerFactory enf = null;
        EntityManager em = null;

        try
        {

            enf = Persistence.createEntityManagerFactory("GurukadakshPU");
            em = enf.createEntityManager();
            em.getTransaction().begin();
            String lowerGivenText = givenText.trim().toLowerCase()+".";
            //
            TypedQuery<SkillsDatabase> allSkillsQuery = em.createNamedQuery("SkillsDatabase.findAll", SkillsDatabase.class);
            List<SkillsDatabase> skillsDatabaseList = allSkillsQuery.getResultList();
            returnStruct = new ArrayList<>();
            for ( SkillsDatabase thisSkill : skillsDatabaseList )
            {
                if ( lowerGivenText.contains(thisSkill.getSkillName().toLowerCase()) )
                {
                    String lowerFind = thisSkill.getSkillName().toLowerCase();
                    if ( lowerGivenText.contains(lowerFind+".") || lowerGivenText.contains(lowerFind+" ") || lowerGivenText.contains(lowerFind+",") || lowerGivenText.contains(lowerFind+"\n")  )
                    
                    {
                        LuceneMap lsm = new LuceneMap();
                        lsm.setId(thisSkill.getSkillId().toString());
                        lsm.setName(thisSkill.getSkillName());
                        lsm.setType("Skill");
                        if(!returnStruct.contains(lsm))
                        {
                            returnStruct.add(lsm);
                        }
                    }
                }
            }
        }
    
            //<editor-fold defaultstate="collapsed" desc="Old Code">
            //            Directory d = FSDirectory.open(INDEX_DIR);
            //
            //            IndexSearcher indexSearcher = new IndexSearcher(IndexReader.open(d));//.open("index"));
            //            StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);
            //            String lowercaseText = givenText.toLowerCase();
            //            Term termWildCard = new Term("name","*"+lowercaseText+"*");
            //
            //            Query queryWildCard = new WildcardQuery(termWildCard);
            //            //TopDocs tdWildCard = indexSearcher.search(queryWildCard, 10, Sort.RELEVANCE);
            //            FullTextCollector tdWildCard;
            //            final ArrayList<Integer> matchedDocs = new ArrayList<Integer>();
            //            tdWildCard = new FullTextCollector();
            //            indexSearcher.search(queryWildCard, tdWildCard);
            //
            //            IndexReader.open(d);
            //            returnStruct = new ArrayList<LuceneMap>();
            //
            //            MemoryIndex index = new MemoryIndex();
            //            index.addField("content", givenText, analyzer);
            //            //index.addField("author", "Tales of James", analyzer);
            //            //QueryParser parser = new QueryParser("content", analyzer);
            //            float score = index.search(queryWildCard);
            //            if (score > 0.0f) {
            //                System.out.println("it's a match");
            //            } else {
            //                System.out.println("no match found");
            //            }
            //
            //            for(Integer docId: tdWildCard.collectedDocs)
            //            {
            //                //Retrieve the matched document and show relevant details
            //                Document doc = indexSearcher.doc(docId);
            ////                System.out.println("\nSender: "+doc.getField("sender").stringValue());
            ////                System.out.println("Subject: "+doc.getField("subject").stringValue());
            ////                System.out.println("Email file location: "
            ////                                                +doc.getField("emailDoc").stringValue());
            //                LuceneMap lsm = new LuceneMap();
            //                lsm.setId(doc.get("id"));
            //                lsm.setName(doc.get("name"));
            //                lsm.setType(doc.get("type"));
            //                if(linkedInInfo.debugMode) {
            //                    Logger.getLogger(this.getClass().getName()).log(Level.INFO,"name is {0}",lsm.name);
            //                }
            //                if(!returnStruct.contains(lsm))
            //                {
            //                    returnStruct.add(lsm);
            //                }
            //
            //            }
            //</editor-fold>
            
        
        catch (Exception exception)
        {         
            returnStruct =  null;
        }
        finally
        {
            em.close();
            enf.close();
            return returnStruct;
        }
    }

}


class FullTextCollector extends Collector
{
    public ArrayList<Integer> collectedDocs;
    
    public FullTextCollector() 
    {
        super();
        collectedDocs = new ArrayList<>();            
    }
    
    @Override
    public void setScorer(Scorer scorer) throws IOException {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void collect(int i) throws IOException {
        //throw new UnsupportedOperationException("Not supported yet.");
        collectedDocs.add(i);
    }

    @Override
    public void setNextReader(IndexReader reader, int i) throws IOException {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean acceptsDocsOutOfOrder() {
        //throw new UnsupportedOperationException("Not supported yet.");
        return true;
    }
    
}

















