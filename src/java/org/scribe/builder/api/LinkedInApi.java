package org.scribe.builder.api;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.scribe.model.*;

public class LinkedInApi extends DefaultApi10a
{
  private static final String AUTHORIZE_URL = "https://api.linkedin.com/uas/oauth/authorize?oauth_token=%s";

  @Override
  public String getAccessTokenEndpoint()
  {
    return "https://api.linkedin.com/uas/oauth/accessToken";
  }

  @Override
  public String getRequestTokenEndpoint() //This has been changed by deepak
  {
        String linkedinAPI = "https://api.linkedin.com/uas/oauth/requestToken?scope=r_fullprofile+r_emailaddress+r_network+r_basicprofile+rw_nus+rw_groups+r_contactinfo+w_messages";
        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, linkedinAPI);
        return  linkedinAPI;
 }
  
    @Override
  public String getAuthorizationUrl(Token requestToken)
  {
    return String.format(AUTHORIZE_URL, requestToken.getToken());
  }
  
}
