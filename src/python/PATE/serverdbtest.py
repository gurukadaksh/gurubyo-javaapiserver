import MySQLdb
import json
import time
import AlchemyAPI
import urllib
import urllib2
import xml.etree.ElementTree as ET
import sys
import BaseHTTPServer
from BaseHTTPServer import BaseHTTPRequestHandler
import cgi
import traceback
from types import NoneType
from re import sub
from sys import stderr
from HTMLParser import HTMLParser
from traceback import print_exc
from MySQLdb.constants.FIELD_TYPE import NULL
import keyword
import glob
import logging
import logging.handlers


LOG_FILENAME = 'PATELogger.log'

# Set up a specific logger with our desired output level
my_logger = logging.getLogger('MyLogger')
my_logger.setLevel(logging.DEBUG)

# Add the log message handler to the logger
handler = logging.handlers.RotatingFileHandler(
              LOG_FILENAME, maxBytes=26214400, backupCount=20)
my_logger.addHandler(handler)

# Log some messages
#for i in range(20):
#    my_logger.debug('i = %d' % i)

# See what files are created
logfiles = glob.glob('%s*' % LOG_FILENAME)

#sys.stdout = open('redirOp.txt', 'w')
class _DeHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.__text = []

    def handle_data(self, data):
        text = data.strip()
        if len(text) > 0:
            text = sub('[ \t\r\n]+', ' ', text)
            self.__text.append(text + ' ')

    def handle_starttag(self, tag, attrs):
        if tag == 'p':
            self.__text.append('\n\n')
        elif tag == 'br':
            self.__text.append('\n')

    def handle_startendtag(self, tag, attrs):
        if tag == 'br':
            self.__text.append('\n\n')

    def text(self):
        return ''.join(self.__text).strip()

def dehtml(text):
    try:
        parser = _DeHTMLParser()
        parser.feed(text)
        parser.close()
        return parser.text()
    except:
        print_exc(file=stderr)
        return text


HOST_NAME = 'localhost' 
PORT_NUMBER = 7283 


class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    db = MySQLdb.connect(host="localhost", # your host, usually localhost
             user="root", # your username - pate
              passwd="singapore", # your password - gurukadakshatgoogle
              db="gurukadakshDB") # name of the data base

    # you must create a Cursor object. It will let
    #  you execute all the query you need
    cur = db.cursor()

    @staticmethod
    def isDisambiguationPage(wikiArticle):
        disambiguationPage = False
        wikiPrintURL = 'http://en.wikipedia.org/w/index.php?title='+wikiArticle.strip()+'&printable=yes'
        #=======================================================================
        # print wikiPrintURL
        #=======================================================================
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        infile = opener.open(wikiPrintURL)
        page = infile.read()
        searchText = dehtml(page)
        if 'disambiguation pages' in searchText:
            disambiguationPage = True
        return disambiguationPage

    @staticmethod
    def getWikiURL(searchfor):
        data = {}
        while True:
            query = urllib.urlencode({'q': searchfor})
            url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' % query
            search_response = urllib.urlopen(url)
            search_results = search_response.read()
            results = json.loads(search_results)
            data = results['responseData']
            if data == None and results['responseStatus']==403:
                time.sleep(20)
                continue
            break
            
        hits = data['results']
        #=======================================================================
        # print 'Total results: %s' % data['cursor']['estimatedResultCount']
        # print 'Top %d hits:' % len(hits)
        # for h in hits: print ' ', h['url']
        # print 'For more results, see %s' % data['cursor']['moreResultsUrl']
        #=======================================================================
        return hits[0]['url']
    
    @staticmethod
    def freqWords(linkedinCompanyId, wikiArticle, companyDesc):
        returnJSON = []
        wikiPrintURL = 'http://en.wikipedia.org/w/index.php?title='+wikiArticle.strip()+'&printable=yes'
        ##Create an AlchemyAPI object.
        alchemyObj = AlchemyAPI.AlchemyAPI()
        ##Load the API key from disk.
        alchemyObj.loadAPIKey("api_key.txt");

        keyword_list = []
        
        disambiguationPage = MyHandler.isDisambiguationPage(wikiArticle)
        if not disambiguationPage:
            ##Extract topic keywords from a web URL.
            result = alchemyObj.URLGetRankedKeywords(wikiPrintURL)
            tree = ET.fromstring(result)
            for textTag in tree.findall('keywords'):
                for text in textTag.findall('keyword'):
                    keyword_list.append(text.find('text').text)
        else:
            my_logger.info('Disambiguation Page in Company Pate - Keyword')

        result = alchemyObj.TextGetRankedKeywords(companyDesc);
        tree = ET.fromstring(result)
        for textTag in tree.findall('keywords'):
            for text in textTag.findall('keyword'):
                keyword_list.append(text.find('text').text)

        concepts_list = []
        result = alchemyObj.TextGetRankedConcepts(companyDesc);
        tree = ET.fromstring(result)
        for textTag in tree.findall('concepts'):
            for text in textTag.findall('concept'):
                concepts_list.append(text.find('text').text)
                
        if not disambiguationPage:
            ##Extract topic concepts from a web URL.
            result = alchemyObj.URLGetRankedConcepts(wikiPrintURL)
            tree = ET.fromstring(result)
            for textTag in tree.findall('concepts'):
                for text in textTag.findall('concept'):
                    concepts_list.append(text.find('text').text)

            result = alchemyObj.URLGetRankedNamedEntities(wikiPrintURL)
            tree = ET.fromstring(result)
            for textTag in tree.findall('entities'):
                for text in textTag.findall('entity'):
                    if text.find('type').text == 'FieldTerminology':
                        concepts_list.append(text.find('text').text)
        else:
            my_logger.info('Disambiguation Page in Company Pate - Concepts')
        
        for concept in concepts_list:
            MyHandler.insertCompanyTag(concept, linkedinCompanyId, returnJSON)
        #=======================================================================
        # print 'Concepts List'
        # print '='*30
        # print concepts_list
        # print '='*30
        # print 'Keyword List'
        # print keyword_list
        # print '='*30
        #=======================================================================
            
        freq_dic = {}
        ignore_list = ['at', 'the', 'for', 'is', 'in', 'an', 'am', 'a', 'from', 'during', 'within', 'since', 'over', 'and', 'of', 'to', 'retrieved', 'with', 'as']
        
        # Splitting the keyword into subsequent words (Art School as Art, School)
        for keywd in keyword_list:
            for keyword in keywd.split():
                if keyword not in ignore_list:
                    try:
                        freq_dic[keyword] += 1
                    except:
                        freq_dic[keyword] = 1

        # create list of (key, val) tuple pairs
        freq_list = freq_dic.items()
        # sort by key or word
        freq_list.sort()

        # create list of (val, key) tuple pairs
        freq_list2 = [(val, key) for key, val in freq_dic.items()]
        # sort by val or frequency
        freq_list2.sort(reverse=True)
        
        wtList = []
        for frq in freq_list2:
            wtList.append(frq[0])
        maxWt = max(wtList)
        
        for concept in concepts_list:
            freq_dic[concept] = maxWt
        
        # display result
#        print '-'*30
#        print 'High frequency words for '  + wikiArticle
#        print 'Concept list for '  + wikiArticle
#        print concepts_list
#        print len(concepts_list)
#        print 'Keyword list for '  + wikiArticle
#        print keyword_list
#        print len(keyword_list)
#        print '-'*30
        
        for freq, word in freq_list2[:15]:
            for keywd in keyword_list:
                if word in keywd:
                    # Checking the skill and inserting into db
                    MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                    # print all the first cell of all the rows
                    for row in MyHandler.cur.fetchall() :
                        if row[0] == None:
                            keywordId = 1
                        else:
                            keywordId = long(row[0]) + 1

                        skillIdTemp = MyHandler.checkExistsTag(keywd)
                        skillNameTemp = ''
                        if skillIdTemp == 0 :
                            try:
                                skillIdTemp = keywordId
                                skillNameTemp = keywd
                                returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s, %s)"""
                                MyHandler.cur.execute(qryString, (keywordId, keywd, True))
                                skillIdTemp = keywordId
                                MyHandler.db.commit()
                            except Exception, err:
                                  my_logger.critical(traceback.format_exc())
                                  MyHandler.db.rollback()
    
                        if not MyHandler.checkExistsCompanyTagAssociation(linkedinCompanyId, skillIdTemp):
                            try:
                                qryString = 'INSERT INTO organizationPateAssociations(linkedinCompanyId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                if not skillIdTemp == None:
                                    MyHandler.cur.execute(qryString, (linkedinCompanyId, skillIdTemp, True, 1));
                                    MyHandler.db.commit()
                            except Exception, err:
                              my_logger.critical(traceback.format_exc())
                              MyHandler.db.rollback()
                        else:
                            my_logger.info('Not inserting now %s', skillNameTemp)
        return json.dumps(returnJSON)
    
    @staticmethod
    def insertCompanyTag(keywd, linkedinCompanyId, returnJSON):
        
        MyHandler.cur.execute("select max(skillId) from skillsDatabase")
        # print all the first cell of all the rows
        for row in MyHandler.cur.fetchall() :
            if row[0] == None:
                keywordId = 1
            else:
                keywordId = long(row[0]) + 1
                
        skillIdTemp = MyHandler.checkExistsTag(keywd)
        skillNameTemp = ''
        if skillIdTemp == 0 :
            try:
                skillIdTemp = keywordId
                skillNameTemp = keywd
                returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s, %s)"""
                MyHandler.cur.execute(qryString, (skillIdTemp, skillNameTemp, True))
                skillIdTemp = keywordId
                MyHandler.db.commit()
            except Exception, err:
                  my_logger.critical(traceback.format_exc())
                  MyHandler.db.rollback()

        if not MyHandler.checkExistsCompanyTagAssociation(linkedinCompanyId, skillIdTemp):
            try:
                qryString = 'INSERT INTO organizationPateAssociations(linkedinCompanyId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                if not skillIdTemp == None:
                    MyHandler.cur.execute(qryString, (linkedinCompanyId, skillIdTemp, True, 1));
                    MyHandler.db.commit()
            except Exception, err:
              my_logger.critical(traceback.format_exc())
              MyHandler.db.rollback()
        else:
            my_logger.info('Not inserting now')
    
    @staticmethod    
    def checkExistsTag (tagname):
        
        try:
            MyHandler.cur.execute("""select skillId from skillsDatabase where skillName = %s """ , (tagname) )
            curFetch = MyHandler.cur.fetchall()
            if len(curFetch) == 0:
                return 0
            for row in curFetch:
                if row[0] == None:
                    return 0
                else:
                    my_logger.info('in CheckExistsTag')
                    return row[0]
        except Exception, err:
            my_logger.critical(traceback.format_exc())

    @staticmethod
    def checkExistsCompanyTagAssociation (linkedinCompanyId , pateKeyId):
        
        try:
            MyHandler.cur.execute("select pateKeyId from organizationPateAssociations where linkedinCompanyId = %s AND pateKeyId = %s" , (linkedinCompanyId, pateKeyId))
            curFetch = MyHandler.cur.fetchall()
            if len(curFetch) == 0:
                return False
            for row in curFetch:
                if row[0] == None:
                    my_logger.info('None found in CheckExistsCompanyTag')
                    return False
                return True
        except Exception, err:
            my_logger.critical(traceback.format_exc())
        
    @staticmethod
    def checkExistsUserTagAssociation (personId , pateKeyId):
        
        try:
            MyHandler.cur.execute("select pateKeyId from userPateAssociations where linkedinId = %s AND pateKeyId = %s" , (personId, pateKeyId))
            curFetch = MyHandler.cur.fetchall()
            if len(curFetch) == 0:
                return False
            for row in curFetch:
                if row[0] == None:
                    return False
                return True
        except Exception, err:
            my_logger.critical(traceback.format_exc())

    @staticmethod
    def checkExistsProjectTagAssociation (projectId , pateKeyId):
        
        try:
            MyHandler.cur.execute("select pateKeyId from projectPateAssociations where projectId = %s AND pateKeyId = %s" , (projectId, pateKeyId))
            curFetch = MyHandler.cur.fetchall()
            if len(curFetch) == 0:
                return False
            for row in curFetch: 
                if row[0] == None:
                    return False
                return True
        except Exception, err:
            my_logger.critical(traceback.format_exc())

    @staticmethod
    def performCompanyPate(linkedinId):
        
        returnJSON = []
        my_logger.info("Company Pate entered")
        if not linkedinId == None:
            MyHandler.cur.execute("select linkedinCompanyId from userPositionsDetails where linkedinId = %s ", (linkedinId))
            linkedinCompanyIdList = []
            curFetch = MyHandler.cur.fetchall()
            if len(curFetch) == 0:
                return False
            # append to company list
            for row in curFetch:
                linkedinCompanyIdList.append(row[0])

            for linkedinCompanyId in linkedinCompanyIdList:
                if not linkedinCompanyId == None:
                    MyHandler.cur.execute("select organizationName, organizationDescription from organizationsDatabase where linkedinCompanyId= %s", (linkedinCompanyId))
    
                    for row in MyHandler.cur.fetchall() :
                        companyName = row[0]
                        companyDesc = row[1]
    
                        if len(companyName) > 0:
                            wikiURL = MyHandler.getWikiURL('site:wikipedia.org ' + companyName)
                            wikiArticle = wikiURL[wikiURL.find('wiki/')+5:]
                            returnJSON = MyHandler.freqWords(linkedinCompanyId, wikiArticle, companyDesc)
                            my_logger.info('Company Profiling Complete for ' + companyName)
                        else:
                            my_logger.info('Company Desc ->' + str(len(companyName)))
        else:
            my_logger.info('LinkedIn ID - NULL')
        my_logger.info('Company Profiling Complete - All Done')
        
        my_logger.info('Performing Profile Experience Pate')
        if not linkedinId == None:
            MyHandler.cur.execute("select linkedinCompanyId, summary from userPositionsDetails where linkedinId = %s ", (linkedinId))
            linkedinCompanyIdList = []
            linkedinCompanySumList = []
            
            curFetch = MyHandler.cur.fetchall()
            if len(curFetch) == 0:
                return False
            # append to company list
            for row in curFetch:
                linkedinCompanyIdList.append(row[0])
                linkedinCompanySumList.append(row[1])
                
            for i in range(len(linkedinCompanyIdList)):
                linkedinCompanySummary = linkedinCompanySumList[i]
                returnJSON=[]
                alchemyObj = AlchemyAPI.AlchemyAPI()
                alchemyObj.loadAPIKey("api_key_2.txt");
                result = alchemyObj.TextGetRankedConcepts(linkedinCompanySummary);

                skillIdTemp = 0L
                skillNameTemp = ""
                    
                tree = ET.fromstring(result)
                for textTag in tree.findall('concepts'):
                    for text in textTag.findall('concept'):
                        MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                        curFetch = MyHandler.cur.fetchall()
                        if len(curFetch) == 0:
                            keywordId = 1
                        for row in curFetch:
                            if row[0] == None:
                                keywordId = 1
                            else:
                                keywordId = long(row[0]) + 1
                                
                            skillIdTemp = MyHandler.checkExistsTag(text.find('text').text)
                            skillNameTemp = ''
                            
                            if skillIdTemp == 0 or skillIdTemp == None:
                              try:
                                  skillIdTemp = keywordId
                                  skillNameTemp = text.find('text').text
                                  returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                  qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s,true)"""
                                  MyHandler.cur.execute(qryString, (keywordId,  text.find('text').text))
                                  skillIdTemp = keywordId
                                  MyHandler.db.commit()
                              except Exception, err:
                                  my_logger.critical(traceback.format_exc())
                                  MyHandler.db.rollback()
                                  
                            if not MyHandler.checkExistsUserTagAssociation(linkedinId, skillIdTemp):
                              try:
                                  qryString = 'INSERT INTO userPateAssociations(linkedinId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                  MyHandler.cur.execute(qryString, (linkedinId, skillIdTemp, True, text.find('relevance').text));
                                  MyHandler.db.commit()
                              except Exception, err:
                                  my_logger.critical(traceback.format_exc())
                                  MyHandler.db.rollback()
    
                result = alchemyObj.TextGetRankedKeywords(linkedinCompanySummary);

                skillIdTemp = 0L
                skillNameTemp = ""
                    
                tree = ET.fromstring(result)
                for textTag in tree.findall('keywords'):
                    for text in textTag.findall('keyword'):
                        MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                        curFetch = MyHandler.cur.fetchall()
                        if len(curFetch) == 0:
                            keywordId = 1
                        for row in curFetch:
                            if row[0] == None:
                                keywordId = 1
                            else:
                                keywordId = long(row[0]) + 1

                            skillIdTemp = MyHandler.checkExistsTag(text.find('text').text)
                            skillNameTemp = ''
                            
                            if skillIdTemp == 0 or skillIdTemp == None:
                                try:
                                    skillIdTemp = keywordId
                                    skillNameTemp = text.find('text').text
                                    returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                    qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s,true)"""
                                    MyHandler.cur.execute(qryString, (keywordId,  text.find('text').text))
                                    skillIdTemp = keywordId
                                    MyHandler.db.commit()
                                except Exception, err:
                                    my_logger.critical(traceback.format_exc())
                                    MyHandler.db.rollback()
    
                            if not MyHandler.checkExistsUserTagAssociation(linkedinId, skillIdTemp):
                                try:
                                    qryString = 'INSERT INTO userPateAssociations(linkedinId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                    MyHandler.cur.execute(qryString, (linkedinId, skillIdTemp, True, text.find('relevance').text));
                                    MyHandler.db.commit()
                                except Exception, err:
                                    my_logger.critical(traceback.format_exc())
                                    MyHandler.db.rollback()
                            
                            if not linkedinCompanyId == None:
                                if not MyHandler.checkExistsCompanyTagAssociation(linkedinCompanyId, skillIdTemp):
                                    try:
                                        qryString = 'INSERT INTO organizationPateAssociations(linkedinCompanyId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                        if not skillIdTemp == None:
                                            MyHandler.cur.execute(qryString, (linkedinCompanyId, skillIdTemp, True, 1));
                                            MyHandler.db.commit()
                                    except Exception, err:
                                      my_logger.critical(traceback.format_exc())
                                      MyHandler.db.rollback()
                                else:
                                    my_logger.info('Not inserting now - lkin compant summary')
            
            my_logger.info('Lkin Company Summary Done')
        else:
            my_logger.info('LinkedIn ID - NULL')
        
        return returnJSON
            
    @staticmethod
    def performProfilePate(linkedinId):
            
            returnJSON = []
            my_logger.info("Profile Pate entered")
            MyHandler.cur.execute("select summary from userDetails where linkedinId = %s ", (linkedinId))
            userSummary = ""

            curFetch = MyHandler.cur.fetchall()
            if len(curFetch) == 0:
                userSummary = ""
            
            for row in curFetch:
                userSummary = row[0]

            if len(userSummary) > 0:
                alchemyObj = AlchemyAPI.AlchemyAPI()
                alchemyObj.loadAPIKey("api_key_2.txt");
                result = alchemyObj.TextGetRankedConcepts(userSummary);

                skillIdTemp = 0L
                skillNameTemp = ""
                    
                tree = ET.fromstring(result)
                for textTag in tree.findall('concepts'):
                    for text in textTag.findall('concept'):
                        MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                        curFetch = MyHandler.cur.fetchall()
                        if len(curFetch) == 0:
                            keywordId = 1
                        for row in curFetch:
                            if row[0] == None:
                                keywordId = 1
                            else:
                                keywordId = long(row[0]) + 1
                                
                            skillIdTemp = MyHandler.checkExistsTag(text.find('text').text)
                            skillNameTemp = ''
                            
                            if skillIdTemp == 0 or skillIdTemp == None:
                              try:
                                  skillIdTemp = keywordId
                                  skillNameTemp = text.find('text').text
                                  returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                  qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s,true)"""
                                  MyHandler.cur.execute(qryString, (keywordId,  text.find('text').text))
                                  skillIdTemp = keywordId
                                  MyHandler.db.commit()
                              except Exception, err:
                                  my_logger.critical(traceback.format_exc())
                                  MyHandler.db.rollback()
                                  
                            if not MyHandler.checkExistsUserTagAssociation(linkedinId, skillIdTemp):
                              try:
                                  qryString = 'INSERT INTO userPateAssociations(linkedinId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                  MyHandler.cur.execute(qryString, (linkedinId, skillIdTemp, True, text.find('relevance').text));
                                  MyHandler.db.commit()
                              except Exception, err:
                                  my_logger.critical(traceback.format_exc())
                                  MyHandler.db.rollback()
    
                result = alchemyObj.TextGetRankedKeywords(userSummary);

                skillIdTemp = 0L
                skillNameTemp = ""
                    
                tree = ET.fromstring(result)
                for textTag in tree.findall('keywords'):
                    for text in textTag.findall('keyword'):
                        MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                        curFetch = MyHandler.cur.fetchall()
                        if len(curFetch) == 0:
                            keywordId = 1
                        for row in curFetch:
                            if row[0] == None:
                                keywordId = 1
                            else:
                                keywordId = long(row[0]) + 1

                            skillIdTemp = MyHandler.checkExistsTag(text.find('text').text)
                            skillNameTemp = ''
                            
                            if skillIdTemp == 0 or skillIdTemp == None:
                                try:
                                    skillIdTemp = keywordId
                                    skillNameTemp = text.find('text').text
                                    returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                    qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s,true)"""
                                    MyHandler.cur.execute(qryString, (keywordId,  text.find('text').text))
                                    skillIdTemp = keywordId
                                    MyHandler.db.commit()
                                except Exception, err:
                                    my_logger.critical(traceback.format_exc())
                                    MyHandler.db.rollback()
    
                            if not MyHandler.checkExistsUserTagAssociation(linkedinId, skillIdTemp):
                                try:
                                    qryString = 'INSERT INTO userPateAssociations(linkedinId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                    MyHandler.cur.execute(qryString, (linkedinId, skillIdTemp, True, text.find('relevance').text));
                                    MyHandler.db.commit()
                                except Exception, err:
                                    my_logger.critical(traceback.format_exc())
                                    MyHandler.db.rollback()
                                  
	        my_logger.info('User Profiling Complete - All Done' )               
		if len(returnJSON) == 0:
                    return []
                return json.dumps(returnJSON)
            else:
                my_logger.info("Profile Desc -> " + str(len(userSummary)))

    @staticmethod
    def performProjectPate(projectId):
            
            returnJSON = []
            my_logger.info("Project Pate entered")
            MyHandler.cur.execute("select projectName, projectDescription from userProjects where projectId = %s ", (projectId))
            projectDescription = ""
            
            # print all the first cell of all the rows
            for row in MyHandler.cur.fetchall() :
                if not type(row[0]) is NoneType and not type(row[1]) is NoneType:
                    projectDescription = str(row[0]).strip() + str(row[1]).strip()
                elif not type(row[0]) is NoneType:
                    projectDescription = row[0]
                elif not type(row[1]) is NoneType:
                    projectDescription = row[1]

                if len(projectDescription) > 0:
                    # Create an AlchemyAPI object.
                    alchemyObj = AlchemyAPI.AlchemyAPI()
    
                    # Load the API key from disk.
                    alchemyObj.loadAPIKey("api_key.txt");
    
                    ##Extract concept tags from a web URL.
                    result = alchemyObj.TextGetRankedConcepts(projectDescription);
    
                    skillIdTemp = 0L
                    skillNameTemp = "" 
                                
                    tree = ET.fromstring(result)
                    for textTag in tree.findall('concepts'):
                        for text in textTag.findall('concept'):
                          MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                          # print all the first cell of all the rows
                          for row in MyHandler.cur.fetchall() :
                              if row[0] == None:
                                  keywordId = 1
                              else:
                                  keywordId = long(row[0]) + 1
    
                              skillIdTemp = MyHandler.checkExistsTag(text.find('text').text)
                              if skillIdTemp == 0 or skillIdTemp == None:
                                try:
                                    skillIdTemp = keywordId
                                    skillNameTemp = text.find('text').text
                                    returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                    qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s, true)"""
                                    MyHandler.cur.execute(qryString, (keywordId, text.find('text').text))
                                    MyHandler.db.commit()
                                except Exception, err:
                                    my_logger.critical(traceback.format_exc())
                                    MyHandler.db.rollback()
                                     
                              if not MyHandler.checkExistsProjectTagAssociation(projectId, skillIdTemp):
                                try:
                                    qryString = 'INSERT INTO projectPateAssociations(projectId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s)'
                                    MyHandler.cur.execute(qryString, (projectId, skillIdTemp, True, text.find('relevance').text))
                                    MyHandler.db.commit()
                                except Exception, err:
                                    my_logger.critical(traceback.format_exc())
                                    MyHandler.db.rollback()
    
                    result = alchemyObj.TextGetRankedKeywords(projectDescription)
                    
                    skillIdTemp = 0L
                    skillNameTemp = ""
                    
                    tree = ET.fromstring(result)
                    for textTag in tree.findall('keywords'):
                        for text in textTag.findall('keyword'):
                            MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                            # print all the first cell of all the rows
                            for row in MyHandler.cur.fetchall() :
                                if row[0] == None:
                                    keywordId = 1
                                else:
                                    keywordId = long(row[0]) + 1
                                
                                skillIdTemp = MyHandler.checkExistsTag(text.find('text').text)
                                if skillIdTemp == 0 or skillIdTemp == None:       
                                    try:
                                        skillIdTemp = keywordId
                                        skillNameTemp = text.find('text').text
                                        returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                        qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s,true)"""
                                        MyHandler.cur.execute(qryString, (keywordId, text.find('text').text))
                                        MyHandler.db.commit()
                                    except Exception, err:
                                        my_logger.critical(traceback.format_exc())
                                        MyHandler.db.rollback()
    
                                if not MyHandler.checkExistsProjectTagAssociation(projectId, skillIdTemp):
                                    try:	  
                                        qryString = 'INSERT INTO projectPateAssociations(projectId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                        MyHandler.cur.execute(qryString, (projectId, skillIdTemp, True, text.find('relevance').text));
                                        MyHandler.db.commit()
                                    except Exception, err:
                                        my_logger.critical(traceback.format_exc())
                                        MyHandler.db.rollback()
                    else:
                        my_logger.info("Project Desc -> " + str(len(projectDescription)))

		    my_logger.info('Project Profiling Complete - All Done')                    
            if len(returnJSON) == 0:
                return []
            return json.dumps(returnJSON)

    @staticmethod
    def performFileRead(fileName):
        return ''
    
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "application/json")
        s.end_headers()

    def do_POST(self):
        # Parse the form data posted
        form = cgi.FieldStorage(
            fp=self.rfile, 
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST'                   
                     })

        # Begin the response
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write('Client: %s\n' % str(self.client_address))
        self.wfile.write('User-agent: %s\n' % str(self.headers['user-agent']))
        self.wfile.write('Path: %s\n' % self.path)
        self.wfile.write('Form data:\n')
        # Echo back information about what was posted in the form
        try:
            while True:
                givenId = form['id'].value
                analysisType = form['analysisType'].value
#                self.wfile.write('\tField (linkedinId) = Value(%s)\n' % givenId)
#                self.wfile.write('\tField (analysisType) = Value(%s)\n' % analysisType)
                try:
                    if analysisType == 'profilePate':
                        self.wfile.write(MyHandler.performProfilePate(givenId))
                    elif analysisType == 'projectPate':
                        self.wfile.write(MyHandler.performProjectPate(givenId))
                    elif analysisType == 'companyPate':
                        self.wfile.write(MyHandler.performCompanyPate(givenId))
                    elif analysisType == 'fileUpload':
                        self.wfile.write(MyHandler.performFileRead(givenId))
                    break
                except MySQLdb.OperationalError:
                    my_logger.critical('MYSQL DEAD~~!!')
                    db = MySQLdb.connect(host="localhost", # your host, usually localhost
                                         user="root", # your username - pate
                                         passwd="singapore", # your password - gurukadakshatgoogle
                                         db="gurukadakshDB") # name of the data base
    
                    # you must create a Cursor object. It will let
                    #  you execute all the query you need
                    cur = db.cursor()
                except Exception, err:
                    my_logger.critical(traceback.format_exc())
                    break

        except:
            self.wfile.write("required value not found")
            
        
        return
    
    def do_GET(s):
        """Respond to a GET request."""
        s.send_response(401)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write("<html><head><title>PATE.</title></head>")
        s.wfile.write("<body><p>GET NOT SUPPORTED AT THIS END POINT</p>")
        # If someone went to "http://something.somewhere.net/foo/bar/",
        # then s.path equals "/foo/bar/".
        s.wfile.write("<p>You accessed path: %s</p>" % s.path)
        s.wfile.write("</body></html>")
           
if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print(time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass                     
    httpd.server_close()
    MyHandler.db.close()
    print(time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))