import re
import json
import urllib
import urllib2
import AlchemyAPI
import sys
import xml.etree.ElementTree as ET
from re import sub
from sys import stderr
from HTMLParser import HTMLParser
from traceback import print_exc

class _DeHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.__text = []

    def handle_data(self, data):
        text = data.strip()
        if len(text) > 0:
            text = sub('[ \t\r\n]+', ' ', text)
            self.__text.append(text + ' ')

    def handle_starttag(self, tag, attrs):
        if tag == 'p':
            self.__text.append('\n\n')
        elif tag == 'br':
            self.__text.append('\n')

    def handle_startendtag(self, tag, attrs):
        if tag == 'br':
            self.__text.append('\n\n')

    def text(self):
        return ''.join(self.__text).strip()

def dehtml(text):
    try:
        parser = _DeHTMLParser()
        parser.feed(text)
        parser.close()
        return parser.text()
    except:
        print_exc(file=stderr)
        return text

def freqWords(wikiPrintURL):
    ##Create an AlchemyAPI object.
    alchemyObj = AlchemyAPI.AlchemyAPI()
    ##Load the API key from disk.
    alchemyObj.loadAPIKey("api_key_2.txt");

    ##Extract topic keywords from a web URL.
    result = alchemyObj.URLGetRankedKeywords(wikiPrintURL)
    keyword_list = []
    tree = ET.fromstring(result)
    for textTag in tree.findall('keywords'):
        for text in textTag.findall('keyword'):
             keyword_list.append(text.find('text').text)

    result = alchemyObj.URLGetRankedConcepts(wikiPrintURL)
    concepts_list = []
    tree = ET.fromstring(result)
    for textTag in tree.findall('concepts'):
        for text in textTag.findall('concept'):
            concepts_list.append(text.find('text').text)

    result = alchemyObj.URLGetRankedNamedEntities(wikiPrintURL)
    tree = ET.fromstring(result)
    for textTag in tree.findall('entities'):
        for text in textTag.findall('entity'):
            if text.find('type').text == 'FieldTerminology':
                concepts_list.append(text.find('text').text)
                
#    print 'Concepts List'
#    print '='*30
#    print concepts_list
#    print '='*30
#    print 'Keyword List'
#    print keyword_list
#    print '='*30
    freq_dic = {}
    ignore_list = ['at', 'the', 'for', 'is', 'in', 'an', 'am', 'a', 'from', 'during', 'within', 'since', 'over', 'and', 'of', 'to', 'retrieved', 'with', 'as']
    for concept in concepts_list:
        freq_dic[concept] = 1
        
    # Splitting the concept into subsequent words (Art School as Art, School)
    for keywd in keyword_list:
        for keyword in keywd.split():
            if keyword not in ignore_list:
                try:
                    freq_dic[keyword] += 1
                except:
                    freq_dic[keyword] = 1

    # create list of (key, val) tuple pairs
    freq_list = freq_dic.items()
    # sort by key or word
    freq_list.sort()

    # create list of (val, key) tuple pairs
    freq_list2 = [(val, key) for key, val in freq_dic.items()]
    # sort by val or frequency
    freq_list2.sort(reverse=True)
    #===========================================================================
    # wtList = []
    # for frq in freq_list2:
    #    wtList.append(frq[0])
    # print max(wtList)
    # display result
    #===========================================================================
    print '-'*30
    print 'High frequency words'
    print '-'*30
    for freq, word in freq_list2[:5]:
        for keywd in keyword_list:
            if word in keywd:
                print keywd, freq
    print '-'*30

def getWikiURL(searchfor):
  query = urllib.urlencode({'q': searchfor})
  url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' % query
  search_response = urllib.urlopen(url)
  search_results = search_response.read()
  results = json.loads(search_results)
  data = results['responseData']
  hits = data['results']
  #print 'Total results: %s' % data['cursor']['estimatedResultCount']
  #print 'Top %d hits:' % len(hits)
  #for h in hits: print ' ', h['url']
  #print 'For more results, see %s' % data['cursor']['moreResultsUrl']
  return hits[0]['url']

def isDisambiguationPage(wikiArticle):
    disambiguationPage = False
    wikiPrintURL = 'http://en.wikipedia.org/w/index.php?title='+wikiArticle.strip()+'&printable=yes'
##    print wikiPrintURL
    opener = urllib2.build_opener()
    opener.addheaders = [('User-agent', 'Mozilla/5.0')]
    infile = opener.open(wikiPrintURL)
    page = infile.read()
    searchText = dehtml(page)
    if 'disambiguation pages' in searchText:
        disambiguationPage = True
    return disambiguationPage

srchKwd = ''
if len(sys.argv) > 1:
    srchKwd = sys.argv[1]
else:
    srchKwd = 'Cognizant'

wikiURL = getWikiURL('site:wikipedia.org ' + srchKwd)
wikiArticle = wikiURL[wikiURL.find('wiki/')+5:]
wikiPrintURL = 'http://en.wikipedia.org/w/index.php?title='+wikiArticle.strip()+'&printable=yes'
#===============================================================================
# freqWords(wikiPrintURL)
#===============================================================================
##print wikiPrintURL
print isDisambiguationPage(wikiArticle)

