import urllib2
import json

# The request also includes the userip parameter which provides the end
# user's IP address. Doing so will help distinguish this legitimate
# server-side traffic from traffic which doesn't come from an end-user.
url = ('https://ajax.googleapis.com/ajax/services/search/web'
       '?v=1.0&q=Paris%20Hilton&userip=1.2.3.4')

request = urllib2.Request(
    url, None, {'Referer': 'www.google.com.sg'})
response = urllib2.urlopen(request)

# Process the JSON string.
results = json.load(response)
##for i in range(0,len(results)):
print results
