import re
import json
import urllib
import urllib2
import AlchemyAPI
import sys
import xml.etree.ElementTree as ET

def freqWords(txtPara):
    # create list of lower case words, \s+ --> match any whitespace(s)
    # you can replace file(filename).read() with given string
    ignore_list = ['at', 'the', 'for', 'is', 'in', 'an', 'am', 'a', 'from', 'during', 'within', 'since', 'over', 'and', 'of', 'to', 'retrieved', 'with', 'as', '^']
    word_list = re.split('\s+', txtPara.lower())
    wholeWords =  re.split('\n+', txtPara.lower())
    highFreqWords = []
    print '-'*30
    print 'Words in text:', len(word_list)
    # create dictionary of word:frequency pairs
    freq_dic = {}
    # punctuation marks to be removed
    punctuation = re.compile(r'[.?!,":;]')

    for word in word_list:
        # remove punctuation marks
        word = punctuation.sub("", word)
        # form dictionary
        try:
            if word not in ignore_list:
                freq_dic[word] += 1
        except: 
            freq_dic[word] = 1
        
    print 'Unique words:', len(freq_dic)
    # create list of (key, val) tuple pairs
    freq_list = freq_dic.items()
    # sort by key or word
    freq_list.sort()

    # create list of (val, key) tuple pairs
    freq_list2 = [(val, key) for key, val in freq_dic.items()]
    # sort by val or frequency
    freq_list2.sort(reverse=True)
    # display result
    print '-'*30
    print 'High frequency words'
    print '-'*30
    for freq, word in freq_list2[:10]:
        print word, freq
    print '-'*30

def getWikiURL(searchfor):
  query = urllib.urlencode({'q': searchfor})
  url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' % query
  search_response = urllib.urlopen(url)
  search_results = search_response.read()
  results = json.loads(search_results)
  data = results['responseData']
  hits = data['results']
  #print 'Total results: %s' % data['cursor']['estimatedResultCount']
  #print 'Top %d hits:' % len(hits)
  #for h in hits: print ' ', h['url']
  #print 'For more results, see %s' % data['cursor']['moreResultsUrl']
  return hits[0]['url']

def remove_html_tags(data):
    p = re.compile(r'<.*?>')
    return p.sub('', data)

def remove_extra_spaces(data):
    p = re.compile(r'\s+')
    return p.sub(' ', data)

wikiURL = getWikiURL('site:wikipedia.org '  + sys.argv[1])
wikiArticle = wikiURL[wikiURL.find('wiki/')+5:]

opener = urllib2.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]
wikiPrintURL = 'http://en.wikipedia.org/w/index.php?title='+wikiArticle.strip()+'&printable=yes'

##Create an AlchemyAPI object.
alchemyObj = AlchemyAPI.AlchemyAPI()
##Load the API key from disk.
alchemyObj.loadAPIKey("api_key.txt");

##Extract topic keywords from a web URL.
result = alchemyObj.URLGetRankedKeywords(wikiPrintURL)
keyword_list = []
tree = ET.fromstring(result)
for textTag in tree.findall('keywords'):
    for text in textTag.findall('keyword'):
         keyword_list.append(text.find('text').text)

result = alchemyObj.URLGetRankedConcepts(wikiPrintURL)
tree = ET.fromstring(result)
for textTag in tree.findall('concepts'):
    for text in textTag.findall('concept'):
        keyword_list.append(text.find('text').text)

infile = opener.open(wikiPrintURL)
page = infile.read()

removeHTML = remove_html_tags(page)
removeSpaces = remove_extra_spaces(removeHTML)

wordcount = dict((x,0) for x in keyword_list)
for w in re.findall(r"\w+", removeSpaces):
    if w in wordcount:
        wordcount[w] += 1

# create list of (key, val) tuple pairs
freq_list = wordcount.items()
# sort by key or word
freq_list.sort()

# create list of (val, key) tuple pairs
freq_list2 = [(val, key) for key, val in wordcount.items()]
# sort by val or frequency
freq_list2.sort(reverse=True)

print 'High frequency words'
print '-'*30
for freq, word in freq_list2[:10]:
    print word, freq
print '-'*30
