import os
import sys, time

folPath = '/home/karthik/Downloads/'
fileName = str(sys.argv[1])
print 'File to be processed - ', folPath + fileName 

os.system('xpdf/bin64/pdftotext ' + folPath + fileName + ' ' + folPath + fileName.replace('pdf', 'txt'))

time.sleep(2)

# Read mode opens a file for reading only.
try:
    f = open(folPath + fileName.replace('pdf', 'txt'), "r")
    try:
        # Read the entire contents of a file at once.
        fileContents = f.read() 
    finally:
        f.close()
except IOError:
    pass

print 'PDF Text - ' + str(len(fileContents))