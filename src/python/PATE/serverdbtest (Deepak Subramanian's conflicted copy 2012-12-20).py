import MySQLdb
import json
import time
import AlchemyAPI
import urllib
import urllib2
import xml.etree.ElementTree as ET
import sys
import BaseHTTPServer
from BaseHTTPServer import BaseHTTPRequestHandler
import cgi
import traceback

HOST_NAME = 'localhost' 
PORT_NUMBER = 7283 


class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    db = MySQLdb.connect(host="localhost", # your host, usually localhost
             user="root", # your username - pate
              passwd="singapore", # your password - gurukadakshatgoogle
              db="gurukadakshDB") # name of the data base

    # you must create a Cursor object. It will let
    #  you execute all the query you need
    cur = db.cursor()

    @staticmethod
    def getWikiURL(searchfor):
        query = urllib.urlencode({'q': searchfor})
        url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' % query
        search_response = urllib.urlopen(url)
        search_results = search_response.read()
        results = json.loads(search_results)
        data = results['responseData']
        hits = data['results']
        #print 'Total results: %s' % data['cursor']['estimatedResultCount']
        #print 'Top %d hits:' % len(hits)
        #for h in hits: print ' ', h['url']
        #print 'For more results, see %s' % data['cursor']['moreResultsUrl']
        return hits[0]['url']
    
    @staticmethod
    def freqWords(linkedinCompanyId, wikiPrintURL, companyDesc):
        returnJSON = []
        ##Create an AlchemyAPI object.
        alchemyObj = AlchemyAPI.AlchemyAPI()
        ##Load the API key from disk.
        alchemyObj.loadAPIKey("api_key.txt");

        ##Extract topic keywords from a web URL.
        result = alchemyObj.URLGetRankedKeywords(wikiPrintURL)
        keyword_list = []
        tree = ET.fromstring(result)
        for textTag in tree.findall('keywords'):
            for text in textTag.findall('keyword'):
                keyword_list.append(text.find('text').text)

        result = alchemyObj.TextGetRankedKeywords(companyDesc);
        tree = ET.fromstring(result)
        for textTag in tree.findall('keywords'):
            for text in textTag.findall('keyword'):
                keyword_list.append(text.find('text').text)

        result = alchemyObj.URLGetRankedConcepts(wikiPrintURL)
        concepts_list = []
        tree = ET.fromstring(result)
        for textTag in tree.findall('concepts'):
            for text in textTag.findall('concept'):
                concepts_list.append(text.find('text').text)

##        print '-'*30
        result = alchemyObj.URLGetRankedNamedEntities(wikiPrintURL)

        tree = ET.fromstring(result)
        for textTag in tree.findall('entities'):
            for text in textTag.findall('entity'):
                if text.find('type').text == 'FieldTerminology':
                    concepts_list.append(text.find('text').text)
                    
##        print 'Concepts List'
##        print '='*30
##        print concepts_list
##        print '='*30
##        print 'Keyword List'
##        print keyword_list
##        print '='*30
        freq_dic = {}
        ignore_list = ['at', 'the', 'for', 'is', 'in', 'an', 'am', 'a', 'from', 'during', 'within', 'since', 'over', 'and', 'of', 'to', 'retrieved', 'with', 'as']
        for concept in concepts_list:
            # Splitting the concept into subsequent words (Art School as Art, School)
            for conceptWord in concept.split():
                # create dictionary of word:frequency pairs
                for keywd in keyword_list:
                    if conceptWord in keywd and conceptWord not in ignore_list:
                        try:
                            freq_dic[concept] += 1
                        except:
                            freq_dic[concept] = 1

        # create list of (key, val) tuple pairs
        freq_list = freq_dic.items()
        # sort by key or word
        freq_list.sort()

        # create list of (val, key) tuple pairs
        freq_list2 = [(val, key) for key, val in freq_dic.items()]
        # sort by val or frequency
        freq_list2.sort(reverse=True)
        # display result
##        print '-'*30
##        print 'High frequency words for ' + wikiPrintURL
##        print '-'*30
        for freq, word in freq_list2[:15]:
            # Checking the skill and inserting into db
            MyHandler.cur.execute("select max(skillId) from skillsDatabase")
            # print all the first cell of all the rows
            for row in MyHandler.cur.fetchall() :
                if row[0] == None:
                    keywordId = 1
                else:
                    keywordId = long(row[0]) + 1

            skillIdTemp = MyHandler.checkExistsTag(word)
            skillNameTemp = ''
            if skillIdTemp == 0 :
                try:
                    skillIdTemp = keywordId
                    skillNameTemp = word
                    returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                    qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s,true)"""
                    MyHandler.cur.execute(qryString, (keywordId, word))
                    skillIdTemp = keywordId
                    MyHandler.db.commit()
                except Exception, err:
                    print(traceback.format_exc())
                    MyHandler.db.rollback()

                if not MyHandler.checkExistsCompanyTagAssociation(linkedinCompanyId, skillIdTemp):
                    try:
                        qryString = 'INSERT INTO organizationPateAssociations(linkedinCompanyId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                        MyHandler.cur.execute(qryString, (linkedinCompanyId, skillIdTemp, True, 1));
                        MyHandler.db.commit()
                    except Exception, err:
                      print(traceback.format_exc())
                      MyHandler.db.rollback()

        return json.dumps(returnJSON)
    
    @staticmethod    
    def checkExistsTag (tagname):
        MyHandler.cur.execute("select skillId from skillsDatabase where skillName = %s" , tagname)
        for row in MyHandler.cur.fetchall():
          print str(row[0]) + ' checkexiststag'
          if row[0] == None:
            return 0
          return row[0]
        return 0

    @staticmethod
    def checkExistsCompanyTagAssociation (linkedinCompanyId , pateKeyId):
        MyHandler.cur.execute("select pateKeyId from organizationPateAssociations where linkedinCompanyId = %s AND pateKeyId = %s" , (linkedinCompanyId, pateKeyId))
        for row in MyHandler.cur.fetchall():
            if row[0] == None:
                return False
            return True
        
    @staticmethod
    def checkExistsUserTagAssociation (personId , pateKeyId):
        MyHandler.cur.execute("select pateKeyId from userPateAssociations where linkedinId = %s AND pateKeyId = %s" , (personId, pateKeyId))
        for row in MyHandler.cur.fetchall():
            if row[0] == None:
                return False
            return True

    @staticmethod
    def checkExistsProjectTagAssociation (projectId , pateKeyId):
        MyHandler.cur.execute("select pateKeyId from projectPateAssociations where projectId = %s AND pateKeyId = %s" , (projectId, pateKeyId))
        for row in MyHandler.cur.fetchall():
            
            if row[0] == None:
                return False
            return True

    @staticmethod
    def performCompanyPate(linkedinId):
            print "Company Pate entered"
            if not linkedinId == None:
                MyHandler.cur.execute("select linkedinCompanyId from userPositionsDetails where linkedinId = %s ", (linkedinId))
                linkedinCompanyIdList = []
                # print all the first cell of all the rows
                for row in MyHandler.cur.fetchall() :
                    linkedinCompanyIdList.append(row[0])

            for linkedinCompanyId in linkedinCompanyIdList:
                if not linkedinCompanyId == None:
                    MyHandler.cur.execute("select organizationName, organizationDescription from organizationsDatabase where linkedinCompanyId= %s", (linkedinCompanyId))

                    for row in MyHandler.cur.fetchall() :
                        companyName = row[0]
                        companyDesc = row[1]

                    if len(companyName) > 0:
                        print companyName
                        wikiURL = MyHandler.getWikiURL('site:wikipedia.org ' + companyName)
                        wikiArticle = wikiURL[wikiURL.find('wiki/')+5:]

                        opener = urllib2.build_opener()
                        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
                        wikiPrintURL = 'http://en.wikipedia.org/w/index.php?title='+wikiArticle.strip()+'&printable=yes'
                        print 'Company Profiling Complete'
                        return MyHandler.freqWords(linkedinCompanyId, wikiPrintURL, companyDesc)
                    print 'Company Profiling Complete'
            return []
            
    @staticmethod
    def performProfilePate(linkedinId):
            returnJSON = []
            print "Profile Pate entered"
            MyHandler.cur.execute("select summary from userDetails where linkedinId = %s ", (linkedinId))
            userSummary = ""
            # print all the first cell of all the rows
            for row in MyHandler.cur.fetchall() :
                userSummary = row[0]

            if len(userSummary) > 0:
                # Create an AlchemyAPI object.
                alchemyObj = AlchemyAPI.AlchemyAPI()

                # Load the API key from disk.
                alchemyObj.loadAPIKey("api_key.txt");

                ##Extract concept tags from a web URL.
                result = alchemyObj.TextGetRankedConcepts(userSummary);
                ##print result

                tree = ET.fromstring(result)
                for textTag in tree.findall('concepts'):
                    for text in textTag.findall('concept'):
                        MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                        for row in MyHandler.cur.fetchall() :
                          if row[0] == None:
                               keywordId = 1
                          else:
                              keywordId = long(row[0]) + 1

                        skillIdTemp = MyHandler.checkExistsTag(text.find('text').text)
                        skillNameTemp = ''
                        
                        if skillIdTemp == 0 :
                            try:
                                skillIdTemp = keywordId
                                skillNameTemp = text.find('text').text
                                returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                print skillNameTemp
                                qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s,true)"""
                                MyHandler.cur.execute(qryString, (keywordId,  text.find('text').text))
                                skillIdTemp = keywordId
                                MyHandler.db.commit()
                            except Exception, err:
                                print(traceback.format_exc())
                                MyHandler.db.rollback()
                                
                        if not MyHandler.checkExistsUserTagAssociation(linkedinId, skillIdTemp):
                            try:
                                qryString = 'INSERT INTO userPateAssociations(linkedinId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                print qryString, (linkedinId, skillIdTemp, True, text.find('relevance').text)
                                MyHandler.cur.execute(qryString, (linkedinId, skillIdTemp, True, text.find('relevance').text));
                                MyHandler.db.commit()
                            except Exception, err:
                                print(traceback.format_exc())
                                MyHandler.db.rollback()
    
                result = alchemyObj.TextGetRankedKeywords(userSummary);

                skillIdTemp = 0L
                skillNameTemp = ""
                    
                tree = ET.fromstring(result)
                for textTag in tree.findall('keywords'):
                    for text in textTag.findall('keyword'):
                        MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                        # print all the first cell of all the rows
                        for row in MyHandler.cur.fetchall() :
                            if row[0] == None:
                                keywordId = 1
                            else:
                                keywordId = long(row[0]) + 1

                            skillIdTemp = MyHandler.checkExistsTag(text.find('text').text)
                            skillNameTemp = ''
                            if skillIdTemp == 0 :
                                try:
                                    skillIdTemp = keywordId
                                    skillNameTemp = text.find('text').text
                                    returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                    qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s,true)"""
                                    MyHandler.cur.execute(qryString, (keywordId,  text.find('text').text))
                                    skillIdTemp = keywordId
                                    MyHandler.db.commit()
                                except:
                                    MyHandler.db.rollback()

                            if not MyHandler.checkExistsUserTagAssociation(linkedinId, skillIdTemp):
                                try:
                                    qryString = 'INSERT INTO userPateAssociations(linkedinId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                    MyHandler.cur.execute(qryString, (linkedinId, skillIdTemp, True, text.find('relevance').text));
                                    MyHandler.db.commit()
                                except Exception, err:
                                    print(traceback.format_exc())
                                    MyHandler.db.rollback()
                                  
                if len(returnJSON) == 0:
                    return []
                return json.dumps(returnJSON)
            else:
                print "Profile Desc -> " + str(len(userSummary))

    @staticmethod
    def performProjectPate(projectId):
            returnJSON = []
            print "Project Pate entered"
            MyHandler.cur.execute("select projectName, projectDescription from userProjects where projectId = %s ", (projectId))
            projectDescription = ""
            # print all the first cell of all the rows
            for row in MyHandler.cur.fetchall() :
                projectDescription = row[0]

            if len(projectDescription) > 0:
                # Create an AlchemyAPI object.
                alchemyObj = AlchemyAPI.AlchemyAPI()

                # Load the API key from disk.
                alchemyObj.loadAPIKey("api_key.txt");

                ##Extract concept tags from a web URL.
                result = alchemyObj.TextGetRankedConcepts(projectDescription);
                ##print result

                skillIdTemp = 0L
                skillNameTemp = ""
                
                tree = ET.fromstring(result)
                for textTag in tree.findall('concepts'):
                    for text in textTag.findall('concept'):
                      MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                      # print all the first cell of all the rows
                      for row in MyHandler.cur.fetchall() :
                          print row
                          if row[0] == None:
                              print "IF"
                              keywordId = 1
                          else:
                              keywordId = long(row[0]) + 1
                              print keywordId

                          skillIdTemp = MyHandler.checkExistsTag(text.find('text').text)
                          if skillIdTemp == 0 :
                              try:
                                skillIdTemp = keywordId
                                skillNameTemp = text.find('text')
                                returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s,true)"""
                                MyHandler.cur.execute(qryString, (keywordId, text.find('text').text))
                                skillIdTemp = keywordId
                                MyHandler.db.commit()
                              except:
                                  MyHandler.db.rollback()
                          if not MyHandler.checkExistsProjectTagAssociation(projectId, skillIdTemp):
                              try:
                                    qryString = 'INSERT INTO projectPateAssociations(projectId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                    MyHandler.cur.execute(qryString, (projectId, skillIdTemp, True, text.find('relevance').text));
                                    MyHandler.db.commit()
                              except Exception, err:
                                  print(traceback.format_exc())
                                  MyHandler.db.rollback()

                result = alchemyObj.TextGetRankedKeywords(projectDescription);
        
                tree = ET.fromstring(result)
                for textTag in tree.findall('keywords'):
                    for text in textTag.findall('keyword'):
                        MyHandler.cur.execute("select max(skillId) from skillsDatabase")
                        # print all the first cell of all the rows
                        for row in MyHandler.cur.fetchall() :
                            if row[0] == None:
                                keywordId = 1
                            else:
                                keywordId = long(row[0]) + 1
                                print keywordId
                            
                            skillIdTemp = MyHandler.checkExistsTag(text.find('text').text)
                            if skillIdTemp == 0 :       
                                try:
                                    #if not skillIdTemp == None and not skillNameTemp == None:
                                    returnJSON.append({'skillId': skillIdTemp, 'skillName': skillNameTemp})
                                    qryString = """INSERT INTO skillsDatabase (skillId,skillName,addedByPate) VALUES (%s, %s,true)"""
                                    MyHandler.cur.execute(qryString, (keywordId, text.find('text').text))
                                    skillIdTemp = keywordId
                                    MyHandler.db.commit()
                                except:
                                    MyHandler.db.rollback()
                        #if not skillIdTemp == None:
                            if not MyHandler.checkExistsProjectTagAssociation(projectId, skillIdTemp):
                                try:	  
                                      qryString = 'INSERT INTO projectPateAssociations(projectId,pateKeyId,keyType,keyWeight) VALUES (%s, %s, %s, %s);'
                                      print qryString, str(projectId), str(skillIdTemp), text.find('relevance').text
                                      MyHandler.cur.execute(qryString, (projectId, skillIdTemp, True, text.find('relevance').text));
                                      MyHandler.db.commit()
                                except Exception, err:
                                    print(traceback.format_exc())
                                    MyHandler.db.rollback()

                if len(returnJSON) == 0:
                    return []
                return json.dumps(returnJSON)
            else:
                print "Project Desc -> " + str(len(projectDescription))

    def do_HEAD(s):
	print "Stage AAAAA"
        s.send_response(200)
        s.send_header("Content-type", "application/json")
        s.end_headers()

    def do_POST(self):
	print "Stage 0.001"
        # Parse the form data posted
        form = cgi.FieldStorage(
            fp=self.rfile, 
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST'                   
                     })

        # Begin the response
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write('Client: %s\n' % str(self.client_address))
        self.wfile.write('User-agent: %s\n' % str(self.headers['user-agent']))
        self.wfile.write('Path: %s\n' % self.path)
        self.wfile.write('Form data:\n')
	print "Stage 0.0015"
        # Echo back information about what was posted in the form
        try:
            givenId = form['id'].value
            analysisType = form['analysisType'].value
            self.wfile.write('\tField (linkedinId) = Value(%s)\n' % givenId)
	    print "Stage 0.002"
            self.wfile.write('\tField (analysisType) = Value(%s)\n' % analysisType)
            try:
                if analysisType == 'profilePate':
                    self.wfile.write(MyHandler.performProfilePate(givenId))
                elif analysisType == 'projectPate':
		    print "Stage 1"
                    self.wfile.write(MyHandler.performProjectPate(givenId))
                elif analysisType == 'companyPate':
                    self.wfile.write(MyHandler.performCompanyPate(givenId))
            except Exception, err:
                print(traceback.format_exc())
        except:
            self.wfile.write("required value not found")
            
        
        return
    
    def do_GET(s):
        """Respond to a GET request."""
        s.send_response(401)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write("<html><head><title>PATE.</title></head>")
        s.wfile.write("<body><p>GET NOT SUPPORTED AT THIS END POINT</p>")
        # If someone went to "http://something.somewhere.net/foo/bar/",
        # then s.path equals "/foo/bar/".
        s.wfile.write("<p>You accessed path: %s</p>" % s.path)
        s.wfile.write("</body></html>")
           
if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass                     
    httpd.server_close()
    MyHandler.db.close()
    print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)
    
