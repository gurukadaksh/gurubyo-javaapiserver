# another word frequency program, uses re
# tested with Python2.4.3   HAB
import re
import sys
# this one in honor of 4th July, or pick text file you have!!!!!!!
#filename = 'tags.txt'
data = sys.stdin.readlines()
fileString = ''.join(data)
# create list of lower case words, \s+ --> match any whitespace(s)
# you can replace file(filename).read() with given string
word_list = re.split('\s+', fileString.lower())#file(filename).read().lower())
wholeWords =  re.split('\n+', fileString.lower())#file(filename).read().lower())
highFreqWords = []
print '-'*30
print 'Words in text:', len(word_list)
# create dictionary of word:frequency pairs
freq_dic = {}
# punctuation marks to be removed
punctuation = re.compile(r'[.?!,":;]')

for word in word_list:
    # remove punctuation marks
    word = punctuation.sub("", word)
    # form dictionary
    try: 
        freq_dic[word] += 1
    except: 
        freq_dic[word] = 1
    
print 'Unique words:', len(freq_dic)
# create list of (key, val) tuple pairs
freq_list = freq_dic.items()
# sort by key or word
freq_list.sort()
# display result
#for word, freq in freq_list:
 #   print word, freq

# create list of (val, key) tuple pairs
freq_list2 = [(val, key) for key, val in freq_dic.items()]
# sort by val or frequency
freq_list2.sort(reverse=True)
# display result
print '-'*30
print 'High frequency words'
print '-'*30
for freq, word in freq_list2[:5]:
    print word, freq
print '-'*30
for freq, indicators in freq_list2[:5]:
    for phrase in wholeWords:
        # To check if the indicator is a full word (ex: art in partner should not be counted)
    	#if (indicators + ' ') or (' ' + indicators + ' ') or (' ' + indicators)  in phrase:
        if indicators in phrase:
            if phrase not in highFreqWords:
                print phrase + '--------' + indicators
                highFreqWords.append(phrase)
   #      if ' ' + indicators + ' ' in phrase:
			# if phrase not in highFreqWords:
			# 	print phrase + '--------' + indicators
   #      elif ' ' + indicators in phrase:
   #          if phrase not in highFreqWords:
   #              print phrase + '--------' + indicators
   #      elif indicators + ' ' in phrase:
   #          if phrase not in highFreqWords:
   #              print phrase + '--------' + indicators
                #highFreqWords.append(phrase)
        # elif ' ' + indicators in phrase:
        #     if phrase not in highFreqWords:
        #         highFreqWords.append(phrase)
        # elif indicators + ' ' in phrase:
        #     if phrase not in highFreqWords:
        #         highFreqWords.append(phrase)

print '-'*60
print freq_list2[:5]
print highFreqWords
print '-'*60